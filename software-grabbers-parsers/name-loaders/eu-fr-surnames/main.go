package main

import (
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"strconv"

	log "github.com/sirupsen/logrus"
	"unset.space/new-mailer/go-mailer/shared/httpclient"
)

const (
	cLASTNotLoadedPageNumber int = 3702
)

func main() {
	log.SetLevel(log.DebugLevel)
	//===========================
	hn_useragent := `User-Agent`
	//hn_content_encoding := `Content-Encoding`

	hv_Useragent := `Mozilla/5.0 (Windows NT 6.1; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0`
	//hv_encoding := `gzip,deflate`

	hdr_httpgeneral := http.Header{
		hn_useragent: {hv_Useragent},
		//hn_content_encoding: {hv_encoding},
		`Connection`:      {`keep-alive`},
		`Pragma`:          {`no-cache`},
		`Cache-Control`:   {`no-cache`},
		`Accept-Language`: {`en-US,en;q=0.5`},
	}
	nStartPage := cLASTNotLoadedPageNumber - 1

	var HttpConnectTimeoutSec uint = 5
	var HttpPageLoadTimeoutSec uint = 10

	netTimeoutConnect := time.Second * time.Duration(HttpConnectTimeoutSec)
	netTimeoutPageLoad := time.Second * time.Duration(HttpPageLoadTimeoutSec)

	httpClient := httpclient.GetNewClient(HttpConnectTimeoutSec, &hdr_httpgeneral)
	//	httpClient.SetProxyAndPort(t.currProxy.Address, netTimeoutConnect) //Fiddler - comment it
	//httpClient.SetProxy("", 0, netTimeoutConnect)//Fiddler uncomment it

	//Create results file:
	fName := "FR-surnames.csv"
	//out, err := os.OpenFile(fName, os.O_CREATE|os.O_WRONLY, 0666)
	out, err := os.OpenFile(fName, os.O_APPEND|os.O_WRONLY, 0666)
	if err != nil {
		fmt.Printf("error[%s] creating out file[%s]!", err.Error(), fName)
		return
	}
	defer out.Close()
	//sHeaderCsv := "SURNAME,COUNT,PERCENT,PROP100K\n"
	//_, err = out.WriteString(sHeaderCsv)

	//loop by pages:
	sUrlFormat := "http://www.journaldesfemmes.com/nom-de-famille/noms/1/%d/france.shtml"
	nLastPageNum := 3808
	nNamesFoundTotal := 0
	nProxyIdx := 0
	for i := nStartPage; i < nLastPageNum; i++ {
		log.Infof("Loading page %d of %d", i+1, nLastPageNum-1)

		nextUrl := fmt.Sprintf(sUrlFormat, i+1)

		//time.Sleep(time.Duration(1500) * time.Millisecond)
		//62.210.250.215:56528
		httpClient.SetProxyAndPort(GArrProxies[nProxyIdx], netTimeoutConnect)
		code, body, hdrs, err := httpClient.Get(nextUrl, netTimeoutPageLoad, nil)
		if nil != err || 200 != code {
			msg := fmt.Sprintf("Error loading page #%d: %v. HttpCode: %v", i, err, code)
			log.Debugln(msg)

			nProxyIdx++
			if nProxyIdx >= len(GArrProxies) {
				nProxyIdx = 0
			}
			//time.Sleep(time.Duration(10000) * time.Millisecond)
			i--
			continue
		}
		log.Debugf("Page #%d loaded: code: %v, Contentlength: %v, HeadersCount^ %v, Error: %v\n", i, code, len(body), len(hdrs), err)

		//=====================================================
		outFormatHtml := "/home/df/dev/go/dfwork/coldetector_/Names/EU/FR/_Surnames_full_list/journaldesfemmes.com_%05d.html"
		strHtmlOut := fmt.Sprintf(outFormatHtml, i+1)
		outHtml, err := os.OpenFile(strHtmlOut, os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			fmt.Printf("error[%s] creating out HTML file[%s]!", err.Error(), strHtmlOut)
			return
		}
		_, err = outHtml.Write(body)
		outHtml.Close()
		//=====================================================

		// now parse page and save CSV format:
		// </tr><tr>
		// <td></td>
		// <td>1.</td>
		// <td><a href="/nom-de-famille/nom/145690/martin.shtml">Martin</a></td>
		// <td align="right">&nbsp;&nbsp;235846</td>
		// <td></td><td></td>
		// <td>31.</td>
		// <td><a href="/nom-de-famille/nom/132776/legrand.shtml">Legrand</a></td>
		// <td align="right">&nbsp;&nbsp;49455</td>
		// <td></td>
		// </tr><tr>
		// <td></td>
		delimNr1 := "<td><a href=\"/nom-de-famille/"
		arrNamesWithCounts := strings.Split(string(body), delimNr1)

		log.Infof("Parsing page %d of %d...", i+1, nLastPageNum-1)
		nNamesFoundOnPage := 0
		checkPart1_1 := "<td align=\"right\">&nbsp;&nbsp;"
		//checkPart1_2 := "<td align=\"right\">&nbsp;&nbsp;"
		for _, parts1 := range arrNamesWithCounts {
			if !strings.Contains(parts1, checkPart1_1) {
				continue
			}
			strSurname := ""
			strCount := ""

			pSubstr := GetSubstring(parts1, ".shtml\">", "</a><")
			if nil != pSubstr {
				strSurname = strings.ToUpper(*pSubstr)
			}
			pSubstr = GetSubstring(parts1, "align=\"right\">&nbsp;&nbsp;", "</td>")
			if nil != pSubstr {
				strCount = strings.ToUpper(*pSubstr)
				//===========
				//2000&NBSP;&NBSP;2500
				if strings.Contains(strCount, "&NBSP;") {
					countParts := strings.Split(strCount, "&NBSP;")
					if 3 != len(countParts) {
						log.Debugf("numeric parts %d != 3 [%s]", len(countParts), strCount)
					} else {
						minCount, _ := strconv.Atoi(countParts[0])
						maxCount, _ := strconv.Atoi(countParts[2])
						avgCount := int((minCount + maxCount) / 2)
						strCount = strconv.Itoa(avgCount)
					}
				}
			}

			if 0 == len(strSurname) {
				// msg := fmt.Sprintf("Surname is empty! page #%d: %v. HttpCode: %v", i, err, code)
				log.Debugln(parts1)
				continue
			}
			if 0 == len(strCount) {
				msg := fmt.Sprintf("Count is empty! page #%d: %v. HttpCode: %v", i, err, code)
				log.Debugln(msg)
				return
			}
			nNamesFoundOnPage++
			nNamesFoundTotal++

			strLine := fmt.Sprintf("%s,%s,,\n", strSurname, strCount)
			_, err = out.WriteString(strLine)
		}
		log.Infof("Page %d parsed, %d names more got. Total found: %d", i+1, nNamesFoundOnPage, nNamesFoundTotal)

		if 0 >= nNamesFoundOnPage {
			msg := fmt.Sprintf("Surnames not foud on page! page #%d: %v. HttpCode: %v", i, err, code)
			log.Debugln(msg)
			return
		}
	}

	// ===================================================

	return
}

func GetSubstring(src string, before string, after string) *string {
	if 0 == len(src) {
		return nil
	}

	startIdx := 0
	if 0 < len(before) {
		startIdx = strings.Index(src, before)
		if 0 > startIdx {
			return nil
		}
		startIdx += len(before)
	}

	endIdxIdx := len(src) - 1
	if 0 < len(after) {
		if startIdx > 0 {
			endIdxIdx = strings.Index(src[startIdx:], after)
			if 0 <= endIdxIdx {
				endIdxIdx += startIdx
			}
		} else {
			endIdxIdx = strings.Index(src, after)
		}
	}

	retValStr := src[startIdx:endIdxIdx]
	return &retValStr
}
