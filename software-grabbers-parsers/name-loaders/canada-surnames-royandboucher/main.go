package main

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"unset.space/new-mailer/go-mailer/shared/httpclient"
)

func main() {
	log.SetLevel(log.DebugLevel)

	//======================================================
	var (
		cLASTNotLoadedPageIdx int = 3
		cLASTPageIdx          int = 81

		cUrlFormat string = "https://www.royandboucher.com/tng/surnames_statistics.php?surname=&offset=%d&tree=&test_type=&test_group=&tngpage=%d"

		outFormatHtml string = "/home/df/dev/go/dfwork/nameloader/canada-surnames-royandboucher/HTML_All/%02d.html"

		cReferer string = "https://www.royandboucher.com/tng/surnames_statistics.php"

		strCsvHeaders string = "NAME,COUNT,PERCENT,PROP100K"

		cDatasetsDir string = "/home/df/dev/go/dfwork/nameloader/canada-surnames-royandboucher/outCsv"

		cOutCsvTryToAppendFirst bool = true
	)
	//======================================================

	//======================================================
	hn_useragent := `User-Agent`
	//hn_content_encoding := `Content-Encoding`

	hv_Useragent := `Mozilla/5.0 (Windows NT 6.1; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0`
	//hv_encoding := `gzip,deflate`

	hdr_httpgeneral := http.Header{
		hn_useragent: {hv_Useragent},
		//hn_content_encoding: {hv_encoding},
		`Connection`:      {`keep-alive`},
		`Pragma`:          {`no-cache`},
		`Cache-Control`:   {`no-cache`},
		`Accept-Language`: {`en-US,en;q=0.5`},
	}
	nStartPageIdx := cLASTNotLoadedPageIdx

	var HttpConnectTimeoutSec uint = 5
	var HttpPageLoadTimeoutSec uint = 40

	netTimeoutConnect := time.Second * time.Duration(HttpConnectTimeoutSec)
	netTimeoutPageLoad := time.Second * time.Duration(HttpPageLoadTimeoutSec)

	httpClient := httpclient.GetNewClient(HttpConnectTimeoutSec, &hdr_httpgeneral)
	//	httpClient.SetProxyAndPort(t.currProxy.Address, netTimeoutConnect) //Fiddler - comment it
	//httpClient.SetProxy("", 0, netTimeoutConnect)//Fiddler uncomment it

	//Create results file:
	fErrLoadName := "_ErrLoadingUrls.csv"
	fErrLoadUrls, err := os.OpenFile(fErrLoadName, os.O_APPEND|os.O_WRONLY, 0666)
	if err != nil {
		fErrLoadUrls, err = os.OpenFile(fErrLoadName, os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			fmt.Printf("error[%s] open/create errors file[%s]!", err.Error(), fErrLoadName)
			return
		}
	}
	defer fErrLoadUrls.Close()
	//sHeaderCsv := "SURNAME,COUNT,PERCENT,PROP100K\n"
	//_, err = out.WriteString(sHeaderCsv)

	//https://www.royandboucher.com/tng/surnames_statistics.php?surname=&offset=3950&tree=&test_type=&test_group=&tngpage=80
	//loop by pages:
	nProxyIdx := 0
	const nMaxProxyTries = 200
	nCurrProxyTry := 1

	strCsvFileContent := strCsvHeaders + "\n"

	for i := nStartPageIdx; i <= cLASTPageIdx; i++ {
		if 1 == i {
			strCsvFileContent = strCsvHeaders + "\n"
		} else {
			strCsvFileContent = ""
		}

		log.Infof("Loading page %d of %d", i, cLASTPageIdx)
		nextUrl := fmt.Sprintf(cUrlFormat, (i-1)*50, i)

		//time.Sleep(time.Duration(1500) * time.Millisecond)
		//62.210.250.215:56528
		httpClient.SetProxyAndPort(GArrProxies[nProxyIdx], netTimeoutConnect)

		var addHeaders = map[string]string{
			"Accept":          "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
			"Accept-Encoding": "gzip, deflate, br",
			"Referer":         cReferer,
			"Connection":      "keep-alive",
		}
		code, body, hdrs, err := httpClient.Get(nextUrl, netTimeoutPageLoad, httpclient.MergeUserHeaders(&addHeaders))
		if nil != err || 200 != code {
			msg := fmt.Sprintf("Error loading page #%d: %v. HttpCode: %v", i, err, code)
			log.Warningln(msg)

			bLogErrorAndSkip := true
			if 0 == code {
				nCurrProxyTry++
				if nCurrProxyTry < nMaxProxyTries {
					nProxyIdx++
					if nProxyIdx >= len(GArrProxies) {
						nProxyIdx = 0
					}
					i--
					bLogErrorAndSkip = false
				}
			}

			if bLogErrorAndSkip {
				strErrLine := fmt.Sprintf("%d\n", i)
				fErrLoadUrls.WriteString(strErrLine)
				break
			}
			continue
		}
		log.Debugf("Page #%d loaded: code: %v, Contentlength: %v, HeadersCount^ %v, Error: %v\n", i, code, len(body), len(hdrs), err)

		//=====================================================
		strHtmlOut := fmt.Sprintf(outFormatHtml, i)
		outHtml, err := os.OpenFile(strHtmlOut, os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			fmt.Printf("error[%s] creating out HTML file[%s]!", err.Error(), strHtmlOut)
			strErrLine := fmt.Sprintf("%d\n", i)
			fErrLoadUrls.WriteString(strErrLine)
			continue
		}
		_, err = outHtml.WriteString(fmt.Sprintf("<!--\n%s\n-->\n", nextUrl))
		_, err = outHtml.Write(body)
		outHtml.Close()
		//=====================================================

		// now parse page and save CSV format:
		// <form action="http://www.wordle.net/advanced" method="POST" id="wordle" target="_blank">
		//     <textarea name="colorwordcounts" style="display:none">
		// Emma:709:FF0000
		// Louise:548:FF0000
		// Elise:479:FF0000strconv.ToUpper(
		// Marie:446:FF0000
		// ...
		// Luca:290:0000FF
		// Alexander:283:0000FF
		// ...
		//     </textarea>
		//     <input name="bg" type="hidden" value="FFFFFF">

		pAllTableText := GetSubstring(string(body), "		</tr></thead>", "	</table>")
		if nil == pAllTableText {
			strErrLine := fmt.Sprintf("HTML STRUCTURE ERROR - no table:\t%d,\n", i)
			log.Warning(strErrLine)
			fErrLoadUrls.WriteString(strErrLine)
			continue
		}

		delimNr1 := "&nbsp;</span></td></tr>"
		arrNamesWithCounts := strings.Split(*pAllTableText, delimNr1)

		//log.Infof("Parsing page %d of %d...", i+1, nLastPageNum-1)
		nNamesFoundOnPage := 0
		//strCsvFileContent := strCsvHeaders + "\n"
		//checkPart1_2 := "<td align=\"right\">&nbsp;&nbsp;"
		strParsingErrorStr := ""
		delimNr2 := "<td class=\"sscell databack\""
		for _, parts1 := range arrNamesWithCounts {
			if 20 >= len(parts1) {
				continue
			}
			allNameInfoParts := strings.Split(parts1, delimNr2)
			if 10 != len(allNameInfoParts) {
				strParsingErrorStr = fmt.Sprintf("HTML STRUCTURE ERROR - %d parts of the name[%s]: %d\n", len(allNameInfoParts), parts1, i)
				break
			}

			strContainName := allNameInfoParts[1]
			strContainCount := allNameInfoParts[3]
			// 	strGender := ""
			strName := GetSubstring(strContainName, "class=\"normal\">&nbsp;<b>", "<")
			if nil == strName {
				strParsingErrorStr = fmt.Sprintf("HTML 'Name' #%d parsing error - has not been found!\n", nNamesFoundOnPage)
				break
			}
			// 	nCount, err := strconv.Atoi(allNameInfoParts[1])
			// 	if nil != err || 0 >= nCount {
			// 		strParsingErrorStr = fmt.Sprintf("HTML 'Count' parsing error[%v]:\t{\"%s\", \"%s\"},\n", err, country, yr)
			// 		break
			// 	}
			strCount := GetSubstring(strContainCount, "class=\"normal\">&nbsp;<b>", "<")
			if nil == strCount {
				strParsingErrorStr = fmt.Sprintf("HTML 'Count' #%d parsing error - has not been found!\n", nNamesFoundOnPage)
				break
			}
			// 	//Gender:
			// 	strGender_F := "FF0000"
			// 	strGender_M := "0000FF"
			// 	if strings.Contains(allNameInfoParts[2], strGender_F) {
			// 		strGender = "F"
			// 	} else if strings.Contains(allNameInfoParts[2], strGender_M) {
			// 		strGender = "M"
			// 	} else {
			// 		strParsingErrorStr = fmt.Sprintf("HTML 'Gender' parsing error! Gender: %s :\t{\"%s\", \"%s\"},\n", allNameInfoParts[2], country, yr)
			// 		break
			// 	}

			nNamesFoundOnPage++
			//strCsvFileContent += fmt.Sprintf("%s,%s,%s\n", strName, strGender, strCount)
			strCsvFileContent += fmt.Sprintf("%s,%s\n", *strName, *strCount)
			//===========================================

		} //loop by names found on page:

		//===========================================
		//Check parsing error:
		if 0 != len(strParsingErrorStr) {
			log.Warning(strParsingErrorStr)
			fErrLoadUrls.WriteString(strParsingErrorStr)
			strParsingErrorStr = ""
			continue //load next url
		}
		//===========================================

		if 0 >= nNamesFoundOnPage {
			msg := fmt.Sprintf("HTML parsing error! 0 names found:\t%d\n", i)
			log.Warning(msg)
			fErrLoadUrls.WriteString(msg)
			continue //load next url
		} else {
			msg := fmt.Sprintf("Found %d names on page %d", nNamesFoundOnPage, i)
			log.Infoln(msg)
		}

		// //===========================================
		lastDir := cDatasetsDir
		//otherwise - save results
		// os.Mkdir(cDatasetsDir, 0766)
		// countryDir := filepath.Join(cDatasetsDir, country)
		// os.Mkdir(countryDir, 0766)
		// countryDirFname := filepath.Join(countryDir, "firstname")
		// os.Mkdir(countryDirFname, 0766)
		// //===========================================
		// countryAllYearsDir := filepath.Join(countryDirFname, "allyears")
		// os.Mkdir(countryAllYearsDir, 0766)
		// lastDir = countryAllYearsDir
		//===========================================
		csvName := fmt.Sprintf("canada-surnames.csv")
		//outCsvFileName := filepath.Join(lastDir, country+"_"+yr+".csv")
		outCsvFileName := filepath.Join(lastDir, csvName)
		var (
			outCsvFile *os.File
			errOutCsv  error
		)
		if cOutCsvTryToAppendFirst {
			outCsvFile, errOutCsv = os.OpenFile(outCsvFileName, os.O_APPEND|os.O_WRONLY, 0666)
		}
		if nil == outCsvFile {
			outCsvFile, errOutCsv = os.OpenFile(outCsvFileName, os.O_CREATE|os.O_WRONLY, 0666)
		}
		if errOutCsv != nil {
			log.Errorf("error[%s] open/create grab-file[%s]!", errOutCsv.Error(), outCsvFileName)
			return
		}
		outCsvFile.WriteString(strCsvFileContent)
		outCsvFile.Close()
	}

	// ===================================================
	log.Info("All work done!")
	return
}

func GetSubstring(src string, before string, after string) *string {
	if 0 == len(src) {
		return nil
	}

	startIdx := 0
	if 0 < len(before) {
		startIdx = strings.Index(src, before)
		if 0 > startIdx {
			return nil
		}
		startIdx += len(before)
	}

	endIdxIdx := len(src) - 1
	if 0 < len(after) {
		if startIdx > 0 {
			endIdxIdx = strings.Index(src[startIdx:], after)
			if 0 <= endIdxIdx {
				endIdxIdx += startIdx
			}
		} else {
			endIdxIdx = strings.Index(src, after)
		}
	}

	retValStr := src[startIdx:endIdxIdx]
	return &retValStr
}
