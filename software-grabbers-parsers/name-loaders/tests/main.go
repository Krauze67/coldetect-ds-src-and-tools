package main

import (
	"fmt"
	"unicode"

	"golang.org/x/text/secure/precis"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
)

func main() {
	//log.SetLevel(log.DebugLevel)

	loosecompare := precis.NewIdentifier(
		precis.AdditionalMapping(func() transform.Transformer {
			return transform.Chain(norm.NFD, transform.RemoveFunc(func(r rune) bool {
				return unicode.Is(unicode.Mn, r)
			}))
		}),
		precis.Norm(norm.NFC), // This is the default; be explicit though.
	)
	p, _ := loosecompare.String("žůžo")
	fmt.Println(p, loosecompare.Compare("žůžo", "zuzo"))
	// Prints "zuzo true"

	t := transform.Chain(norm.NFD, transform.RemoveFunc(func(r rune) bool {
		return unicode.Is(unicode.Mn, r) // Mn: nonspacing marks
	}), norm.NFC)
	result, _, _ := transform.String(t, "žůžo")
	fmt.Println(result)

	result, _, _ = transform.String(t, "AŁA")
	fmt.Println(result)

	p, _ = loosecompare.String("AŁA")
	fmt.Println(p, loosecompare.Compare("AŁA", "ALA"))
}

// type AccentsTransformer map[rune]string

// func (a AccentsTransformer) Reset() {

// }
// func (a AccentsTransformer) Transform(dst, src []byte, atEOF bool) (nDst, nSrc int, err error) {
// 	for nSrc < len(src) {
// 		// If we're at the edge, note this and return.
// 		if !atEOF && !utf8.FullRune(src[nSrc:]) {
// 			err = transform.ErrShortSrc
// 			return
// 		}
// 		r, width := utf8.DecodeRune(src[nSrc:])
// 		if r == utf8.RuneError && width == 1 {
// 			err = fmt.Errorf("Decoding error")
// 			return
// 		}
// 		if d, ok := a[r]; ok {
// 			if nDst+len(d) > len(dst) {
// 				err = transform.ErrShortDst
// 				return
// 			}
// 			copy(dst[nDst:], d)
// 			nSrc += width
// 			nDst += len(d)
// 			continue
// 		}

// 		if nDst+width > len(dst) {
// 			err = transform.ErrShortDst
// 			return
// 		}
// 		copy(dst[nDst:], src[nSrc:nSrc+width])
// 		nDst += width
// 		nSrc += width
// 	}
// 	return
// }

// func main() {
// 	//transliterations := AccentsTransformer{'Æ': "E", 'Ø': "OE"}

// 	// This map is used by RemoveAccents function to convert non-accented characters.
// 	var transliterations = AccentsTransformer{'Æ': "E", 'Ð': "D", 'Ł': "L", 'Ø': "OE", 'Þ': "Th", 'ß': "ss", 'æ': "e", 'ð': "d", 'ł': "l", 'ø': "oe", 'þ': "th", 'Œ': "OE", 'œ': "oe"}

// 	testString := "cØØl beÆns"
// 	b := transform.NewReader(bytes.NewBufferString(testString), transliterations)
// 	scanner := bufio.NewScanner(b)
// 	scanner.Split(bufio.ScanWords)
// 	for scanner.Scan() {
// 		fmt.Println("token:", scanner.Text())
// 	}
// }
