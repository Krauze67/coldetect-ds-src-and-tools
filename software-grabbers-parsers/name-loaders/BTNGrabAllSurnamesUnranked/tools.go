package main

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"strings"
	"unicode"
	"unicode/utf8"

	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
)

func GetSubstringPtr(src string, before string, after string) *string {
	emptyStr := ""
	if 0 == len(src) {
		if 0 == len(before) && 0 == len(after) {
			return &emptyStr
		}
		return nil
	}

	startIdx := 0
	if 0 < len(before) {
		startIdx = strings.Index(src, before)
		if 0 > startIdx {
			return nil
		}
		startIdx += len(before)
	}

	if startIdx == len(src) {
		if 0 == len(after) {
			return &emptyStr
		}
		return nil
	}

	endIdx := len(src) - 1
	if 0 < len(after) {
		if startIdx > 0 {
			endIdx = strings.Index(src[startIdx:], after)
		} else {
			endIdx = strings.Index(src, after)
		}

		if 0 > endIdx {
			return nil
		}
		//Correct original string Idx:
		endIdx += startIdx
	}

	retValStr := src[startIdx:endIdx]
	return &retValStr
}

func GetSubstring(src string, before string, after string) (string, error) {
	if 0 == len(src) {
		if 0 == len(before) && 0 == len(after) {
			return "", nil
		}
		return "", errors.New("empty source")
	}

	startIdx := 0
	if 0 < len(before) {
		startIdx = strings.Index(src, before)
		if 0 > startIdx {
			return "", fmt.Errorf("'before'[%s] not found", before)
		}
		startIdx += len(before)
	}

	if startIdx == len(src) {
		if 0 == len(after) {
			return "", nil
		}
		return "", fmt.Errorf("after[%s] could not be found - 'before' ends the string", after)
	}

	endIdx := len(src) - 1
	if 0 < len(after) {
		if startIdx > 0 {
			endIdx = strings.Index(src[startIdx:], after)
		} else {
			endIdx = strings.Index(src, after)
		}

		if 0 > endIdx {
			return "", fmt.Errorf("after[%s] not found", after)
		}
		//Correct original string Idx:
		endIdx += startIdx
	}

	retValStr := src[startIdx:endIdx]
	return retValStr, nil
}

func IsLatinOnlyName(str string) bool {
	const otherOk = "- '"
	for _, r := range []rune(str) {
		isSmallLatin := (r >= 'a' && r <= 'z')
		isBigLatin := (r >= 'A' && r <= 'Z')
		isOtherOk := (0 <= strings.IndexRune(otherOk, r))
		if !(isSmallLatin || isBigLatin || isOtherOk) {
			return false
		}
	}
	return true
}

func GetLatinView(str string) string {

	//1. remove diactric marks like "žůžo":
	t := transform.Chain(norm.NFD, transform.RemoveFunc(func(r rune) bool {
		return unicode.Is(unicode.Mn, r) // Mn: nonspacing marks
	}), norm.NFC)
	noDiactr, _, _ := transform.String(t, str)

	//2. map similar symbols:
	// This map is used by RemoveAccents function to convert non-accented characters.
	var transliterations = AccentsTransformer{'Æ': "E", 'Ð': "D", 'Ł': "L", 'Ø': "OE", 'Þ': "Th", 'ß': "ss", 'æ': "e", 'ð': "d", 'ł': "l", 'ø': "oe", 'þ': "th", 'Œ': "OE", 'œ': "oe"}
	b := transform.NewReader(bytes.NewBufferString(noDiactr), transliterations)
	scanner := bufio.NewScanner(b)
	if scanner.Scan() {
		if noDiactrNoAccents := scanner.Text(); IsLatinOnlyName(noDiactrNoAccents) {
			return noDiactrNoAccents
		}
	}

	return ""
}

// AccentsTransform part:
type AccentsTransformer map[rune]string

func (a AccentsTransformer) Reset() {
}

func (a AccentsTransformer) Transform(dst, src []byte, atEOF bool) (nDst, nSrc int, err error) {
	for nSrc < len(src) {
		// If we're at the edge, note this and return.
		if !atEOF && !utf8.FullRune(src[nSrc:]) {
			err = transform.ErrShortSrc
			return
		}
		r, width := utf8.DecodeRune(src[nSrc:])
		if r == utf8.RuneError && width == 1 {
			err = fmt.Errorf("Decoding error")
			return
		}
		if d, ok := a[r]; ok {
			if nDst+len(d) > len(dst) {
				err = transform.ErrShortDst
				return
			}
			copy(dst[nDst:], d)
			nSrc += width
			nDst += len(d)
			continue
		}

		if nDst+width > len(dst) {
			err = transform.ErrShortDst
			return
		}
		copy(dst[nDst:], src[nSrc:nSrc+width])
		nDst += width
		nSrc += width
	}
	return
}
