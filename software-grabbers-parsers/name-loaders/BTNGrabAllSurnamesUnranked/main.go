package main

import (
	log "github.com/sirupsen/logrus"
)

var gWorkService *WorkerService = nil

func main() {
	log.SetLevel(log.DebugLevel)

	//INIT:
	gWorkService := &WorkerService{
		Checkers:       make([]*Checker, 0),
		chProcessing:   make(chan *WorkItem),
		chResult:       make(chan *WorkItem),
		chStopWorkLoop: make(chan bool),
	}

	gWorkService.Run()

	// ===================================================
	log.Info("All work done!")
	return
}
