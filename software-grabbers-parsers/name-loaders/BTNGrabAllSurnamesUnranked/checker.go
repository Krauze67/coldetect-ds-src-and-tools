package main

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"

	"html"

	"unset.space/new-mailer/go-mailer/shared/httpclient"
)

type Checker struct {
	Id int
	//=================
	chInWorkItems chan *WorkItem
	chOutCheckRes chan *WorkItem
	chStopWork    chan bool
	wgDoneExt     *sync.WaitGroup

	//=================
	State   CheckerState
	onceRun sync.Once

	//Tech-part of check: =============
	StopReason      string
	netErrors       int
	proxyChanges    int
	bIsCheckTimeout bool
	httpClient      *httpclient.Client
}

//======================================================

const (
	cSubstrsPreTable  = "<div style=\"float:right;width:180px;margin-left:10px;\">"
	cSubstrsPostTable = "<div style=\"margin-top:15px;"

	//ccVeryFistPageProperty = 1 //for csv headers

	cUrlFormat = "https://surnames.behindthename.com/names/usage/%s/%d"
	cReferer   = "https://surnames.behindthename.com/names/usage"

	strResultsBaseDir       = "/home/df/dev/go/dfwork/nameloader/BTNGrabAllSurnamesUnranked/results"
	strSubResultsHtml       = "HTML_ALL"
	strSubResultsData       = "datasets"
	strSubResultsName       = "lastname"
	strFnameFormatCsvLatin  = "%s-lnames-NOCOUNT-LATIN.csv"
	strFnameFormatCsvUTF8   = "%s-lnames-NOCOUNT-UTF8.csv"
	strFnameFormatCsvAll    = "%s-lnames-NOCOUNT-ALLFORMS.csv"
	strFnameFormatCsvJoined = "%s-lnames-NOCOUNT-SEPJOINED.csv"
	cJoinNameDelim          = "|"

	formatOutHtml = "%s-lnames-%02d.html"

	strCsvHeaders  = "NAME,COUNT,PERCENT,PROP100K,ISNOTREALCOUNT"
	cStrFakeCounts = ",1,0.01%,10,1"
)

//======================================================

// type CheckResType int

// const (
// 	CheckRes_Finished    CheckResType = 0
// 	CheckRes_ChangeProxy CheckResType = 1
// 	CheckRes_ProxyError  CheckResType = 2
// 	//CheckRes_    CheckResType = 3
// )

func (t Checker) GetNew(id int, inchan chan *WorkItem, reschan chan *WorkItem, pwg *sync.WaitGroup) *Checker {
	ch := Checker{
		Id:              id,
		chInWorkItems:   inchan,
		chOutCheckRes:   reschan,
		chStopWork:      make(chan bool),
		wgDoneExt:       pwg,
		State:           Created,
		StopReason:      "",
		proxyChanges:    0,
		bIsCheckTimeout: false,
	}

	ch.SetState(Created)
	return &ch
}

func (t *Checker) Run() {
	t.onceRun.Do(func() {
		t.wgDoneExt.Add(1)
		t.SetState(Created)
		go t.waitWorkItemsLoop()
	})
}

func (t *Checker) SendStop() {
	t.chStopWork <- true
}

func (t *Checker) SetState(newstate CheckerState) {
	log.Debugf("[%v] state: %v->%v", t.Id, t.State.String(), newstate.String())
	//oldState := t.State
	t.State = newstate
	//t.eventListener.CheckTask_StateChanged(t, oldState, newstate)
}

func (t *Checker) waitWorkItemsLoop() {
	thisistheend := false
	for !thisistheend {
		t.SetState(Wait)
		runtime.Gosched()
		select {
		case thisistheend = <-t.chStopWork:
			t.StopReason = "Stop signal got"
			break
		case workItem := <-t.chInWorkItems:
			t.CheckItem(workItem)
			runtime.Gosched()
		}
	}
	t.wgDoneExt.Done()
	if thisistheend {
		close(t.chStopWork)
	}
}

func (t *Checker) CheckItem(workItem *WorkItem) {
	t.SetState(Checking)

	t.StopReason = ""

	t.ResetTaskVars()

	workItem.CurrNotLoadedPage = workItem.StartPage
	nStartPageIdx := workItem.StartPage

	// //All Ok, just continue
	// t.StopReason = "Check done"

	//Make dirs:
	//DS
	os.Mkdir(strResultsBaseDir, 0766)
	strDatasetsBaseDir := filepath.Join(strResultsBaseDir, strSubResultsData)
	os.Mkdir(strDatasetsBaseDir, 0766)
	//HTML content:
	strHtmlBaseDir := filepath.Join(strResultsBaseDir, strSubResultsHtml)
	os.Mkdir(strHtmlBaseDir, 0766)

	//======================================================
	hn_useragent := `User-Agent`
	//hn_content_encoding := `Content-Encoding`
	hv_Useragent := `Mozilla/5.0 (Windows NT 6.1; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0`
	//hv_encoding := `gzip,deflate`
	hdr_httpgeneral := http.Header{
		hn_useragent: {hv_Useragent},
		//hn_content_encoding: {hv_encoding},
		`Connection`:      {`keep-alive`},
		`Pragma`:          {`no-cache`},
		`Cache-Control`:   {`no-cache`},
		`Accept-Language`: {`en-US,en;q=0.5`},
	}

	var HttpConnectTimeoutSec uint = 5
	var HttpPageLoadTimeoutSec uint = 40

	netTimeoutConnect := time.Second * time.Duration(HttpConnectTimeoutSec)
	netTimeoutPageLoad := time.Second * time.Duration(HttpPageLoadTimeoutSec)

	httpClient := httpclient.GetNewClient(HttpConnectTimeoutSec, &hdr_httpgeneral)
	//	httpClient.SetProxyAndPort(t.currProxy.Address, netTimeoutConnect) //Fiddler - comment it
	//httpClient.SetProxy("", 0, netTimeoutConnect)//Fiddler uncomment it

	//sHeaderCsv := "SURNAME,COUNT,PERCENT,PROP100K\n"
	//_, err = out.WriteString(sHeaderCsv)

	//https://www.royandboucher.com/tng/surnames_statistics.php?surname=&offset=3950&tree=&test_type=&test_group=&tngpage=80
	//loop by pages:
	nProxyIdx := 0
	const nMaxProxyTries = 200
	nCurrProxyTry := 1

	t.StopReason = ""
	workItem.CheckRes = ""
	workItem.CurrNotLoadedPage = nStartPageIdx
	workItem.Reported = false

	bContinueLoading := true
	i := nStartPageIdx - 1
	for bContinueLoading && 0 == len(t.StopReason) {
		i++
		workItem.CurrNotLoadedPage = i
		// if cLASTPageIdx > 0 && i > cLASTPageIdx {
		// 	workItem.CurrNotLoadedPage = -1
		// 	bContinueLoading = false
		// 	continue
		// }

		log.Infof("Loading page %s-%d", workItem.Country, i)
		nextUrl := fmt.Sprintf(cUrlFormat, workItem.Country, i)

		//time.Sleep(time.Duration(1500) * time.Millisecond)
		//62.210.250.215:56528
		httpClient.SetProxyAndPort(GArrProxies[nProxyIdx], netTimeoutConnect)

		var addHeaders = map[string]string{
			"Accept":          "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
			"Accept-Encoding": "gzip, deflate, br",
			"Referer":         cReferer,
			"Connection":      "keep-alive",
		}
		code, body, hdrs, err := httpClient.Get(nextUrl, netTimeoutPageLoad, httpclient.MergeUserHeaders(&addHeaders))
		if nil != err || 200 != code {
			msg := fmt.Sprintf("Error loading page %s-%d: %v. HttpCode: %v", workItem.Country, i, err, code)
			log.Warningln(msg)

			bLogErrorAndSkip := true
			if 0 == code {
				nCurrProxyTry++
				if nCurrProxyTry < nMaxProxyTries {
					nProxyIdx++
					if nProxyIdx >= len(GArrProxies) {
						nProxyIdx = 0
					}
					i--
					bLogErrorAndSkip = false
				}
			}

			if bLogErrorAndSkip {
				t.StopReason = fmt.Sprintf("%s-%d ProxyChangesError", workItem.Country, i)
				//strErrLine := fmt.Sprintf("%d\n", i)
				//fErrLoadUrls.WriteString(strErrLine)
				workItem.CheckRes = t.StopReason
				t.chOutCheckRes <- workItem
				workItem.Reported = true
				bContinueLoading = false
				continue
			}
			continue
		}
		log.Debugf("Page %s-#%d loaded: code: %v, Contentlength: %v, HeadersCount %v, Error: %v\n", workItem.Country, i, code, len(body), len(hdrs), err)
		strBody := string(body)

		//check page does not exist:
		if !strings.Contains(strBody, "class=\"browsename b0\"><b><a href=\"/name/") {
			//err = t.SaveHtml(strHtmlBaseDir, workItem.Country, i, nextUrl, body)
			// <!-- End supplemental 160 ad -->
			// </div>
			// </div>
			//
			// </div>
			//
			//
			// <div style="margin-top:15px;margin-bottom:15px;">
			substr, err := GetSubstring(strBody, cSubstrsPreTable, cSubstrsPostTable)
			if nil != err {
				t.StopReason = fmt.Sprintf("%s-%d error[%s] checking the page 'after last' - structure error #1", workItem.Country, i, err.Error())
				workItem.CheckRes = t.StopReason
				t.chOutCheckRes <- workItem
				workItem.Reported = true
				bContinueLoading = false
				continue
			}
			//check it is empty page instead:
			substr, err = GetSubstring(substr, "</div>", "</div>\n\n\n")

			if nil != err {
				t.StopReason = fmt.Sprintf("%s-%d error[%s] checking the page 'after last' - structure error #2", workItem.Country, i, err)
			} else if "\n\n" != substr && "\n</div>\n\n" != substr {
				t.StopReason = fmt.Sprintf("%s-%d error['invalid structure'] checking the page 'after last' - structure error #3", workItem.Country, i)
			} else { //All OK - this is the 'page after last'
				t.StopReason = ""
				workItem.CurrNotLoadedPage = -1
			}
			workItem.CheckRes = t.StopReason
			t.chOutCheckRes <- workItem
			workItem.Reported = true
			bContinueLoading = false
			continue
		}

		//=====================================================
		err = t.SaveHtml(strHtmlBaseDir, workItem.Country, i, nextUrl, body)
		if err != nil {
			t.StopReason = fmt.Sprintf("%s-%d error[%s] saving out HTML file", workItem.Country, i, err.Error())
			workItem.CheckRes = t.StopReason
			t.chOutCheckRes <- workItem
			workItem.Reported = true
			bContinueLoading = false
			continue
		}

		//=====================================================

		// now parse page and save CSV format:
		strAllTableText, err := GetSubstring(strBody, cSubstrsPreTable, cSubstrsPostTable)
		if nil != err {
			// strErrLine := fmt.Sprintf("HTML STRUCTURE ERROR - no table:\t%d,\n", i)
			// log.Warning(strErrLine)
			// fErrLoadUrls.WriteString(strErrLine)
			t.StopReason = fmt.Sprintf("%s-%d HTML STRUCTURE ERROR [%v] - no table:\t%d,\n", workItem.Country, i, err, i)
			workItem.CheckRes = t.StopReason
			t.chOutCheckRes <- workItem
			workItem.Reported = true
			bContinueLoading = false
			continue //will break check loop
		}

		delimNr1 := "<div class=\"browsename b0\">"
		arrAllNamesInfo := strings.Split(strAllTableText, delimNr1)

		nNamesFoundOnPage := 0
		//checkPart1_2 := "<td align=\"right\">&nbsp;&nbsp;"
		strParsingErrorStr := ""
		strPartContainsNameInfoProperty := "href=\"/name/"
		for _, nameInfo := range arrAllNamesInfo {
			if !bContinueLoading {
				break
			}
			if !strings.Contains(nameInfo, strPartContainsNameInfoProperty) {
				continue
			}

			contentLatin, contentUnicode, contentNamesAllForms, contentNamesJoined, err := t.ParseAllNameInfo(&nameInfo)

			if nil != err {
				strParsingErrorStr = fmt.Sprintf("%s-%d, name #%d parsing error[%s]", workItem.Country, i, nNamesFoundOnPage+1, err.Error())
				bContinueLoading = false
				continue
			}

			nNamesFoundOnPage++

			t.SaveToCsv(strDatasetsBaseDir, workItem.Country, i, contentLatin, contentUnicode, contentNamesAllForms, contentNamesJoined)
			//strCsvFileContent += fmt.Sprintf("%s,%s,%s\n", strName, strGender, strCount)
			//strCsvFileContent += fmt.Sprintf("%s,%s\n", *strName, *strCount)
			//===========================================

		} //loop by names found on page:

		//===========================================
		//Check parsing error:
		if 0 != len(strParsingErrorStr) {
			t.StopReason, workItem.CheckRes = strParsingErrorStr, strParsingErrorStr
			t.chOutCheckRes <- workItem
			workItem.Reported = true
			bContinueLoading = false
			continue //will break check loop
		}
		//===========================================

		if 0 >= nNamesFoundOnPage {
			// msg := fmt.Sprintf("HTML parsing error! 0 names found:\t%d\n", i)
			// log.Warning(msg)
			// fErrLoadUrls.WriteString(msg)
			// continue //load next url
			t.StopReason = fmt.Sprintf("%s-%d HTML parsing error! 0 names found:\t%d\n", workItem.Country, i, i)
			workItem.CheckRes = t.StopReason
			t.chOutCheckRes <- workItem
			workItem.Reported = true
			bContinueLoading = false
			continue //will break check loop
		} else {
			log.Infof("%s-%d Found %d names\n", workItem.Country, i, nNamesFoundOnPage)
		}

	}

	if !workItem.Reported {
		if 0 == len(workItem.CheckRes) {
			workItem.CheckRes = t.StopReason
		}
		if 0 == len(workItem.CheckRes) {
			workItem.CurrNotLoadedPage = -1
		}
		t.chOutCheckRes <- workItem
		workItem.Reported = true
	}
	t.SetState(Wait)

}

func (t *Checker) ResetTaskVars() {
	t.StopReason = ""
	t.netErrors = 0
	t.proxyChanges = 0
	t.bIsCheckTimeout = false
}

func (t *Checker) SaveHtml(basehtmldir string, country string, page int, currUrl string, body []byte) error {
	//=====================================================
	htmlCountryDirName := filepath.Join(basehtmldir, country)
	os.Mkdir(htmlCountryDirName, 0766)
	htmlFileName := fmt.Sprintf(formatOutHtml, country, page)
	htmlFullFileName := filepath.Join(htmlCountryDirName, htmlFileName)

	outHtml, err := os.OpenFile(htmlFullFileName, os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		return fmt.Errorf("creating out HTML file[%s] error: %v!", htmlFullFileName, err)
	}
	_, err = outHtml.WriteString(fmt.Sprintf("<!--\n%s\n-->\n", currUrl))
	if err != nil {
		return err
	}
	_, err = outHtml.Write(body)
	if err != nil {
		return err
	}
	outHtml.Close()
	return nil
}

func (t *Checker) SaveToCsv(dsdir string, country string, page int, contentLatin string, contentunicode string, contentall string, contentjoined string) error {

	// strFnameFormatCsvLatin = "%s-fnames-NOCOUNT-LATIN.csv"
	// strFnameFormatCsvUTF8 = "%s-fnames-NOCOUNT-UTF8.csv"
	// strFnameFormatCsvAll = "%s-fnames-NOCOUNT-ALLFORMS.csv"
	// strFnameFormatCsvJoined = "%s-fnames-NOCOUNT-SEPJOINED.csv"
	if 0 < len(contentLatin) {
		if err := t.saveToCsv_format(strFnameFormatCsvLatin, dsdir, country, page, contentLatin); nil != err {
			return err
		}
	}
	//=====================================================
	if 0 < len(contentunicode) {
		if err := t.saveToCsv_format(strFnameFormatCsvUTF8, dsdir, country, page, contentunicode); nil != err {
			return err
		}
	}
	//=====================================================
	if 0 < len(contentall) {
		if err := t.saveToCsv_format(strFnameFormatCsvAll, dsdir, country, page, contentall); nil != err {
			return err
		}
	}
	//=====================================================
	if 0 < len(contentjoined) {
		if err := t.saveToCsv_format(strFnameFormatCsvJoined, dsdir, country, page, contentjoined); nil != err {
			return err
		}
	}
	//=====================================================

	return nil
}

func (t *Checker) saveToCsv_format(filenameformat string, dsdir string, country string, page int, content string) error {
	//=====================================================
	countryDir := filepath.Join(dsdir, country)
	os.Mkdir(countryDir, 0766)
	countryDirFname := filepath.Join(countryDir, strSubResultsName)
	os.Mkdir(countryDirFname, 0766)
	//===========================================
	csvName := fmt.Sprintf(filenameformat, country)
	outCsvFileName := filepath.Join(countryDirFname, csvName)
	var (
		outCsvFile *os.File
		errOutCsv  error
	)
	bSaveHeadersFirst := false

	outCsvFile, errOutCsv = os.OpenFile(outCsvFileName, os.O_APPEND|os.O_WRONLY, 0666)
	if nil == outCsvFile {
		bSaveHeadersFirst = true
		outCsvFile, errOutCsv = os.OpenFile(outCsvFileName, os.O_CREATE|os.O_WRONLY, 0666)
	}
	if errOutCsv != nil {
		return fmt.Errorf("error[%s] open/create csv-file[%s]", errOutCsv.Error(), outCsvFileName)
	}
	if bSaveHeadersFirst {
		_, err := outCsvFile.WriteString(strCsvHeaders + "\n")
		if nil != err {
			return err
		}
	}
	_, err := outCsvFile.WriteString(content)
	if nil != err {
		return err
	}
	outCsvFile.Close()

	return nil
}

func (t *Checker) ParseAllNameInfo(pNameInfo *string) (string, string, string, string, error) {

	//create contents:
	strCsvContentLatin := ""
	strCsvContentUTF8 := ""
	strCsvContentAllForms := ""
	strCsvContentJoined := ""

	if nil == pNameInfo {
		panic("Internal error")
	}

	//step 1: get a part containing all info:
	strAllNameInfo, err := GetSubstring(*pNameInfo, "href=\"/name/", "href=\"/names/")
	if nil != err {
		return "", "", "", "", fmt.Errorf("a whole 'NameInfo' part has not been found")
	}

	// //step 2: get gender:
	// //<span class="fem">
	// //<span class="masc">
	// strGender := ""

	// isFem := strings.Contains(strAllNameInfo, "<span class=\"fem\">")
	// isMasc := strings.Contains(strAllNameInfo, "<span class=\"masc\">")
	// if isFem && isMasc {
	// 	strGender = "U"
	// } else if isFem {
	// 	strGender = "F"
	// } else if isMasc {
	// 	strGender = "M"
	// } else {
	// 	return "", "", "", "", fmt.Errorf("'Gender' property has not been found")
	// }
	strAfterName := cStrFakeCounts + "\n"

	//step 3:
	strNameHtml := ""
	strNameHtml, err = GetSubstring(strAllNameInfo, ">", "<")
	strNameHtml = strings.Trim(strNameHtml, " \n\t")
	if nil != err {
		return "", "", "", "", fmt.Errorf("'Name' part has not been found")
	}
	if 0 == len(strNameHtml) {
		return "", "", "", "", fmt.Errorf("'Name' part is empty")
	}

	strNameUTF8 := html.UnescapeString(strNameHtml)
	strNameLatinOnly := GetLatinView(strNameUTF8)

	strJoinedNames := ""
	//create contents:
	if 0 < len(strNameLatinOnly) {
		strCsvContentLatin += strNameLatinOnly + strAfterName
		strCsvContentAllForms += strNameLatinOnly + strAfterName
		strJoinedNames += strNameLatinOnly + cJoinNameDelim
	}
	//Check if was actually NOT latin:
	if 0 != strings.Compare(strNameUTF8, strNameLatinOnly) {
		strCsvContentUTF8 += strNameUTF8 + strAfterName
		strCsvContentAllForms += strNameUTF8 + strAfterName
		strJoinedNames += strNameUTF8 + cJoinNameDelim
	}
	//step 4: other views
	//empty:
	//</a></b> &nbsp; <span class="masc">m</span> &nbsp; <span class="info">
	//comma-separated other views(UNICODE):
	//</a></b> &nbsp; &#1593;&#1575;&#1604;&#1610; &nbsp; <span class="masc">
	strOtherViews := ""
	strOtherViews, err = GetSubstring(strAllNameInfo, "</a></b> &nbsp;", "<span class=")
	strOtherViews, err = GetSubstring(strOtherViews, " ", "&nbsp;")
	strOtherViews = strings.Trim(strOtherViews, " \n\t")
	if 0 != len(strOtherViews) {
		otherViews := strings.Split(strOtherViews, ",")
		for _, otherName := range otherViews {
			nextView := html.UnescapeString(strings.Trim(otherName, " \n\t"))
			strCsvContentUTF8 += nextView + strAfterName
			strCsvContentAllForms += nextView + strAfterName
			strJoinedNames += nextView + cJoinNameDelim
		}
	}

	strJoinedNames = strJoinedNames[:len(strJoinedNames)-1]
	strCsvContentJoined = strJoinedNames + strAfterName

	return strCsvContentLatin, strCsvContentUTF8, strCsvContentAllForms, strCsvContentJoined, nil
}
