package main

import (
	"fmt"
	"os"
	"runtime"
	"sync"
	"sync/atomic"

	"time"

	log "github.com/sirupsen/logrus"
)

const (
	RoutinesCount = 10
)

type WorkerService struct {
	Checkers []*Checker

	chProcessing chan *WorkItem

	//=================
	wgShutdownAllCheckers sync.WaitGroup

	//=================
	nWiInCheck     int64
	chResult       chan *WorkItem
	wgWorkLoopDone sync.WaitGroup
	chStopWorkLoop chan bool

	//=======
	fLoadResult *os.File
}

func (t *WorkerService) WorkLoop() {
	continueLoop := true
	for continueLoop {
		select {
		case <-t.chStopWorkLoop: //loop stop condition
			continueLoop = false
			log.Debugln("WorkLoop got Stop signal")
			break
		// case <-time.After(time.Duration(cWSrvSaveWorkstateSec) * time.Second):
		// 	log.Debugln("Save workstate timer signalled!")
		// 	if nil == t.task {
		// 		log.Debugln("Save workstate timer - nothing to save")
		// 		continue
		// 	}

		// 	log.Debugln("Saving current work state.")
		// 	err := t.task.SaveWorkState()
		// 	if nil != err {
		// 		log.Errorf("Error saving current work state! %v", err)
		// 	} else {
		// 		log.Debugln("Current work state saved correctly")
		// 	}
		// 	runtime.Gosched()
		case workItem := <-t.chResult:
			log.Infoln("Checker result got: %v", *workItem)

			atomic.AddInt64(&t.nWiInCheck, -1)
			//if true {
			t.fLoadResult.WriteString(fmt.Sprintf("%s\t%s\t%d\t%d\t	WorkItem{Country: \"%s\", StartPage: %d},\n",
				workItem.CheckRes, workItem.Country,
				workItem.StartPage, workItem.CurrNotLoadedPage,
				workItem.Country, workItem.CurrNotLoadedPage))
			//}
			// t.task.SaveResult(workItem)
			runtime.Gosched()
		}
	}
	t.wgWorkLoopDone.Done()
}

func (t *WorkerService) Run() {

	t.wgWorkLoopDone.Add(1)
	log.Infoln("Starting working loop...")
	go t.WorkLoop()
	log.Infoln("    Working loop started")

	t.TaskExecute()

	t.Shutdown()
}

func (t *WorkerService) TaskExecute() {
	var err error

	//Create results file:
	fLoadResultName := "_LoadResults.csv"
	t.fLoadResult, err = os.OpenFile(fLoadResultName, os.O_APPEND|os.O_WRONLY, 0666)
	if err != nil {
		t.fLoadResult, err = os.OpenFile(fLoadResultName, os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			fmt.Printf("error[%s] open/create errors file[%s]!", err.Error(), fLoadResultName)
			return
		}
	}

	// Create checkers
	t.Checkers = make([]*Checker, RoutinesCount)
	for i := 0; i < RoutinesCount; i++ {
		newChecker := Checker{}.GetNew(i+1, t.chProcessing, t.chResult, &t.wgShutdownAllCheckers)
		newChecker.Run()
		t.Checkers[i] = newChecker
	}

	t.nWiInCheck = 0
	for i := 0; i < len(GWorkInfo); i++ {
		workItem := &GWorkInfo[i]
		atomic.AddInt64(&t.nWiInCheck, 1)
		t.chProcessing <- workItem
		runtime.Gosched()
	}

	//wait all checkers done
	for t.nWiInCheck > 0 {
		runtime.Gosched()
		time.Sleep(time.Duration(10) * time.Second)
	}
}

func (t *WorkerService) Shutdown() {
	t.SendStopCheckers()
	t.WaitCheckersStopped()

	t.chStopWorkLoop <- true
	t.wgWorkLoopDone.Wait()

	close(t.chProcessing)
	close(t.chStopWorkLoop)
	close(t.chResult)
	t.Unload()
}

func (t *WorkerService) Unload() {

	log.Println("Unloading service...")
	t.fLoadResult.Close()

	log.Println("Service unloaded")
}

func (t *WorkerService) SendStopCheckers() {
	for _, chk := range t.Checkers {
		chk.SendStop()
	}
}

func (t *WorkerService) WaitCheckersStopped() {
	t.wgShutdownAllCheckers.Wait()
}
