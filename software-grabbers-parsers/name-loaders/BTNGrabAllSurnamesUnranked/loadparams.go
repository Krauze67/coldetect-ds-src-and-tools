package main

type WorkItem struct {
	Country           string
	StartPage         int
	CurrNotLoadedPage int
	CheckRes          string
	Reported          bool
}

var GWorkInfo = []WorkItem{
	//https://www.behindthename.com/names/usage/albanian/9
	//https://www.behindthename.com/names/usage/japanese
	//WorkItem{Country: "bulgarian", StartPage: 2},
	//Europe
	WorkItem{Country: "albanian", StartPage: 1},
	WorkItem{Country: "basque", StartPage: 1},
	WorkItem{Country: "bulgarian", StartPage: 1},
	WorkItem{Country: "catalan", StartPage: 1},
	WorkItem{Country: "cornish", StartPage: 1},
	WorkItem{Country: "croatian", StartPage: 1},
	WorkItem{Country: "czech", StartPage: 1},
	WorkItem{Country: "danish", StartPage: 1},
	WorkItem{Country: "dutch", StartPage: 1},
	WorkItem{Country: "english", StartPage: 1},
	WorkItem{Country: "finnish", StartPage: 1},
	WorkItem{Country: "french", StartPage: 1},
	WorkItem{Country: "frisian", StartPage: 1},
	WorkItem{Country: "galician", StartPage: 1},
	WorkItem{Country: "german", StartPage: 1},
	WorkItem{Country: "greek", StartPage: 1},
	WorkItem{Country: "hungarian", StartPage: 1},
	WorkItem{Country: "icelandic", StartPage: 1},
	WorkItem{Country: "irish", StartPage: 1},
	WorkItem{Country: "italian", StartPage: 1},
	WorkItem{Country: "latvian", StartPage: 1},
	WorkItem{Country: "lithuanian", StartPage: 1},
	WorkItem{Country: "macedonian", StartPage: 1},
	WorkItem{Country: "norwegian", StartPage: 1},
	WorkItem{Country: "occitan", StartPage: 1},
	WorkItem{Country: "polish", StartPage: 1},
	WorkItem{Country: "portuguese", StartPage: 1},
	WorkItem{Country: "romanian", StartPage: 1},
	WorkItem{Country: "russian", StartPage: 1},
	WorkItem{Country: "scottish", StartPage: 1},
	WorkItem{Country: "serbian", StartPage: 1},
	WorkItem{Country: "slovak", StartPage: 1},
	WorkItem{Country: "slovene", StartPage: 1},
	WorkItem{Country: "spanish", StartPage: 1},
	WorkItem{Country: "swedish", StartPage: 1},
	WorkItem{Country: "ukrainian", StartPage: 1},
	WorkItem{Country: "welsh", StartPage: 1},
	//Worldwide:
	WorkItem{Country: "african", StartPage: 1},
	WorkItem{Country: "arabic", StartPage: 1},
	WorkItem{Country: "armenian", StartPage: 1},
	WorkItem{Country: "chinese", StartPage: 1},
	WorkItem{Country: "indian", StartPage: 1},
	WorkItem{Country: "iranian", StartPage: 1},
	WorkItem{Country: "japanese", StartPage: 1},
	WorkItem{Country: "jewish", StartPage: 1},
	WorkItem{Country: "khmer", StartPage: 1},
	WorkItem{Country: "korean", StartPage: 1},
	WorkItem{Country: "thai", StartPage: 1},
	WorkItem{Country: "turkish", StartPage: 1},
	WorkItem{Country: "vietnamese", StartPage: 1},
}
