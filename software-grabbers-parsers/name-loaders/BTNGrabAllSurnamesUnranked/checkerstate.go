package main

type CheckerState int

const (
	Created CheckerState = iota
	Wait
	GetProxy
	ProxyGot
	Checking
	ReleasingProxy
	Reporting
	Destroyed
)

func (state CheckerState) String() string {
	switch state {
	case Created:
		return "Created"
	case Wait:
		return "Wait"
	case GetProxy:
		return "GetProxy"
	case ProxyGot:
		return "ProxyGot"
	case Checking:
		return "Checking"
	case ReleasingProxy:
		return "ReleasingProxy"
	case Reporting:
		return "Reporting"
	case Destroyed:
		return "Destroyed"
	}

	return "unknown"
}
