package main

import (
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"strconv"

	"path/filepath"

	log "github.com/sirupsen/logrus"
	"unset.space/new-mailer/go-mailer/shared/httpclient"
)

const (
	cLASTNotLoadedPageIdx int = 0
)

func main() {
	log.SetLevel(log.DebugLevel)
	//===========================
	hn_useragent := `User-Agent`
	//hn_content_encoding := `Content-Encoding`

	hv_Useragent := `Mozilla/5.0 (Windows NT 6.1; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0`
	//hv_encoding := `gzip,deflate`

	hdr_httpgeneral := http.Header{
		hn_useragent: {hv_Useragent},
		//hn_content_encoding: {hv_encoding},
		`Connection`:      {`keep-alive`},
		`Pragma`:          {`no-cache`},
		`Cache-Control`:   {`no-cache`},
		`Accept-Language`: {`en-US,en;q=0.5`},
	}
	nStartPageIdx := cLASTNotLoadedPageIdx

	var HttpConnectTimeoutSec uint = 5
	var HttpPageLoadTimeoutSec uint = 30

	netTimeoutConnect := time.Second * time.Duration(HttpConnectTimeoutSec)
	netTimeoutPageLoad := time.Second * time.Duration(HttpPageLoadTimeoutSec)

	httpClient := httpclient.GetNewClient(HttpConnectTimeoutSec, &hdr_httpgeneral)
	//	httpClient.SetProxyAndPort(t.currProxy.Address, netTimeoutConnect) //Fiddler - comment it
	//httpClient.SetProxy("", 0, netTimeoutConnect)//Fiddler uncomment it

	//Create results file:
	fErrLoadName := "_ErrLoadingUrls.csv"
	fErrLoadUrls, err := os.OpenFile(fErrLoadName, os.O_APPEND|os.O_WRONLY, 0666)
	if err != nil {
		fErrLoadUrls, err = os.OpenFile(fErrLoadName, os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			fmt.Printf("error[%s] open/create errors file[%s]!", err.Error(), fErrLoadName)
			return
		}
	}
	defer fErrLoadUrls.Close()
	//sHeaderCsv := "SURNAME,COUNT,PERCENT,PROP100K\n"
	//_, err = out.WriteString(sHeaderCsv)

	//https://www.behindthename.com/top/list.php?region=italy&year=2011&sort=&display=max
	sUrlFormat := "https://www.behindthename.com/top/list.php?region=%s&year=%s&sort=&display=max"
	//loop by pages:
	nProxyIdx := 0
	const nMaxProxyTries = 20
	nCurrProxyTry := 1
	strCsvHeaders := "NAME,GENDER,COUNT,PERCENTAGE,PROP100K"

	for i := nStartPageIdx; i < len(GArrUrls); i++ {
		country := GArrUrls[i].Country
		yr := GArrUrls[i].Year

		log.Infof("Loading page %d of %d[%s-%s]", i+1, len(GArrUrls), country, yr)
		nextUrl := fmt.Sprintf(sUrlFormat, country, yr)

		//time.Sleep(time.Duration(1500) * time.Millisecond)
		//62.210.250.215:56528
		httpClient.SetProxyAndPort(GArrProxies[nProxyIdx], netTimeoutConnect)

		var addHeaders = map[string]string{
			"Accept":          "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
			"Accept-Encoding": "gzip, deflate, br",
			"Referer":         "https://www.behindthename.com/top/",
			"Connection":      "keep-alive",
		}
		code, body, hdrs, err := httpClient.Get(nextUrl, netTimeoutPageLoad, httpclient.MergeUserHeaders(&addHeaders))
		if nil != err || 200 != code {
			msg := fmt.Sprintf("Error loading page #%d: %v. HttpCode: %v", i, err, code)
			log.Warningln(msg)

			bLogErrorAndSkip := true
			if 0 == code {
				nCurrProxyTry++
				if nCurrProxyTry < nMaxProxyTries {
					nProxyIdx++
					if nProxyIdx >= len(GArrProxies) {
						nProxyIdx = 0
					}
					i--
					bLogErrorAndSkip = false
				}
			}

			if bLogErrorAndSkip {
				strErrLine := fmt.Sprintf("	TCountryYear{\"%s\", \"%s\"},\n", country, yr)
				fErrLoadUrls.WriteString(strErrLine)
			}
			continue
		}
		log.Debugf("Page #%d[%s-%s] loaded: code: %v, Contentlength: %v, HeadersCount^ %v, Error: %v\n", i, country, yr, code, len(body), len(hdrs), err)

		//=====================================================
		outFormatHtml := "/home/df/dev/go/dfwork/nameloader/BTNGrabAllTopLists/HTML_All/btnames_%03d_%s-%s.html"
		strHtmlOut := fmt.Sprintf(outFormatHtml, i, country, yr)
		outHtml, err := os.OpenFile(strHtmlOut, os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			fmt.Printf("error[%s] creating out HTML file[%s]!", err.Error(), strHtmlOut)
			strErrLine := fmt.Sprintf("	TCountryYear{\"%s\", \"%s\"},\n", country, yr)
			fErrLoadUrls.WriteString(strErrLine)
			continue
		}
		_, err = outHtml.Write(body)
		outHtml.Close()
		//=====================================================

		// now parse page and save CSV format:
		// <form action="http://www.wordle.net/advanced" method="POST" id="wordle" target="_blank">
		//     <textarea name="colorwordcounts" style="display:none">
		// Emma:709:FF0000
		// Louise:548:FF0000
		// Elise:479:FF0000strconv.ToUpper(
		// Marie:446:FF0000
		// ...
		// Luca:290:0000FF
		// Alexander:283:0000FF
		// ...
		//     </textarea>
		//     <input name="bg" type="hidden" value="FFFFFF">

		pAllTableText := GetSubstring(string(body), "textarea name=\"colorwordcounts\" style=\"display:none\">", "</textarea>")
		if nil == pAllTableText {
			strErrLine := fmt.Sprintf("HTML STRUCTURE ERROR - no table:\t{\"%s\", \"%s\"},\n", country, yr)
			log.Warning(strErrLine)
			fErrLoadUrls.WriteString(strErrLine)
			continue
		}
		strTable := strings.ToUpper(strings.Trim(*pAllTableText, "\t\n "))

		delimNr1 := "\n"
		arrNamesWithCounts := strings.Split(strTable, delimNr1)

		//log.Infof("Parsing page %d of %d...", i+1, nLastPageNum-1)
		nNamesFoundOnPage := 0
		//checkPart1_2 := "<td align=\"right\">&nbsp;&nbsp;"
		strParsingErrorStr := ""
		strCsvFileContent := strCsvHeaders + "\n"
		for _, parts1 := range arrNamesWithCounts {
			allNameInfoParts := strings.Split(parts1, ":")
			if 3 != len(allNameInfoParts) {
				strParsingErrorStr = fmt.Sprintf("HTML STRUCTURE ERROR - %d parts of the name[%s]:\t{\"%s\", \"%s\"},\n", len(allNameInfoParts), parts1, country, yr)
				break
			}

			strGender := ""
			strName := allNameInfoParts[0]
			nCount, err := strconv.Atoi(allNameInfoParts[1])
			if nil != err || 0 >= nCount {
				strParsingErrorStr = fmt.Sprintf("HTML 'Count' parsing error[%v]:\t{\"%s\", \"%s\"},\n", err, country, yr)
				break
			}
			strCount := allNameInfoParts[1]
			//Gender:
			strGender_F := "FF0000"
			strGender_M := "0000FF"
			if strings.Contains(allNameInfoParts[2], strGender_F) {
				strGender = "F"
			} else if strings.Contains(allNameInfoParts[2], strGender_M) {
				strGender = "M"
			} else {
				strParsingErrorStr = fmt.Sprintf("HTML 'Gender' parsing error! Gender: %s :\t{\"%s\", \"%s\"},\n", allNameInfoParts[2], country, yr)
				break
			}
			nNamesFoundOnPage++
			strCsvFileContent += fmt.Sprintf("%s,%s,%s\n", strName, strGender, strCount)
			//===========================================

		} //loop by names found on page:

		//===========================================
		//Check parsing error:
		if 0 != len(strParsingErrorStr) {
			log.Warning(strParsingErrorStr)
			fErrLoadUrls.WriteString(strParsingErrorStr)
			strParsingErrorStr = ""
			continue //load next url
		}
		//===========================================
		//log.Infof("Page %d parsed, %d names more got. Total found: %d", i+1, nNamesFoundOnPage, nNamesFoundTotal)
		if 0 >= nNamesFoundOnPage {
			msg := fmt.Sprintf("HTML parsing error! 0 names found:\t{\"%s\", \"%s\"},\n", country, yr)
			log.Warning(msg)
			fErrLoadUrls.WriteString(msg)
			continue //load next url
		} else {
			msg := fmt.Sprintf("Found %d names on page %s-%s", nNamesFoundOnPage, country, yr)
			log.Infoln(msg)
		}

		//===========================================
		//otherwise - save results
		dsDir := "/home/df/dev/go/dfwork/nameloader/BTNGrabAllTopLists/_datasets"
		os.Mkdir(dsDir, 0766)
		countryDir := filepath.Join(dsDir, country)
		os.Mkdir(countryDir, 0766)
		countryDirFname := filepath.Join(countryDir, "firstname")
		os.Mkdir(countryDirFname, 0766)
		//===========================================
		countryAllYearsDir := filepath.Join(countryDirFname, "allyears")
		os.Mkdir(countryAllYearsDir, 0766)
		//===========================================
		countryYearFileName := filepath.Join(countryAllYearsDir, country+"_"+yr+".csv")
		countryYearFile, err := os.OpenFile(countryYearFileName, os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			log.Errorf("error[%s] open/create grab-file[%s]!", err.Error(), countryYearFileName)
			return
		}
		countryYearFile.WriteString(strCsvFileContent)
		countryYearFile.Close()
	}

	// ===================================================
	log.Info("All work done!")
	return
}

func GetSubstring(src string, before string, after string) *string {
	if 0 == len(src) {
		return nil
	}

	startIdx := 0
	if 0 < len(before) {
		startIdx = strings.Index(src, before)
		if 0 > startIdx {
			return nil
		}
		startIdx += len(before)
	}

	endIdxIdx := len(src) - 1
	if 0 < len(after) {
		if startIdx > 0 {
			endIdxIdx = strings.Index(src[startIdx:], after)
			if 0 <= endIdxIdx {
				endIdxIdx += startIdx
			}
		} else {
			endIdxIdx = strings.Index(src, after)
		}
	}

	retValStr := src[startIdx:endIdxIdx]
	return &retValStr
}
