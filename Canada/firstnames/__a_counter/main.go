package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/Krauze67/flib/stringtools"

	log "github.com/sirupsen/logrus"
)

const (
	DataDir = "sourcedata"
)

type NameInfo struct {
	Name       string
	Gender     string
	CountTotal int
}

var GAllNames map[string]*NameInfo
var GTotal int
var GTotalM int
var GTotalF int

//===============================
var GAllNamesSlice []*NameInfo

type TResSortByCountDesc []*NameInfo

func (t TResSortByCountDesc) Len() int {
	return len(t)
}
func (t TResSortByCountDesc) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}
func (t TResSortByCountDesc) Less(i, j int) bool {
	return t[i].CountTotal > t[j].CountTotal
}

func main() {

	GAllNames = make(map[string]*NameInfo)

	// subfiles, _ := ioutil.ReadDir(DataDir)
	// for _, subf := range subfiles {
	// 	if !subf.IsDir() && (strings.Contains(subf.Name(), ".txt") || strings.Contains(subf.Name(), ".csv")) {
	//fullfilepath := filepath.Join(DataDir, subf.Name())
	fullfilepath := filepath.Join(DataDir, "2008_Boys.txt")
	outFileErrors := filepath.Join(DataDir, "2008_Boys-err.txt")

	file, err := os.Open(fullfilepath)
	if nil != err {
		//continue
		log.Error(err.Error())
		return
	}
	foutErr, err := os.Create(outFileErrors)
	if nil != err {
		log.Errorf("Error opening file: %s", err.Error())
		return
	}
	defer foutErr.Close()

	// # Baby Girl Names # Baby Girl Names # Baby Girl Names
	// 1 Aafaf 1 Abigal 2 Addysen
	scanner := bufio.NewScanner(bufio.NewReader(file))
	nLine := -1
	for scanner.Scan() {
		nLine++
		log.Infof("Processing lint %s...", nLine)
		line := strings.ToUpper(strings.Trim(scanner.Text(), "\n\r\t "))
		if 0 == len(line) ||
			strings.Contains(line, "# ") ||
			strings.Contains(line, "REGISTERED ") ||
			strings.Contains(line, "PAGE ") ||
			strings.Contains(line, "#") ||
			strings.Contains(line, "PAGE ") {
			continue
		}

		fields := strings.Split(line, " ")
		// if 3 > len(fields) {
		// 	fields = strings.Split(line, ",")
		// 	if 3 > len(fields) {
		// 		log.Infof("File %s has invalid format of line %d[%s]\n", fullfilepath, nLine, line)
		// 		continue
		// 	}
		// }

		if 0 > strings.IndexByte(stringtools.NumbersDec, line[0]) {
			foutErr.WriteString(line + "\n")
			continue
		}

		names_found := 0
		nLineTotal := 0
		lineNames := make(map[string]*NameInfo)
		for i := 0; i <= len(fields)-2; i += 2 {
			count_field := fields[i]
			if 0 > strings.IndexByte(stringtools.NumbersDec, count_field[0]) {
				foutErr.WriteString(line + "\n")
				break
			}
			currCount, err := strconv.Atoi(count_field)
			if nil != err {
				replaced := strings.Replace(count_field, ",", "", -1)
				replaced = strings.Replace(replaced, " ", "", -1)
				currCount, err = strconv.Atoi(replaced)
				if nil != err {
					log.Errorf("Err converting count %s to int", count_field)
					continue
				}
			}
			currName := strings.Trim(fields[i+1], ",.")
			tryNum, errNum := strconv.Atoi(currName)
			if nil == errNum && tryNum > 0 {
				log.Warnf("Line #%d: extra number skipped", nLine)
				i--
				continue
			}
			if 2 >= len(currName) {
				foutErr.WriteString("[TOO SHORT NAME]\t" + line + "\n")
				continue
			}
			// if !stringtools.IsLatinOnly(currName, "-") {
			// 	latinName := stringtools.GetLatinView(currName, "-'`")
			// 	currName += "|" + latinName
			// 	foutErr.WriteString(line + "\n")
			// 	continue
			// }
			currGender := "M"

			names_found++

			currInfo := GAllNames[currName]
			if nil == currInfo {
				currInfo = &NameInfo{Name: currName}
				lineNames[currName] = currInfo
				//GAllNames[currName] = currInfo
			} else {
				if !true {
					log.Infof("Dup found")
				}
			}

			currInfo.CountTotal += currCount
			// if "U" != currInfo.Gender {
			// 	if 0 != len(currInfo.Gender) && 0 != strings.Compare(currInfo.Gender, currGender) {
			// 		currInfo.Gender = "U"
			// 	} else {
			// 		currInfo.Gender = currGender
			// 	}
			// } else {
			// 	fmt.Print("")
			// }
			if 0 == len(currInfo.Gender) {
				currInfo.Gender = currGender
			}

			nLineTotal += currCount
			// if 0 == strings.Compare(currGender, "M") {
			// 	GTotalM += currCount
			// } else if 0 == strings.Compare(currGender, "F") {
			// 	GTotalF += currCount
			// } else {
			// 	log.Errorf("File %s line %d Invalid gender %s", fullfilepath, nLine, currGender)
			// 	continue
			// }
		}

		if 0 == names_found {
			foutErr.WriteString(line + "\n")
		} else {
			GTotal += nLineTotal
			for key, val := range lineNames {
				GAllNames[key] = val
			}
		}
	} //loop by file
	err = scanner.Err()
	if nil != err && io.EOF != err {
		log.Errorf("Err %v reading file %s", err, fullfilepath)
		//continue
	}
	// 	}
	// }
	// subfiles = nil

	//==================================
	for _, pNameInfo := range GAllNames {
		GAllNamesSlice = append(GAllNamesSlice, pNameInfo)
	}
	GAllNames = make(map[string]*NameInfo)

	sort.Sort(TResSortByCountDesc(GAllNamesSlice))

	fName := "_OutTotalStatistics.csv"
	fNameM := "_OutTotalStatisticsM.csv"
	fNameF := "_OutTotalStatisticsF.csv"
	out, err := os.OpenFile(fName, os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Errorf("error[%s] creating out file[%s]!", err.Error(), fName)
		return
	}
	defer out.Close()
	outM, err := os.OpenFile(fNameM, os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Errorf("error[%s] creating out file[%s]!", err.Error(), fNameM)
		return
	}
	defer outM.Close()
	outF, err := os.OpenFile(fNameF, os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Errorf("error[%s] creating out file[%s]!", err.Error(), fNameF)
		return
	}
	defer outF.Close()

	currLineContent := fmt.Sprintf("TOTAL:,%d\n", GTotal) +
		fmt.Sprintf("TotalM:,%d\n", GTotalM) +
		fmt.Sprintf("TotalF:,%d\n", GTotalF) +
		"NAME,GENDER,COUNT,PERCENT,PROP100K,ISNOTREALCOUNT\n"

	_, err = out.WriteString(currLineContent)
	_, err = outM.WriteString(currLineContent)
	_, err = outF.WriteString(currLineContent)
	if err != nil {
		log.Errorf("error[%s] writing string[%s] to file[%s]", err.Error(), currLineContent, fName)
		return
	}

	for _, pNameInfo := range GAllNamesSlice {
		percentVal := float64(100) * float64(pNameInfo.CountTotal) / float64(GTotal)
		prop100kVal := float64(100000) * float64(pNameInfo.CountTotal) / float64(GTotal)
		currLineContent = fmt.Sprintf("%s,%s,%d,%4.9f%%,%4.9f,0\n", pNameInfo.Name, pNameInfo.Gender, pNameInfo.CountTotal, percentVal, prop100kVal)
		_, err = out.WriteString(currLineContent)
		if err != nil {
			log.Errorf("error[%s] writing string[%s] to file[%s]", err.Error(), currLineContent, fName)
			return
		}

		if pNameInfo.Gender == "M" {
			_, err = outM.WriteString(currLineContent)
		} else if pNameInfo.Gender == "F" {
			_, err = outF.WriteString(currLineContent)
		}
	}

	log.Infof("All done!")
}
