Need to correctnames containing spaces:
0. RE-PARSE all HTMLs to:
   0.1 add column "encoding"(UTF, ASCII)
   0.2 save all names normalized to ASCII view
   0.3 AND "AS IS" in the separate file:
       e.g. goncalves.shtml">Gonçalves
            <td><a href="/nom-de-famille/nom/97675/goncalves.shtml">Gonçalves</a></td>
            <td align="right">&nbsp;&nbsp;18179</td>
       GONCALVES (part from target URL) -> <filename>-ASCII.csv
       GONçALVES (part from HTML) -> <filename>-UTF.csv
       !!! Mark all names with identical ASCII and UTF views as having "ASCII" encoding
   0.4 Do all rank calculations and save both to new *-Ranked.csv files
   0.5 Remove all ASCII records from <filename>-UTF-Ranked.csv
   0.6 Add all records from <filename>-ASCII-Ranked.csv  TO <filename>-UTF-Ranked-FINAL.csv



DO all other AFTER step 0.


1. Eliminate all DE, DA, LE, DU etc
2. separate all other separated by ' ' and '-' on two lines with identical statistics info

                OR
3. Check if exist lines of separated double(triple) surnames - then leave them as is, BUT force make them '-' separated
