/****************************************************
     Author: Brian J Clifton
     Url: http://www.advanced-web-metrics.com
     This script is free to use as long as this info is left in
     
     Combined script for tracking external links, file downloads and mailto links
     
     All scripts presented have been tested and validated by the author and are believed to be correct
     as of the date of publication or posting. The Google Analytics software on which they depend is 
     subject to change, however; and therefore no warranty is expressed or implied that they will
     work as described in the future. Always check the most current Google Analytics documentation.

****************************************************/


// Only links written to the page (already in the DOM) will be tagged
// This version is for ga.js (July 15th 2008)


function addLinkerEvents() {
	var as = document.getElementsByTagName("a");
	var extTrack = ["lowchensaustralia.com"];
	// List of local sites that should not be treated as an outbound link. Include at least your own domain here
	
	var extDoc = [".doc",".xls",".exe",".zip",".pdf",".js"];
	//List of file extensions on your site. Add/edit as you require
	
	/*If you edit no further below this line, Top Content will report as follows:
		/ext/url-of-external-site
		/downloads/filename
		/mailto/email-address-clicked
	*/
	
	for(var i=0; i<as.length; i++) {
		var flag = 0;
		var tmp = as[i].getAttribute("onclick");
		if (tmp != null && (tmp.indexOf('urchinTracker') > -1 || tmp.indexOf('_trackPageview') > -1) ) continue;

		// Tracking outbound links off site - no GATC
		for (var j=0; j<extTrack.length; j++) {					
			if (as[i].href.indexOf(extTrack[j]) == -1 && as[i].href.indexOf('google-analytics.com') == -1 && as[i].href.indexOf('javascript:void(0)') == -1 ) {
				flag++;
			}
		}
		if (flag == extTrack.length){
			var splitResult = as[i].href.split("//");
			as[i].setAttribute("onclick",((tmp != null) ? tmp+";" : "") + "pageTracker._trackPageview('/ext/" +splitResult[1]+ "');");
		}			


		// Tracking electronic documents - doc, xls, pdf, exe, zip
		for (var j=0; j<extDoc.length; j++) {
			if (as[i].href.indexOf(extTrack[0]) != -1 && as[i].href.indexOf(extDoc[j]) != -1) {
				var splitResult = as[i].href.split(extTrack[0]);
				as[i].setAttribute("onclick",((tmp != null) ? tmp+";" : "") + "pageTracker._trackPageview('/downloads" +splitResult[1]+ "');");
				//alert(splitResult[1])
				break;
			}
		}


		// added to track mailto links 23-Oct-2007
		if (as[i].href.indexOf("mailto:") != -1  && as[i].href.indexOf("gashbug@google.com") == -1 ) {
			var splitResult = as[i].href.split(":");
			as[i].setAttribute("onclick",((tmp != null) ? tmp+";" : "") + "pageTracker._trackPageview('/mailto/" +splitResult[1]+ "');");
			//alert(splitResult[1])
			break;
		}
	}
}

