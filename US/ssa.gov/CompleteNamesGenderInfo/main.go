package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
)

const (
	DataDir = "sourcedata"
)

type NameInfo struct {
	Name       string
	Gender     string
	CountTotal int
}

var GAllNames map[string]*NameInfo
var GTotal int
var GTotalM int
var GTotalF int

//===============================
var GAllNamesSlice []*NameInfo

type TResSortByCountDesc []*NameInfo

func (t TResSortByCountDesc) Len() int {
	return len(t)
}
func (t TResSortByCountDesc) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}
func (t TResSortByCountDesc) Less(i, j int) bool {
	return t[i].CountTotal > t[j].CountTotal
}

func main() {

	GAllNames = make(map[string]*NameInfo)

	subfiles, _ := ioutil.ReadDir(DataDir)
	for _, subf := range subfiles {
		if !subf.IsDir() && strings.Contains(subf.Name(), ".txt") {
			fullfilepath := filepath.Join(DataDir, subf.Name())
			file, err := os.Open(fullfilepath)
			if nil != err {
				continue
			}
			//Lisa,F,60267
			scanner := bufio.NewScanner(bufio.NewReader(file))
			nLine := -1
			for scanner.Scan() {
				nLine++
				line := strings.ToUpper(strings.Trim(scanner.Text(), "\n\r\t "))
				if 0 == len(line) {
					continue
				}

				fields := strings.Split(line, ",")
				if 3 != len(fields) {
					fmt.Printf("File %s has invalid format of line %d[%s]", fullfilepath, nLine, line)
					continue
				}

				currCount, err := strconv.Atoi(fields[2])
				if nil != err {
					fmt.Printf("Err converting count %s to int", fields[2])
					continue
				}
				currName := fields[0]
				currGender := fields[1]

				currInfo := GAllNames[currName]
				if nil == currInfo {
					currInfo = &NameInfo{Name: currName}
					GAllNames[currName] = currInfo
				} else {
					if !true {
						fmt.Printf("Dup found")
					}
				}

				currInfo.CountTotal += currCount
				// if "U" != currInfo.Gender {
				// 	if 0 != len(currInfo.Gender) && 0 != strings.Compare(currInfo.Gender, currGender) {
				// 		currInfo.Gender = "U"
				// 	} else {
				// 		currInfo.Gender = currGender
				// 	}
				// } else {
				// 	fmt.Print("")
				// }
				if 0 == len(currInfo.Gender) {
					currInfo.Gender = currGender
				}

				GTotal += currCount
				if 0 == strings.Compare(currGender, "M") {
					GTotalM += currCount
				} else if 0 == strings.Compare(currGender, "F") {
					GTotalF += currCount
				} else {
					fmt.Printf("File %s line %d Invalid gender %s", fullfilepath, nLine, currGender)
					continue
				}

			} //loop by file
			err = scanner.Err()
			if nil != err {
				fmt.Printf("Err %v reading file %s", err, fullfilepath)
				continue
			}
		}
	}

	//==================================
	for _, pNameInfo := range GAllNames {
		GAllNamesSlice = append(GAllNamesSlice, pNameInfo)
	}

	sort.Sort(TResSortByCountDesc(GAllNamesSlice))

	fName := "_OutTotalStatistics.csv"
	out, err := os.OpenFile(fName, os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		fmt.Printf("error[%s] creating out file[%s]!", err.Error(), fName)
		return
	}
	defer out.Close()

	currLineContent := fmt.Sprintf("Total births: %d\n", GTotal)
	_, err = out.WriteString(currLineContent)
	if err != nil {
		fmt.Printf("error[%s] writing string[%s] to file[%s]", err.Error(), currLineContent, fName)
		return
	}
	currLineContent = fmt.Sprintf("Total M: %d\n", GTotalM)
	_, err = out.WriteString(currLineContent)
	if err != nil {
		fmt.Printf("error[%s] writing string[%s] to file[%s]", err.Error(), currLineContent, fName)
		return
	}
	currLineContent = fmt.Sprintf("Total F: %d\n", GTotalF)
	_, err = out.WriteString(currLineContent)
	if err != nil {
		fmt.Printf("error[%s] writing string[%s] to file[%s]", err.Error(), currLineContent, fName)
		return
	}

	for _, pNameInfo := range GAllNamesSlice {
		currLineContent = fmt.Sprintf("%s,%s,%d\n", pNameInfo.Name, pNameInfo.Gender, pNameInfo.CountTotal)
		_, err = out.WriteString(currLineContent)
		if err != nil {
			fmt.Printf("error[%s] writing string[%s] to file[%s]", err.Error(), currLineContent, fName)
			return
		}

		for _, pNameInfo := range GAllNames {
			GAllNamesSlice = append(GAllNamesSlice, pNameInfo)
		}
	}

	fmt.Printf("All done!")
}
