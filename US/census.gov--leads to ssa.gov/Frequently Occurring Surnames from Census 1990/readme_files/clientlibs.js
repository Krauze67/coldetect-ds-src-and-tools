(function($) {

    var DAYS = ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"];
    var MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var ONE_DAY = 1000 * 60 * 60 * 24;

    /* Single date regular expression: Saturday, (Dec)ember (10), (2011) */
    var SINGLE_REGEXP = /[A-Z]{3,9}, ([A-Z]{3})[A-Z]{0,6} ([0-9]{1,2}), ([0-9]{4})(.*)/i;
    /* Range of dates regular expression: Saturday, (Dec)ember (10)[, (2011)] to Sunday, (Dec)ember (11), (2011) */
    var RANGE_REGEXP = /[A-Z]{3,9}, ([A-Z]{3})[A-Z]{0,6} ([0-9]{1,2})(?:, ([0-9]{4}))? to [A-Z]{3,9}, ([A-Z]{3})[A-Z]{0,6} ([0-9]{1,2}), ([0-9]{4})(.*)/i;
    /*HH:MM regular expression: 10:00 or 8:30*/
    var TIME_REGEX = /(^|\s)(1[0-2]|0?[1-9]):([0-5]?[0-9])/;
    
    $(document).ready(function() {

        var events = [];

        $("#calendarwiz").find(".cwuceeventtitle").each(function() {
            var titleCell = $(this),
                title = $.trim(titleCell.text()),
                onClick = titleCell.children("a").attr("onclick"),
                dateCell = titleCell.parent("tr").children("td"),
                dateText = $.trim(dateCell.text()),
                range = dateText.match(RANGE_REGEXP),
                single, start, end, details;

            if (range) {
                start = {month: $.inArray(range[1], MONTHS), day: range[2], year: range[3] || range[6]};
                end = {month: $.inArray(range[4], MONTHS), day: range[5], year: range[6]};
                details = $.trim(range[7]);
            } else {
                single = dateText.match(SINGLE_REGEXP);
                if (single) {
                    start = end = {month: $.inArray(single[1], MONTHS), day: single[2], year: single[3]};
                    details = $.trim(single[4]);
                }
            }

            if (start && end) {
            	/*If the string "details" contains HH:MM, display as part of details and remove the info from title to avoid duplication.*/
            	/*If the string "details" does not contain HH:MM, keep as part of title and do not display as part of details*/
            	if(details.search(TIME_REGEX) == -1){
            		details = '';
            	} else {
            		title = title.replace(details,'');
            	}
                var startDate = new Date(start.year, start.month, start.day),
                    endDate = new Date(end.year, end.month, end.day);
                events.push({title: title, start: startDate, end: endDate, details: details, onClick: onClick});
            }
        });

        /* Populate The Week Ahead calendar */
        var today = new Date();
        today.setHours(0, 0, 0, 0);

        for (var i = 0, j = 0; i < 7; i++) {
            var date = addTo(today, i),
                day = date.getDate(),
                dow = DAYS[date.getDay()],
                month = MONTHS[date.getMonth()];

            if (dow == DAYS[0] || dow == DAYS[6]) continue;

            $("#calendar").append(
                $("<div/>").addClass("day").toggleClass("selected", j++ == 0).append(
                    $("<a/>").attr("href", "#").data("date", date).append(
                        $("<div/>").addClass("dow").text(dow),
                        $("<div/>").addClass("date").text(month + " " + day)
                    ).click(function(event) {
                        var date = $(this).data("date"),
                            count = 0;

                        $("#events").empty();
                        $("#calendar .selected").removeClass("selected");
                        $(this).parent(".day").addClass("selected");

                        $.each(events, function(k, event) {
                        	//Added code to account for DST (Math.ceil(x/ONE_DAY)).
                            var time = Math.ceil(date.getTime()/ONE_DAY);
                            
                            if (time >= Math.ceil(event.start.getTime()/ONE_DAY) && time <= Math.ceil(event.end.getTime()/ONE_DAY)) {
                                count++;
                                $("#events").append(
                                    $("<p/>").append(
                                    	//Replace to delete utf-8 replacement character.
                                        //$("<a/>").attr("href", "#").attr("title", "Click for event details").text(event.title.replace(/[^\x00-\x7F]/g,'')).click(event.onClick),
                                    	$("<a/>").attr("href", "#").attr("title", "Click for event details").text(event.title.replace(/\uFFFD/g, '')).click(event.onClick),
                                    	$("<span/>").text(event.details)
                                    )
                                );
                            }
                        });

                        if (count == 0) {
                            $("#events").append(
                                $("<p/>").text("No events scheduled on this day")
                            )
                        }

                        $(this)[0].blur();
                        event.preventDefault();
                        return false;
                    })
                )
            );
        }

        $("#calendar .selected a").trigger("click");
    });

    function addTo(date, days) {
        return days == 0 ? date : new Date(date.getTime() + (days * ONE_DAY));
    }

})(jQuery);

//tabbed list
// JavaScript Document

// Wait until the DOM has loaded before querying the document
            $(document).ready(function(){
                
                $('li.button-tabs').each(function() //the button tabs li is the div that contains the dropdown and all the list divs
                {
                
                    
                    var $dropdown = $(this).parent().find("li.button_tabs_dropdown"); //get the div that contains the dropdown button and extra years
                    var $expand = $(this).find("a.toggle_dropdown");
                    var $allTabs = $(this).find('a.tabs'); //allTabs is an anchor with the tabs class
                    var k = 0;
                    var $tabsArray = [];
                    $tabsArray.push('#taball');
                    var $nextTab = $('a[href="'+"#tab"+k+'"]');
                    while ($nextTab.length > 0) //while there is a tab with an href like #tab[n]
                    {
                        
                        $nextTab = $('a[href="'+"#tab"+k+'"]'); //get the tab with href like #tab[n]
                        $tabsArray.push($nextTab.attr('href'));
                        k++;
                    }
                    
                    //on click display the drop down image
                    $($expand).on('click', function(e) //this is the list items div and also includes the dropdown menu
                    {
                        
                        if ($dropdown.attr('style') != undefined && $dropdown.attr('style').indexOf('none') >0)
                            {$dropdown.removeAttr('style');} //either show or hide the dropdown
                        else
                            {$dropdown.attr('style', 'display:none');}
                            
                    });
                    $($allTabs).on('click', function(e) //when one of the years in the dropdown is clicked
                    {
                        var $listTabs = document.getElementById('listMenu');
                        var $tabNum = ($allTabs.attr('href')); //tabNum is a string of format #tab[1-n] represnting which tab was clicked
                        var index = $tabNum.replace('#tab', '');
                        if (index == 'all')
                            index=0;
                        
                        var i=2;
                        var j =2;
                        
                        if (k-index < 2)
                        {
                            
                            j = parseInt(index,10)+4-k;
                            i=k-index;                            
                        }
                        else if (index < 2)
                        {
                            i=4-index;
                            j=index;                            
                        }
                        
                        var updateNum = 0;
                        for (var n=0; n<$tabsArray.length; n++)
                        {
                           
                            var $currentTab = $('a[href="'+$tabsArray[n]+'"]');
                            if (n>= (index-j) && n<=(index+i))
                            {
                                //set as active
                                $currentTab.parent().parent().parent().show();
                                if (n+1 == index)
                                {
                                   
                                    $currentTab.parent().parent().parent().addClass('current');
                                    $currentTab.addClass('active');
                                    //$currentTab.trigger('click');
                                }
                            } 
                            
                            else
                            {
                                //set as inactive
                                hideTab($currentTab);
                                //need some functinality to place this link in the dropdown
                                $dropdownAnchors = $(document).find('a.tabs').eq(updateNum++);
                                $dropdownAnchors.contents().filter(function() {return this.nodeType==3;})
                                    .replaceWith($currentTab.text());
                                $dropdownAnchors.attr('href', $tabsArray[n]);
                            }
                        }
                        var $active = $(document).find('ul.current'); //active is the currently selected tab ul
                        
                    });
                    
                 
                    
                });
                var hideTab = (function(currentTab)
                {
                    currentTab.parent().parent().parent().hide();
                    currentTab.parent().parent().parent().removeAttr('class');
                    currentTab.removeAttr('class');
                    
                    
                });
                $('ul.listTabs').on('click', 'li', function(e){
                        if ($(this).find('a').attr('class') == 'toggle_dropdown')
                            {
                            e.preventDefault();
                            return;}
                        var $content = $(this).find('a').attr('href');
                        window.open($content, "_self");
                        
                });
                $('ul.listTabs').on('hover', 'li', function(e)
                {
                    if ($(this).parent().attr('class') == 'listTabs')
                        return;
                    var $anchor = $(this).find('a');
                    var $color = $anchor.css("color");
                    
                    if ($anchor.css('color') == 'rgb(14, 114, 162)')
                        $anchor.css('color','#084e84' ); //"#084e84"
                    else
                        $anchor.removeAttr('style');
                    
                });
                $('li.button-tabs').on('hover', 'a',function(e)
                {
                    var $img = $(this).find('img');
                    var $src = $(this).find('img').attr('src');
                    if ($img.attr('class') == "selected")
                        return;
                    if ($src.indexOf("active") > 0 )                    
                        $img.attr('src', $src.replace("_active", ""));
                    
                    else
                        $img.attr('src', $src.replace(".jpg", "_active.jpg"));
                });
                $('li.button-tabs').on('click', 'a', function(e)
                {
                    var $img = $(this).find('img');
                    var $src = $(this).find('img').attr('src');
                    var $button_cls = $(this).find('img').attr('class');
                    
                    if ($button_cls===undefined && $src!=undefined)
                    {
                        $img.attr('class', 'selected');
                        $img.attr('src', $src.replace("n.jpg", "n_active.jpg"));
                    }
                    
                    else
                    {
                        $img.removeAttr('class');
                        //$img.attr('src', $src.replace("_active", ""));
                    }
                    
                });
                $('ul.tabs').each(function(){
                    
                    // For each set of tabs, we want to keep track of
                    // which tab is active and its associated content
                    var $active, $content, $links = $(this).find('a');

                    // If the location.hash matches one of the links, use that as the active tab.
                    // If no match is found, use the first link as the initial active tab.
                    $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
                    $active.addClass('active'); 
                    
                    //In case the href is an actual ref to another page, then abort as there's no need                    
                    //to hide/show the other tabs as the page will be redrawn.                    
                    if ( $active.attr('href') === undefined || $active.attr('href').indexOf('#') != 0)
                        return;
                    $content = $($active.attr('href'));
                    // Hide the remaining content
                    $links.not($active).each(function () {
                    	if (Modernizr.mq('only screen and (min-width : 960px)')) {
                    		$($(this).attr('href')).hide();
                    	}
                    });

                    // Bind the click event handler
                    $(this).on('click', 'li', function(e){
                        // Make the old tab inactive.
                        $active.removeAttr('class');
                        $($active.parent().siblings()[0]).removeClass('current-arrow');
                        $($active.parent().siblings()[0]).addClass('arrow-row');
                        $(".current").removeClass('current');
                        $content.hide(); 

                        // Update the variables with the new link and content
                        $active = $(this).find('a');
                        $content = $($(this).find('a').attr('href'));

                        // Make the tab active.
                        $active.attr('class','active');
                        
                        $($active.parent().siblings()[0]).removeClass('arrow-row');
                        $($active.parent().siblings()[0]).addClass('current-arrow');
                        $($($active).parent().parent()).addClass('current');
                        $content.show();

                        // Prevent the anchor's default click action
                        e.preventDefault();
                    });
                });
            });
var TINY={};

function T$(i){return document.getElementById(i)}
function T$$(e,p){return p.getElementsByTagName(e)}

TINY.accordion=function(){
	function slider(n){this.n=n; this.a=[]}
	slider.prototype.init=function(t,e,m,o,k,p){
		var a=T$(t), i=s=0, n=a.childNodes, l=n.length; this.s=k||0; this.m=m||0; this.p=p||0;
		for(i;i<l;i++){
			var v=n[i];
			if(v.nodeType!=3){
				this.a[s]={}; this.a[s].h=h=T$$(e,v)[0]; this.a[s].c=c=T$$('div',v)[0]; h.onclick=new Function(this.n+'.pr(0,'+s+')');
				if(o==s){h.className=this.s; c.style.height='auto'; c.d=1}else{h.className=this.p;c.style.height=0; c.d=-1}s++
			}
		}
		this.l=s
	};
	slider.prototype.pr=function(f,d){
		for(var i=0;i<this.l;i++){
			var h=this.a[i].h, c=this.a[i].c, k=c.style.height; k=k=='auto'?1:parseInt(k); clearInterval(c.t);
			if((k!=1&&c.d==-1)&&(f==1||i==d)){
				c.style.height=''; c.m=c.offsetHeight; c.style.height=k+'px'; c.d=1; h.className=this.s; su(c,1)
			}else if(k>0&&(f==-1||this.m||i==d)){
				c.d=-1; h.className=this.p; su(c,-1)
			}
		}
	};
	function su(c){c.t=setInterval(function(){sl(c)},20)};
	function sl(c){
		var h=c.offsetHeight, d=c.d==1?c.m-h:h; c.style.height=h+(Math.ceil(d/5)*c.d)+'px';
		c.style.opacity=h/c.m; c.style.filter='alpha(opacity='+h*100/c.m+')';
		if((c.d==1&&h>=c.m)||(c.d!=1&&h==1)){if(c.d==1){c.style.height='auto'} clearInterval(c.t)}
	};
	return{slider:slider}
}();

function initTabAccordions(numTabs) {
	resizeTabAccordions(numTabs);
	
	for (x=1; x<=numTabs; x++) {
		var tabId = '#tab' + x;
		var accordionId = tabId + "_accd";
		
		if (Modernizr.mq('only screen and (max-width : 600px)')) {
			$(tabId).show(); 	
		}
		$(accordionId).on('hide.bs.collapse', function () {
			var arrowId = '#' + $(this).attr('id') + "_a";
    		$(arrowId).removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right");
   		 });
		$(accordionId).on('show.bs.collapse', function () {
			var arrowId = '#' + $(this).attr('id') + "_a";
       		$(arrowId).removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");
    	});
	}
}

function initTabAccordionChevrons(selectedTabSpan) {
	$(selectedTabSpan).removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");
}

	
function resizeTabAccordions(numTabs) {
	for (x=1; x<=numTabs; x++) {
		var state = 'show';
		var tabId = '#tab' + x;
		var accordionId = tabId + "_accd";
		if (Modernizr.mq('only screen and (max-width : 600px)')) {
			state = 'hide';
			$(accordionId).addClass('collapse'); 
			$(tabId).show();
		} else {
			if ($("a[href='" + tabId + "']").hasClass('active')) 
				$(tabId).show();
			else
				$(tabId).hide();
		}	
		$(accordionId).collapse(state); 
	}
}
	

function initAccordionGroup(group, collapsedWidth) {
	if (collapsedWidth===undefined)
		collapsedWidth = 601;
	else 
		collapsedWidth = collapsedWidth + 1;
	resizeAccordionGroup(group, collapsedWidth);
	$(group).find('.panel-collapse').each(function() {
		$(this).on('hide.bs.collapse', function () {
	        $(this).parent().find(".glyphicon-chevron-down").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right");
	    });
		
		$(this).on('show.bs.collapse', function () {
	       $(this).parent().find(".glyphicon-chevron-right").removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");
	    });
	});
	$(group).find('a[data-toggle="collapse"]').click(function(e){		 
		  if (Modernizr.mq('only screen and (min-width : ' + collapsedWidth + 'px)')) { 
			e.preventDefault()
		    e.stopPropagation();
		  } 
	});
}

function resizeAccordionGroup(group, collapsedWidth) {
	if (collapsedWidth===undefined)
		collapsedWidth = 600;
	var state = 'show';
	if (Modernizr.mq('only screen and (max-width : ' + collapsedWidth + 'px)')) {
        state = 'hide'; // collapse accordion
        $(group).find('.panel-collapse').addClass('collapse');
    }
	$(group).find('.panel-collapse').collapse(state);
}

/**
 * A function used to add Accordion HTML to each columns of a Column Control component.
 * @param colTitleArray[] - An array contains the title for each column
 * @param index - integer representing the index value of each column control...if you have 3 column controls in a page index will be 0 for the frist  ...1...2 etc
 * @param baseCSSClass - the CSS class for the column control - its a CQ class...
 */
function wrapColumnsWithAccordionHTML(colTitleArray,  index, baseCSSClass) {			    			
	var stringContent = "";			    						    			
	var thisControl = $('.' + baseCSSClass)[index];			    			
	var uniqueId = 'columnControlComponent' + colTitleArray.length + index;			    			
	$(thisControl).attr('id', uniqueId);
	
	var colCtr = 1;
	$(thisControl).children('div').each(function(){                                                                                                                        
        var uniqueColumnId = 'columnControlComponent' + colTitleArray.length + index + '-c' + colCtr;
        $(this).attr('id', uniqueColumnId);			    			
		stringContent =  "<div class='panel'>";
		stringContent += "<div class='panel-heading'>";
		stringContent += "<a class='accordion-toggle collapsed' data-toggle='collapse' data-parent='#" + uniqueId + "' data-target='#target-" + uniqueColumnId + "'>";
		stringContent += "<h3 class='panel-title text-center visible-xs visible-sm hidden-md hidden-lg'>" + colTitleArray[(colCtr-1)];
		stringContent += "<span class='glyphicon glyphicon-chevron-right chevron'></span>";
		stringContent += "</h3>";
		stringContent += "<h2 class='hidden-xs visible-md visible-lg'>" + colTitleArray[(colCtr-1)] + "</h2>";
		stringContent += "</a>";
		stringContent += "</div>";//end panel-heading div
		stringContent += "<div id='target-" + uniqueColumnId + "' class='panel-collapse collapse'>";
		stringContent += "<div class='panel-body'>";
		stringContent += $(this).html();
		stringContent += "</div>";//end panel-body div
		stringContent += "</div>";//end panel-collapge target div
		stringContent += "</div>";//end panel div
					    			
		var customDiv = $(stringContent);			    			
		$('#' + uniqueId ).addClass('panel-group');
		$('#' + uniqueColumnId ).html(customDiv);
		colCtr++;
	});//end .children('div').each loop
}//end function

/**
 * A function used to add ONLY the column titles to the column control component when viewed in EDIT mode...
 * @param colTitleArray[] - An array containing the title for each column
 * @param index - integer representing the index value of each column control...if you have 3 column controls in a page index will be 0 for the frist  ...1...2 etc
 * @param baseCSSClass - the CSS class for the column control - its a CQ class...
 */
function insertTitleToColumns(colTitleArray, index, baseCSSClass) {		    					
	var thisControl = $('.' + baseCSSClass)[index];
	var colCtr = 1;
	$(thisControl).children('div').each(function(){ 
		$(this).prepend('<h3>' + colTitleArray[(colCtr - 1)] + '</h3>');
		colCtr++;
	});
}//end function
var $ = jQuery.noConflict();

$(document).ready(function(){
    var $searchBox = $("#searchInput");
    Shadowbox.init({
            handleOversize: "drag",
            modal: true
        });        
    // set StateGeo 
    if(google.loader.ClientLocation){
        var stateMap = {'ak':'alaska','al':'alabama','ar':'arkansas','az':'arizona','ca':'california','co':'colorado','ct':'connecticut','de':'delaware','fl':'florida','ga':'georgia','hi':'hawaii','ia':'iowa','id':'idaho','il':'illinois','in':'indiana','ks':'kansas','ky':'kentucky','la':'louisiana','ma':'massachusetts','md':'maryland','me':'maine','mi':'michigan','mn':'minnesota','mo':'missouri','ms':'mississippi','mt':'montana','nc':'north_carolina','nd':'north_dakota','ne':'nebraska','nh':'new_hampshire','nj':'new_jersey','nm':'new_mexico','nv':'nevada','ny':'new_york','oh':'ohio','ok':'oklahoma','or':'oregon','pa':'pennsylvania','ri':'rhode_island','sc':'south_carolina','sd':'south_dakota','tn':'tennessee','tx':'texas','ut':'utah','va':'virginia','vt':'vermont','wa':'washington','wi':'wisconsin','wv':'west_virginia','wy':'wyoming','dc':'district of columbia'}
        $('#stateGeo').val(stateMap[google.loader.ClientLocation.address.region]);
    }else{
        $('#stateGeo').val("none");
    }
    
    // Focus the cursor on the text box
    $searchBox.focus();

    // bring cursor to the end of the text
    cursorToEnd();
    
    $searchBox.blur();
    
    // Submit the search form when the search button is clicked
    $("#searchButton").click(function() { $("#searchForm").submit(); })
    
    // Disambiguation Dialog
    $('#disambigSelect').click(function() {
        $("#disambigDiv").toggle();
        $('#disambigSelect > div').toggleClass("disambigArrowRight disambigArrowDown");
        
        return false;
    });
    $("#searchTooltip").dialog({ autoOpen:false, width: "455px", minHeight: "170px", resizable: false,
                               dialogClass: "searchTooltipWidget", 
                               position: { my: "left top", at: "right-365 top", of: "#searchFormContainer" } });   
    
});

//bring cursor to the end of the text 
function cursorToEnd(){
    var $searchBox = $("#searchInput");
    var val = $searchBox.val();
    $searchBox.val('');
    $searchBox.val(val);
}

// Note: Plan b for video search. Pull image via ajax
// http://jquery-howto.blogspot.com/2009/02/how-to-get-youtube-video-screenshot.html
var $ = jQuery.noConflict();

$(document).ready(function(){
    $( "#searchbox, #mobile_searchbox" ).catcomplete({
        source: function( request, response ) {
            $.ajax({
                url: "/suggest", // Needs to set up apache forwarding to hit SOA typeahead url or JSP/Java forwarding
                dataType: "xml",
                data: {
                    query: request.term,
                    stateGeo: $("stateGeo").val(),
                    operationName: "httpsearch"
                },
                success: function( data ) {
                    // Merge the two types we want to display and iterate over them
                    response( $.map($.merge($(data).find("Answer"), $(data).find("result")), function(item) {
                        // Only the answer xml has a value component
                        if($(item).find("Value").text() != ""){ // for the answers before results
                            return {
                                ansValue: $(item).find("Value").text(),
                                ansGeography: $(item).find("Geography").text(),                                                                
                                prefix: $(item).find("Prefix").text(),
                                suffix: $(item).find("Suffix").text(),
                                ansLastUpdated: $(item).find("LastUpdated").text(),
                                // Don't grab the system desc
                                ansDescription: $(item).find("Description").first().text(),
                                ansSystemDesc: $(item).find("System").find("Description").text(),
                                ansSystemURL: $(item).find("System").find("URL").text()
                            }
                        }else if($(item).attr("name") != ""){ // for regular typeahead results
                            return {
                                label: $(item).attr("name"),
                                value: $(item).attr("name"),
                                category: $(item).attr("category")
                            }
                        }
                    }));
                    
                }
            });
        },
        minLength: 3,
        select: function( event, ui ) {
            if(ui.item){
                $('#searchbox, #mobile_searchbox').val(ui.item.value);
                $('input[name=cssp]').val('Typeahead');
            }
            
            $('#searchbox, #mobile_searchbox').closest("form").submit();
        },
        messages: {
            noResults: '',
                results: function() {}
        },
        open: function() {
//          $(this).dialog("widget").css("z-index", 10000);
            $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function() {
            $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    }); 
});

// Custom Typeahead
$.widget( "custom.catcomplete", $.ui.autocomplete, {
    _renderMenu: function( ul, items ) {
        var self = this, currentCategory = "";
        
        $.each( items, function( index, item ) {
            // double check for blank nodes from the jquery find method
            if(item.ansValue != null || item.label != null){
                // Decorate the system field to include latest estimate date
                var sysName = "";
                
                if(item.ansLastUpdated != ""){
                    var d = new Date(item.ansLastUpdated * 1000); // Given seconds originally
                    sysName = d.getFullYear() + " " + item.ansSystemDesc;
                }else{
                    sysName = item.ansSystemDesc;
                }
                
                if(item.ansValue != null){

                        ul.append('<li class="autocomplete-instant-answer"> ' +
                                '<div class="ia-label"><span class="ia-geo">' + item.ansGeography + '</span>&nbsp|&nbsp<span class="ia-stat">' + item.ansDescription + '</span></div>' +
                                '<div class="ia-stat-value">'+ item.prefix+'' +item.ansValue.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") +' ' +item.suffix+'</div>' +
                                
                                '<div class="ia-system"><a href="' + item.ansSystemURL  + '?cssp=Typeahead">Source: '+ sysName + '</a></div>' +
                                '</li>');
                    
                }else if ( item.category != currentCategory ) {
                    ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
                    currentCategory = item.category;
                }
                
                self._renderItemData( ul, item );
            }
            
        });
    }
});
/*
 * Shadowbox.js, version 3.0.3
 * http://shadowbox-js.com/
 *
 * Copyright 2007-2010, Michael J. I. Jackson
 * Date: 2011-05-14 08:09:50 +0000
 */
(function(au,k){var Q={version:"3.0.3"};var J=navigator.userAgent.toLowerCase();if(J.indexOf("windows")>-1||J.indexOf("win32")>-1){Q.isWindows=true}else{if(J.indexOf("macintosh")>-1||J.indexOf("mac os x")>-1){Q.isMac=true}else{if(J.indexOf("linux")>-1){Q.isLinux=true}}}Q.isIE=J.indexOf("msie")>-1;Q.isIE6=J.indexOf("msie 6")>-1;Q.isIE7=J.indexOf("msie 7")>-1;Q.isGecko=J.indexOf("gecko")>-1&&J.indexOf("safari")==-1;Q.isWebKit=J.indexOf("applewebkit/")>-1;var ab=/#(.+)$/,af=/^(light|shadow)box\[(.*?)\]/i,az=/\s*([a-z_]*?)\s*=\s*(.+)\s*/,f=/[0-9a-z]+$/i,aD=/(.+\/)shadowbox\.js/i;var A=false,a=false,l={},z=0,R,ap;Q.current=-1;Q.dimensions=null;Q.ease=function(K){return 1+Math.pow(K-1,3)};Q.errorInfo={fla:{name:"Flash",url:"http://www.adobe.com/products/flashplayer/"},qt:{name:"QuickTime",url:"http://www.apple.com/quicktime/download/"},wmp:{name:"Windows Media Player",url:"http://www.microsoft.com/windows/windowsmedia/"},f4m:{name:"Flip4Mac",url:"http://www.flip4mac.com/wmv_download.htm"}};Q.gallery=[];Q.onReady=aj;Q.path=null;Q.player=null;Q.playerId="sb-player";Q.options={animate:true,animateFade:true,autoplayMovies:true,continuous:false,enableKeys:true,flashParams:{bgcolor:"#000000",allowfullscreen:true},flashVars:{},flashVersion:"9.0.115",handleOversize:"resize",handleUnsupported:"link",onChange:aj,onClose:aj,onFinish:aj,onOpen:aj,showMovieControls:true,skipSetup:false,slideshowDelay:0,viewportPadding:20};Q.getCurrent=function(){return Q.current>-1?Q.gallery[Q.current]:null};Q.hasNext=function(){return Q.gallery.length>1&&(Q.current!=Q.gallery.length-1||Q.options.continuous)};Q.isOpen=function(){return A};Q.isPaused=function(){return ap=="pause"};Q.applyOptions=function(K){l=aC({},Q.options);aC(Q.options,K)};Q.revertOptions=function(){aC(Q.options,l)};Q.init=function(aG,aJ){if(a){return}a=true;if(Q.skin.options){aC(Q.options,Q.skin.options)}if(aG){aC(Q.options,aG)}if(!Q.path){var aI,S=document.getElementsByTagName("script");for(var aH=0,K=S.length;aH<K;++aH){aI=aD.exec(S[aH].src);if(aI){Q.path=aI[1];break}}}if(aJ){Q.onReady=aJ}P()};Q.open=function(S){if(A){return}var K=Q.makeGallery(S);Q.gallery=K[0];Q.current=K[1];S=Q.getCurrent();if(S==null){return}Q.applyOptions(S.options||{});G();if(Q.gallery.length){S=Q.getCurrent();if(Q.options.onOpen(S)===false){return}A=true;Q.skin.onOpen(S,c)}};Q.close=function(){if(!A){return}A=false;if(Q.player){Q.player.remove();Q.player=null}if(typeof ap=="number"){clearTimeout(ap);ap=null}z=0;aq(false);Q.options.onClose(Q.getCurrent());Q.skin.onClose();Q.revertOptions()};Q.play=function(){if(!Q.hasNext()){return}if(!z){z=Q.options.slideshowDelay*1000}if(z){R=aw();ap=setTimeout(function(){z=R=0;Q.next()},z);if(Q.skin.onPlay){Q.skin.onPlay()}}};Q.pause=function(){if(typeof ap!="number"){return}z=Math.max(0,z-(aw()-R));if(z){clearTimeout(ap);ap="pause";if(Q.skin.onPause){Q.skin.onPause()}}};Q.change=function(K){if(!(K in Q.gallery)){if(Q.options.continuous){K=(K<0?Q.gallery.length+K:0);if(!(K in Q.gallery)){return}}else{return}}Q.current=K;if(typeof ap=="number"){clearTimeout(ap);ap=null;z=R=0}Q.options.onChange(Q.getCurrent());c(true)};Q.next=function(){Q.change(Q.current+1)};Q.previous=function(){Q.change(Q.current-1)};Q.setDimensions=function(aS,aJ,aQ,aR,aI,K,aO,aL){var aN=aS,aH=aJ;var aM=2*aO+aI;if(aS+aM>aQ){aS=aQ-aM}var aG=2*aO+K;if(aJ+aG>aR){aJ=aR-aG}var S=(aN-aS)/aN,aP=(aH-aJ)/aH,aK=(S>0||aP>0);if(aL&&aK){if(S>aP){aJ=Math.round((aH/aN)*aS)}else{if(aP>S){aS=Math.round((aN/aH)*aJ)}}}Q.dimensions={height:aS+aI,width:aJ+K,innerHeight:aS,innerWidth:aJ,top:Math.floor((aQ-(aS+aM))/2+aO),left:Math.floor((aR-(aJ+aG))/2+aO),oversized:aK};return Q.dimensions};Q.makeGallery=function(aI){var K=[],aH=-1;if(typeof aI=="string"){aI=[aI]}if(typeof aI.length=="number"){aF(aI,function(aK,aL){if(aL.content){K[aK]=aL}else{K[aK]={content:aL}}});aH=0}else{if(aI.tagName){var S=Q.getCache(aI);aI=S?S:Q.makeObject(aI)}if(aI.gallery){K=[];var aJ;for(var aG in Q.cache){aJ=Q.cache[aG];if(aJ.gallery&&aJ.gallery==aI.gallery){if(aH==-1&&aJ.content==aI.content){aH=K.length}K.push(aJ)}}if(aH==-1){K.unshift(aI);aH=0}}else{K=[aI];aH=0}}aF(K,function(aK,aL){K[aK]=aC({},aL)});return[K,aH]};Q.makeObject=function(aH,aG){var aI={content:aH.href,title:aH.getAttribute("title")||"",link:aH};if(aG){aG=aC({},aG);aF(["player","title","height","width","gallery"],function(aJ,aK){if(typeof aG[aK]!="undefined"){aI[aK]=aG[aK];delete aG[aK]}});aI.options=aG}else{aI.options={}}if(!aI.player){aI.player=Q.getPlayer(aI.content)}var K=aH.getAttribute("rel");if(K){var S=K.match(af);if(S){aI.gallery=escape(S[2])}aF(K.split(";"),function(aJ,aK){S=aK.match(az);if(S){aI[S[1]]=S[2]}})}return aI};Q.getPlayer=function(aG){if(aG.indexOf("#")>-1&&aG.indexOf(document.location.href)==0){return"inline"}var aH=aG.indexOf("?");if(aH>-1){aG=aG.substring(0,aH)}var S,K=aG.match(f);if(K){S=K[0].toLowerCase()}if(S){if(Q.img&&Q.img.ext.indexOf(S)>-1){return"img"}if(Q.swf&&Q.swf.ext.indexOf(S)>-1){return"swf"}if(Q.flv&&Q.flv.ext.indexOf(S)>-1){return"flv"}if(Q.qt&&Q.qt.ext.indexOf(S)>-1){if(Q.wmp&&Q.wmp.ext.indexOf(S)>-1){return"qtwmp"}else{return"qt"}}if(Q.wmp&&Q.wmp.ext.indexOf(S)>-1){return"wmp"}}return"iframe"};function G(){var aH=Q.errorInfo,aI=Q.plugins,aK,aL,aO,aG,aN,S,aM,K;for(var aJ=0;aJ<Q.gallery.length;++aJ){aK=Q.gallery[aJ];aL=false;aO=null;switch(aK.player){case"flv":case"swf":if(!aI.fla){aO="fla"}break;case"qt":if(!aI.qt){aO="qt"}break;case"wmp":if(Q.isMac){if(aI.qt&&aI.f4m){aK.player="qt"}else{aO="qtf4m"}}else{if(!aI.wmp){aO="wmp"}}break;case"qtwmp":if(aI.qt){aK.player="qt"}else{if(aI.wmp){aK.player="wmp"}else{aO="qtwmp"}}break}if(aO){if(Q.options.handleUnsupported=="link"){switch(aO){case"qtf4m":aN="shared";S=[aH.qt.url,aH.qt.name,aH.f4m.url,aH.f4m.name];break;case"qtwmp":aN="either";S=[aH.qt.url,aH.qt.name,aH.wmp.url,aH.wmp.name];break;default:aN="single";S=[aH[aO].url,aH[aO].name]}aK.player="html";aK.content='<div class="sb-message">'+s(Q.lang.errors[aN],S)+"</div>"}else{aL=true}}else{if(aK.player=="inline"){aG=ab.exec(aK.content);if(aG){aM=ad(aG[1]);if(aM){aK.content=aM.innerHTML}else{aL=true}}else{aL=true}}else{if(aK.player=="swf"||aK.player=="flv"){K=(aK.options&&aK.options.flashVersion)||Q.options.flashVersion;if(Q.flash&&!Q.flash.hasFlashPlayerVersion(K)){aK.width=310;aK.height=177}}}}if(aL){Q.gallery.splice(aJ,1);if(aJ<Q.current){--Q.current}else{if(aJ==Q.current){Q.current=aJ>0?aJ-1:aJ}}--aJ}}}function aq(K){if(!Q.options.enableKeys){return}(K?F:M)(document,"keydown",an)}function an(aG){if(aG.metaKey||aG.shiftKey||aG.altKey||aG.ctrlKey){return}var S=v(aG),K;switch(S){case 81:case 88:case 27:K=Q.close;break;case 37:K=Q.previous;break;case 39:K=Q.next;break;case 32:K=typeof ap=="number"?Q.pause:Q.play;break}if(K){n(aG);K()}}function c(aK){aq(false);var aJ=Q.getCurrent();var aG=(aJ.player=="inline"?"html":aJ.player);if(typeof Q[aG]!="function"){throw"unknown player "+aG}if(aK){Q.player.remove();Q.revertOptions();Q.applyOptions(aJ.options||{})}Q.player=new Q[aG](aJ,Q.playerId);if(Q.gallery.length>1){var aH=Q.gallery[Q.current+1]||Q.gallery[0];if(aH.player=="img"){var S=new Image();S.src=aH.content}var aI=Q.gallery[Q.current-1]||Q.gallery[Q.gallery.length-1];if(aI.player=="img"){var K=new Image();K.src=aI.content}}Q.skin.onLoad(aK,W)}function W(){if(!A){return}if(typeof Q.player.ready!="undefined"){var K=setInterval(function(){if(A){if(Q.player.ready){clearInterval(K);K=null;Q.skin.onReady(e)}}else{clearInterval(K);K=null}},10)}else{Q.skin.onReady(e)}}function e(){if(!A){return}Q.player.append(Q.skin.body,Q.dimensions);Q.skin.onShow(I)}function I(){if(!A){return}if(Q.player.onLoad){Q.player.onLoad()}Q.options.onFinish(Q.getCurrent());if(!Q.isPaused()){Q.play()}aq(true)}if(!Array.prototype.indexOf){Array.prototype.indexOf=function(S,aG){var K=this.length>>>0;aG=aG||0;if(aG<0){aG+=K}for(;aG<K;++aG){if(aG in this&&this[aG]===S){return aG}}return -1}}function aw(){return(new Date).getTime()}function aC(K,aG){for(var S in aG){K[S]=aG[S]}return K}function aF(aH,aI){var S=0,K=aH.length;for(var aG=aH[0];S<K&&aI.call(aG,S,aG)!==false;aG=aH[++S]){}}function s(S,K){return S.replace(/\{(\w+?)\}/g,function(aG,aH){return K[aH]})}function aj(){}function ad(K){return document.getElementById(K)}function C(K){K.parentNode.removeChild(K)}var h=true,x=true;function d(){var K=document.body,S=document.createElement("div");h=typeof S.style.opacity==="string";S.style.position="fixed";S.style.margin=0;S.style.top="20px";K.appendChild(S,K.firstChild);x=S.offsetTop==20;K.removeChild(S)}Q.getStyle=(function(){var K=/opacity=([^)]*)/,S=document.defaultView&&document.defaultView.getComputedStyle;return function(aJ,aI){var aH;if(!h&&aI=="opacity"&&aJ.currentStyle){aH=K.test(aJ.currentStyle.filter||"")?(parseFloat(RegExp.$1)/100)+"":"";return aH===""?"1":aH}if(S){var aG=S(aJ,null);if(aG){aH=aG[aI]}if(aI=="opacity"&&aH==""){aH="1"}}else{aH=aJ.currentStyle[aI]}return aH}})();Q.appendHTML=function(aG,S){if(aG.insertAdjacentHTML){aG.insertAdjacentHTML("BeforeEnd",S)}else{if(aG.lastChild){var K=aG.ownerDocument.createRange();K.setStartAfter(aG.lastChild);var aH=K.createContextualFragment(S);aG.appendChild(aH)}else{aG.innerHTML=S}}};Q.getWindowSize=function(K){if(document.compatMode==="CSS1Compat"){return document.documentElement["client"+K]}return document.body["client"+K]};Q.setOpacity=function(aG,K){var S=aG.style;if(h){S.opacity=(K==1?"":K)}else{S.zoom=1;if(K==1){if(typeof S.filter=="string"&&(/alpha/i).test(S.filter)){S.filter=S.filter.replace(/\s*[\w\.]*alpha\([^\)]*\);?/gi,"")}}else{S.filter=(S.filter||"").replace(/\s*[\w\.]*alpha\([^\)]*\)/gi,"")+" alpha(opacity="+(K*100)+")"}}};Q.clearOpacity=function(K){Q.setOpacity(K,1)};function o(K){return K.target}function V(K){return[K.pageX,K.pageY]}function n(K){K.preventDefault()}function v(K){return K.keyCode}function F(aG,S,K){jQuery(aG).bind(S,K)}function M(aG,S,K){jQuery(aG).unbind(S,K)}jQuery.fn.shadowbox=function(K){return this.each(function(){var aG=jQuery(this);var aH=jQuery.extend({},K||{},jQuery.metadata?aG.metadata():jQuery.meta?aG.data():{});var S=this.className||"";aH.width=parseInt((S.match(/w:(\d+)/)||[])[1])||aH.width;aH.height=parseInt((S.match(/h:(\d+)/)||[])[1])||aH.height;Shadowbox.setup(aG,aH)})};var y=false,al;if(document.addEventListener){al=function(){document.removeEventListener("DOMContentLoaded",al,false);Q.load()}}else{if(document.attachEvent){al=function(){if(document.readyState==="complete"){document.detachEvent("onreadystatechange",al);Q.load()}}}}function g(){if(y){return}try{document.documentElement.doScroll("left")}catch(K){setTimeout(g,1);return}Q.load()}function P(){if(document.readyState==="complete"){return Q.load()}if(document.addEventListener){document.addEventListener("DOMContentLoaded",al,false);au.addEventListener("load",Q.load,false)}else{if(document.attachEvent){document.attachEvent("onreadystatechange",al);au.attachEvent("onload",Q.load);var K=false;try{K=au.frameElement===null}catch(S){}if(document.documentElement.doScroll&&K){g()}}}}Q.load=function(){if(y){return}if(!document.body){return setTimeout(Q.load,13)}y=true;d();Q.onReady();if(!Q.options.skipSetup){Q.setup()}Q.skin.init()};Q.plugins={};if(navigator.plugins&&navigator.plugins.length){var w=[];aF(navigator.plugins,function(K,S){w.push(S.name)});w=w.join(",");var ai=w.indexOf("Flip4Mac")>-1;Q.plugins={fla:w.indexOf("Shockwave Flash")>-1,qt:w.indexOf("QuickTime")>-1,wmp:!ai&&w.indexOf("Windows Media")>-1,f4m:ai}}else{var p=function(K){var S;try{S=new ActiveXObject(K)}catch(aG){}return !!S};Q.plugins={fla:p("ShockwaveFlash.ShockwaveFlash"),qt:p("QuickTime.QuickTime"),wmp:p("wmplayer.ocx"),f4m:false}}var X=/^(light|shadow)box/i,am="shadowboxCacheKey",b=1;Q.cache={};Q.select=function(S){var aG=[];if(!S){var K;aF(document.getElementsByTagName("a"),function(aJ,aK){K=aK.getAttribute("rel");if(K&&X.test(K)){aG.push(aK)}})}else{var aI=S.length;if(aI){if(typeof S=="string"){if(Q.find){aG=Q.find(S)}}else{if(aI==2&&typeof S[0]=="string"&&S[1].nodeType){if(Q.find){aG=Q.find(S[0],S[1])}}else{for(var aH=0;aH<aI;++aH){aG[aH]=S[aH]}}}}else{aG.push(S)}}return aG};Q.setup=function(K,S){aF(Q.select(K),function(aG,aH){Q.addCache(aH,S)})};Q.teardown=function(K){aF(Q.select(K),function(S,aG){Q.removeCache(aG)})};Q.addCache=function(aG,K){var S=aG[am];if(S==k){S=b++;aG[am]=S;F(aG,"click",u)}Q.cache[S]=Q.makeObject(aG,K)};Q.removeCache=function(K){M(K,"click",u);delete Q.cache[K[am]];K[am]=null};Q.getCache=function(S){var K=S[am];return(K in Q.cache&&Q.cache[K])};Q.clearCache=function(){for(var K in Q.cache){Q.removeCache(Q.cache[K].link)}Q.cache={}};function u(K){Q.open(this);if(Q.gallery.length){n(K)}}
/*
 * Sizzle CSS Selector Engine - v1.0
 *  Copyright 2009, The Dojo Foundation
 *  Released under the MIT, BSD, and GPL Licenses.
 *  More information: http://sizzlejs.com/
 *
 * Modified for inclusion in Shadowbox.js
 */
Q.find=(function(){var aP=/((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^[\]]*\]|['"][^'"]*['"]|[^[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,aQ=0,aS=Object.prototype.toString,aK=false,aJ=true;[0,0].sort(function(){aJ=false;return 0});var aG=function(a1,aW,a4,a5){a4=a4||[];var a7=aW=aW||document;if(aW.nodeType!==1&&aW.nodeType!==9){return[]}if(!a1||typeof a1!=="string"){return a4}var a2=[],aY,a9,bc,aX,a0=true,aZ=aH(aW),a6=a1;while((aP.exec(""),aY=aP.exec(a6))!==null){a6=aY[3];a2.push(aY[1]);if(aY[2]){aX=aY[3];break}}if(a2.length>1&&aL.exec(a1)){if(a2.length===2&&aM.relative[a2[0]]){a9=aT(a2[0]+a2[1],aW)}else{a9=aM.relative[a2[0]]?[aW]:aG(a2.shift(),aW);while(a2.length){a1=a2.shift();if(aM.relative[a1]){a1+=a2.shift()}a9=aT(a1,a9)}}}else{if(!a5&&a2.length>1&&aW.nodeType===9&&!aZ&&aM.match.ID.test(a2[0])&&!aM.match.ID.test(a2[a2.length-1])){var a8=aG.find(a2.shift(),aW,aZ);aW=a8.expr?aG.filter(a8.expr,a8.set)[0]:a8.set[0]}if(aW){var a8=a5?{expr:a2.pop(),set:aO(a5)}:aG.find(a2.pop(),a2.length===1&&(a2[0]==="~"||a2[0]==="+")&&aW.parentNode?aW.parentNode:aW,aZ);a9=a8.expr?aG.filter(a8.expr,a8.set):a8.set;if(a2.length>0){bc=aO(a9)}else{a0=false}while(a2.length){var bb=a2.pop(),ba=bb;if(!aM.relative[bb]){bb=""}else{ba=a2.pop()}if(ba==null){ba=aW}aM.relative[bb](bc,ba,aZ)}}else{bc=a2=[]}}if(!bc){bc=a9}if(!bc){throw"Syntax error, unrecognized expression: "+(bb||a1)}if(aS.call(bc)==="[object Array]"){if(!a0){a4.push.apply(a4,bc)}else{if(aW&&aW.nodeType===1){for(var a3=0;bc[a3]!=null;a3++){if(bc[a3]&&(bc[a3]===true||bc[a3].nodeType===1&&aN(aW,bc[a3]))){a4.push(a9[a3])}}}else{for(var a3=0;bc[a3]!=null;a3++){if(bc[a3]&&bc[a3].nodeType===1){a4.push(a9[a3])}}}}}else{aO(bc,a4)}if(aX){aG(aX,a7,a4,a5);aG.uniqueSort(a4)}return a4};aG.uniqueSort=function(aX){if(aR){aK=aJ;aX.sort(aR);if(aK){for(var aW=1;aW<aX.length;aW++){if(aX[aW]===aX[aW-1]){aX.splice(aW--,1)}}}}return aX};aG.matches=function(aW,aX){return aG(aW,null,null,aX)};aG.find=function(a3,aW,a4){var a2,a0;if(!a3){return[]}for(var aZ=0,aY=aM.order.length;aZ<aY;aZ++){var a1=aM.order[aZ],a0;if((a0=aM.leftMatch[a1].exec(a3))){var aX=a0[1];a0.splice(1,1);if(aX.substr(aX.length-1)!=="\\"){a0[1]=(a0[1]||"").replace(/\\/g,"");a2=aM.find[a1](a0,aW,a4);if(a2!=null){a3=a3.replace(aM.match[a1],"");break}}}}if(!a2){a2=aW.getElementsByTagName("*")}return{set:a2,expr:a3}};aG.filter=function(a6,a5,a9,aZ){var aY=a6,bb=[],a3=a5,a1,aW,a2=a5&&a5[0]&&aH(a5[0]);while(a6&&a5.length){for(var a4 in aM.filter){if((a1=aM.match[a4].exec(a6))!=null){var aX=aM.filter[a4],ba,a8;aW=false;if(a3===bb){bb=[]}if(aM.preFilter[a4]){a1=aM.preFilter[a4](a1,a3,a9,bb,aZ,a2);if(!a1){aW=ba=true}else{if(a1===true){continue}}}if(a1){for(var a0=0;(a8=a3[a0])!=null;a0++){if(a8){ba=aX(a8,a1,a0,a3);var a7=aZ^!!ba;if(a9&&ba!=null){if(a7){aW=true}else{a3[a0]=false}}else{if(a7){bb.push(a8);aW=true}}}}}if(ba!==k){if(!a9){a3=bb}a6=a6.replace(aM.match[a4],"");if(!aW){return[]}break}}}if(a6===aY){if(aW==null){throw"Syntax error, unrecognized expression: "+a6}else{break}}aY=a6}return a3};var aM=aG.selectors={order:["ID","NAME","TAG"],match:{ID:/#((?:[\w\u00c0-\uFFFF-]|\\.)+)/,CLASS:/\.((?:[\w\u00c0-\uFFFF-]|\\.)+)/,NAME:/\[name=['"]*((?:[\w\u00c0-\uFFFF-]|\\.)+)['"]*\]/,ATTR:/\[\s*((?:[\w\u00c0-\uFFFF-]|\\.)+)\s*(?:(\S?=)\s*(['"]*)(.*?)\3|)\s*\]/,TAG:/^((?:[\w\u00c0-\uFFFF\*-]|\\.)+)/,CHILD:/:(only|nth|last|first)-child(?:\((even|odd|[\dn+-]*)\))?/,POS:/:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^-]|$)/,PSEUDO:/:((?:[\w\u00c0-\uFFFF-]|\\.)+)(?:\((['"]*)((?:\([^\)]+\)|[^\2\(\)]*)+)\2\))?/},leftMatch:{},attrMap:{"class":"className","for":"htmlFor"},attrHandle:{href:function(aW){return aW.getAttribute("href")}},relative:{"+":function(a2,aX){var aZ=typeof aX==="string",a1=aZ&&!/\W/.test(aX),a3=aZ&&!a1;if(a1){aX=aX.toLowerCase()}for(var aY=0,aW=a2.length,a0;aY<aW;aY++){if((a0=a2[aY])){while((a0=a0.previousSibling)&&a0.nodeType!==1){}a2[aY]=a3||a0&&a0.nodeName.toLowerCase()===aX?a0||false:a0===aX}}if(a3){aG.filter(aX,a2,true)}},">":function(a2,aX){var a0=typeof aX==="string";if(a0&&!/\W/.test(aX)){aX=aX.toLowerCase();for(var aY=0,aW=a2.length;aY<aW;aY++){var a1=a2[aY];if(a1){var aZ=a1.parentNode;a2[aY]=aZ.nodeName.toLowerCase()===aX?aZ:false}}}else{for(var aY=0,aW=a2.length;aY<aW;aY++){var a1=a2[aY];if(a1){a2[aY]=a0?a1.parentNode:a1.parentNode===aX}}if(a0){aG.filter(aX,a2,true)}}},"":function(aZ,aX,a1){var aY=aQ++,aW=aU;if(typeof aX==="string"&&!/\W/.test(aX)){var a0=aX=aX.toLowerCase();aW=K}aW("parentNode",aX,aY,aZ,a0,a1)},"~":function(aZ,aX,a1){var aY=aQ++,aW=aU;if(typeof aX==="string"&&!/\W/.test(aX)){var a0=aX=aX.toLowerCase();aW=K}aW("previousSibling",aX,aY,aZ,a0,a1)}},find:{ID:function(aX,aY,aZ){if(typeof aY.getElementById!=="undefined"&&!aZ){var aW=aY.getElementById(aX[1]);return aW?[aW]:[]}},NAME:function(aY,a1){if(typeof a1.getElementsByName!=="undefined"){var aX=[],a0=a1.getElementsByName(aY[1]);for(var aZ=0,aW=a0.length;aZ<aW;aZ++){if(a0[aZ].getAttribute("name")===aY[1]){aX.push(a0[aZ])}}return aX.length===0?null:aX}},TAG:function(aW,aX){return aX.getElementsByTagName(aW[1])}},preFilter:{CLASS:function(aZ,aX,aY,aW,a2,a3){aZ=" "+aZ[1].replace(/\\/g,"")+" ";if(a3){return aZ}for(var a0=0,a1;(a1=aX[a0])!=null;a0++){if(a1){if(a2^(a1.className&&(" "+a1.className+" ").replace(/[\t\n]/g," ").indexOf(aZ)>=0)){if(!aY){aW.push(a1)}}else{if(aY){aX[a0]=false}}}}return false},ID:function(aW){return aW[1].replace(/\\/g,"")},TAG:function(aX,aW){return aX[1].toLowerCase()},CHILD:function(aW){if(aW[1]==="nth"){var aX=/(-?)(\d*)n((?:\+|-)?\d*)/.exec(aW[2]==="even"&&"2n"||aW[2]==="odd"&&"2n+1"||!/\D/.test(aW[2])&&"0n+"+aW[2]||aW[2]);aW[2]=(aX[1]+(aX[2]||1))-0;aW[3]=aX[3]-0}aW[0]=aQ++;return aW},ATTR:function(a0,aX,aY,aW,a1,a2){var aZ=a0[1].replace(/\\/g,"");if(!a2&&aM.attrMap[aZ]){a0[1]=aM.attrMap[aZ]}if(a0[2]==="~="){a0[4]=" "+a0[4]+" "}return a0},PSEUDO:function(a0,aX,aY,aW,a1){if(a0[1]==="not"){if((aP.exec(a0[3])||"").length>1||/^\w/.test(a0[3])){a0[3]=aG(a0[3],null,null,aX)}else{var aZ=aG.filter(a0[3],aX,aY,true^a1);if(!aY){aW.push.apply(aW,aZ)}return false}}else{if(aM.match.POS.test(a0[0])||aM.match.CHILD.test(a0[0])){return true}}return a0},POS:function(aW){aW.unshift(true);return aW}},filters:{enabled:function(aW){return aW.disabled===false&&aW.type!=="hidden"},disabled:function(aW){return aW.disabled===true},checked:function(aW){return aW.checked===true},selected:function(aW){aW.parentNode.selectedIndex;return aW.selected===true},parent:function(aW){return !!aW.firstChild},empty:function(aW){return !aW.firstChild},has:function(aY,aX,aW){return !!aG(aW[3],aY).length},header:function(aW){return/h\d/i.test(aW.nodeName)},text:function(aW){return"text"===aW.type},radio:function(aW){return"radio"===aW.type},checkbox:function(aW){return"checkbox"===aW.type},file:function(aW){return"file"===aW.type},password:function(aW){return"password"===aW.type},submit:function(aW){return"submit"===aW.type},image:function(aW){return"image"===aW.type},reset:function(aW){return"reset"===aW.type},button:function(aW){return"button"===aW.type||aW.nodeName.toLowerCase()==="button"},input:function(aW){return/input|select|textarea|button/i.test(aW.nodeName)}},setFilters:{first:function(aX,aW){return aW===0},last:function(aY,aX,aW,aZ){return aX===aZ.length-1},even:function(aX,aW){return aW%2===0},odd:function(aX,aW){return aW%2===1},lt:function(aY,aX,aW){return aX<aW[3]-0},gt:function(aY,aX,aW){return aX>aW[3]-0},nth:function(aY,aX,aW){return aW[3]-0===aX},eq:function(aY,aX,aW){return aW[3]-0===aX}},filter:{PSEUDO:function(a2,aY,aZ,a3){var aX=aY[1],a0=aM.filters[aX];if(a0){return a0(a2,aZ,aY,a3)}else{if(aX==="contains"){return(a2.textContent||a2.innerText||S([a2])||"").indexOf(aY[3])>=0}else{if(aX==="not"){var a1=aY[3];for(var aZ=0,aW=a1.length;aZ<aW;aZ++){if(a1[aZ]===a2){return false}}return true}else{throw"Syntax error, unrecognized expression: "+aX}}}},CHILD:function(aW,aZ){var a2=aZ[1],aX=aW;switch(a2){case"only":case"first":while((aX=aX.previousSibling)){if(aX.nodeType===1){return false}}if(a2==="first"){return true}aX=aW;case"last":while((aX=aX.nextSibling)){if(aX.nodeType===1){return false}}return true;case"nth":var aY=aZ[2],a5=aZ[3];if(aY===1&&a5===0){return true}var a1=aZ[0],a4=aW.parentNode;if(a4&&(a4.sizcache!==a1||!aW.nodeIndex)){var a0=0;for(aX=a4.firstChild;aX;aX=aX.nextSibling){if(aX.nodeType===1){aX.nodeIndex=++a0}}a4.sizcache=a1}var a3=aW.nodeIndex-a5;if(aY===0){return a3===0}else{return(a3%aY===0&&a3/aY>=0)}}},ID:function(aX,aW){return aX.nodeType===1&&aX.getAttribute("id")===aW},TAG:function(aX,aW){return(aW==="*"&&aX.nodeType===1)||aX.nodeName.toLowerCase()===aW},CLASS:function(aX,aW){return(" "+(aX.className||aX.getAttribute("class"))+" ").indexOf(aW)>-1},ATTR:function(a1,aZ){var aY=aZ[1],aW=aM.attrHandle[aY]?aM.attrHandle[aY](a1):a1[aY]!=null?a1[aY]:a1.getAttribute(aY),a2=aW+"",a0=aZ[2],aX=aZ[4];return aW==null?a0==="!=":a0==="="?a2===aX:a0==="*="?a2.indexOf(aX)>=0:a0==="~="?(" "+a2+" ").indexOf(aX)>=0:!aX?a2&&aW!==false:a0==="!="?a2!==aX:a0==="^="?a2.indexOf(aX)===0:a0==="$="?a2.substr(a2.length-aX.length)===aX:a0==="|="?a2===aX||a2.substr(0,aX.length+1)===aX+"-":false},POS:function(a0,aX,aY,a1){var aW=aX[2],aZ=aM.setFilters[aW];if(aZ){return aZ(a0,aY,aX,a1)}}}};var aL=aM.match.POS;for(var aI in aM.match){aM.match[aI]=new RegExp(aM.match[aI].source+/(?![^\[]*\])(?![^\(]*\))/.source);aM.leftMatch[aI]=new RegExp(/(^(?:.|\r|\n)*?)/.source+aM.match[aI].source)}var aO=function(aX,aW){aX=Array.prototype.slice.call(aX,0);if(aW){aW.push.apply(aW,aX);return aW}return aX};try{Array.prototype.slice.call(document.documentElement.childNodes,0)}catch(aV){aO=function(a0,aZ){var aX=aZ||[];if(aS.call(a0)==="[object Array]"){Array.prototype.push.apply(aX,a0)}else{if(typeof a0.length==="number"){for(var aY=0,aW=a0.length;aY<aW;aY++){aX.push(a0[aY])}}else{for(var aY=0;a0[aY];aY++){aX.push(a0[aY])}}}return aX}}var aR;if(document.documentElement.compareDocumentPosition){aR=function(aX,aW){if(!aX.compareDocumentPosition||!aW.compareDocumentPosition){if(aX==aW){aK=true}return aX.compareDocumentPosition?-1:1}var aY=aX.compareDocumentPosition(aW)&4?-1:aX===aW?0:1;if(aY===0){aK=true}return aY}}else{if("sourceIndex" in document.documentElement){aR=function(aX,aW){if(!aX.sourceIndex||!aW.sourceIndex){if(aX==aW){aK=true}return aX.sourceIndex?-1:1}var aY=aX.sourceIndex-aW.sourceIndex;if(aY===0){aK=true}return aY}}else{if(document.createRange){aR=function(aZ,aX){if(!aZ.ownerDocument||!aX.ownerDocument){if(aZ==aX){aK=true}return aZ.ownerDocument?-1:1}var aY=aZ.ownerDocument.createRange(),aW=aX.ownerDocument.createRange();aY.setStart(aZ,0);aY.setEnd(aZ,0);aW.setStart(aX,0);aW.setEnd(aX,0);var a0=aY.compareBoundaryPoints(Range.START_TO_END,aW);if(a0===0){aK=true}return a0}}}}function S(aW){var aX="",aZ;for(var aY=0;aW[aY];aY++){aZ=aW[aY];if(aZ.nodeType===3||aZ.nodeType===4){aX+=aZ.nodeValue}else{if(aZ.nodeType!==8){aX+=S(aZ.childNodes)}}}return aX}(function(){var aX=document.createElement("div"),aY="script"+(new Date).getTime();aX.innerHTML="<a name='"+aY+"'/>";var aW=document.documentElement;aW.insertBefore(aX,aW.firstChild);if(document.getElementById(aY)){aM.find.ID=function(a0,a1,a2){if(typeof a1.getElementById!=="undefined"&&!a2){var aZ=a1.getElementById(a0[1]);return aZ?aZ.id===a0[1]||typeof aZ.getAttributeNode!=="undefined"&&aZ.getAttributeNode("id").nodeValue===a0[1]?[aZ]:k:[]}};aM.filter.ID=function(a1,aZ){var a0=typeof a1.getAttributeNode!=="undefined"&&a1.getAttributeNode("id");return a1.nodeType===1&&a0&&a0.nodeValue===aZ}}aW.removeChild(aX);aW=aX=null})();(function(){var aW=document.createElement("div");aW.appendChild(document.createComment(""));if(aW.getElementsByTagName("*").length>0){aM.find.TAG=function(aX,a1){var a0=a1.getElementsByTagName(aX[1]);if(aX[1]==="*"){var aZ=[];for(var aY=0;a0[aY];aY++){if(a0[aY].nodeType===1){aZ.push(a0[aY])}}a0=aZ}return a0}}aW.innerHTML="<a href='#'></a>";if(aW.firstChild&&typeof aW.firstChild.getAttribute!=="undefined"&&aW.firstChild.getAttribute("href")!=="#"){aM.attrHandle.href=function(aX){return aX.getAttribute("href",2)}}aW=null})();if(document.querySelectorAll){(function(){var aW=aG,aY=document.createElement("div");aY.innerHTML="<p class='TEST'></p>";if(aY.querySelectorAll&&aY.querySelectorAll(".TEST").length===0){return}aG=function(a2,a1,aZ,a0){a1=a1||document;if(!a0&&a1.nodeType===9&&!aH(a1)){try{return aO(a1.querySelectorAll(a2),aZ)}catch(a3){}}return aW(a2,a1,aZ,a0)};for(var aX in aW){aG[aX]=aW[aX]}aY=null})()}(function(){var aW=document.createElement("div");aW.innerHTML="<div class='test e'></div><div class='test'></div>";if(!aW.getElementsByClassName||aW.getElementsByClassName("e").length===0){return}aW.lastChild.className="e";if(aW.getElementsByClassName("e").length===1){return}aM.order.splice(1,0,"CLASS");aM.find.CLASS=function(aX,aY,aZ){if(typeof aY.getElementsByClassName!=="undefined"&&!aZ){return aY.getElementsByClassName(aX[1])}};aW=null})();function K(aX,a2,a1,a5,a3,a4){for(var aZ=0,aY=a5.length;aZ<aY;aZ++){var aW=a5[aZ];if(aW){aW=aW[aX];var a0=false;while(aW){if(aW.sizcache===a1){a0=a5[aW.sizset];break}if(aW.nodeType===1&&!a4){aW.sizcache=a1;aW.sizset=aZ}if(aW.nodeName.toLowerCase()===a2){a0=aW;break}aW=aW[aX]}a5[aZ]=a0}}}function aU(aX,a2,a1,a5,a3,a4){for(var aZ=0,aY=a5.length;aZ<aY;aZ++){var aW=a5[aZ];if(aW){aW=aW[aX];var a0=false;while(aW){if(aW.sizcache===a1){a0=a5[aW.sizset];break}if(aW.nodeType===1){if(!a4){aW.sizcache=a1;aW.sizset=aZ}if(typeof a2!=="string"){if(aW===a2){a0=true;break}}else{if(aG.filter(a2,[aW]).length>0){a0=aW;break}}}aW=aW[aX]}a5[aZ]=a0}}}var aN=document.compareDocumentPosition?function(aX,aW){return aX.compareDocumentPosition(aW)&16}:function(aX,aW){return aX!==aW&&(aX.contains?aX.contains(aW):true)};var aH=function(aW){var aX=(aW?aW.ownerDocument||aW:0).documentElement;return aX?aX.nodeName!=="HTML":false};var aT=function(aW,a3){var aZ=[],a0="",a1,aY=a3.nodeType?[a3]:a3;while((a1=aM.match.PSEUDO.exec(aW))){a0+=a1[0];aW=aW.replace(aM.match.PSEUDO,"")}aW=aM.relative[aW]?aW+"*":aW;for(var a2=0,aX=aY.length;a2<aX;a2++){aG(aW,aY[a2],aZ)}return aG.filter(a0,aZ)};return aG})();Q.lang={code:"en",of:"of",loading:"loading",cancel:"Cancel",next:"Next",previous:"Previous",play:"Play",pause:"Pause",close:"Close",errors:{single:'You must install the <a href="{0}">{1}</a> browser plugin to view this content.',shared:'You must install both the <a href="{0}">{1}</a> and <a href="{2}">{3}</a> browser plugins to view this content.',either:'You must install either the <a href="{0}">{1}</a> or the <a href="{2}">{3}</a> browser plugin to view this content.'}};var D,at="sb-drag-proxy",E,j,ag;function ax(){E={x:0,y:0,startX:null,startY:null}}function aA(){var K=Q.dimensions;aC(j.style,{height:K.innerHeight+"px",width:K.innerWidth+"px"})}function O(){ax();var K=["position:absolute","cursor:"+(Q.isGecko?"-moz-grab":"move"),"background-color:"+(Q.isIE?"#fff;filter:alpha(opacity=0)":"transparent")].join(";");Q.appendHTML(Q.skin.body,'<div id="'+at+'" style="'+K+'"></div>');j=ad(at);aA();F(j,"mousedown",L)}function B(){if(j){M(j,"mousedown",L);C(j);j=null}ag=null}function L(S){n(S);var K=V(S);E.startX=K[0];E.startY=K[1];ag=ad(Q.player.id);F(document,"mousemove",H);F(document,"mouseup",i);if(Q.isGecko){j.style.cursor="-moz-grabbing"}}function H(aI){var K=Q.player,aJ=Q.dimensions,aH=V(aI);var aG=aH[0]-E.startX;E.startX+=aG;E.x=Math.max(Math.min(0,E.x+aG),aJ.innerWidth-K.width);var S=aH[1]-E.startY;E.startY+=S;E.y=Math.max(Math.min(0,E.y+S),aJ.innerHeight-K.height);aC(ag.style,{left:E.x+"px",top:E.y+"px"})}function i(){M(document,"mousemove",H);M(document,"mouseup",i);if(Q.isGecko){j.style.cursor="-moz-grab"}}Q.img=function(S,aG){this.obj=S;this.id=aG;this.ready=false;var K=this;D=new Image();D.onload=function(){K.height=S.height?parseInt(S.height,10):D.height;K.width=S.width?parseInt(S.width,10):D.width;K.ready=true;D.onload=null;D=null};D.src=S.content};Q.img.ext=["bmp","gif","jpg","jpeg","png"];Q.img.prototype={append:function(S,aI){var aG=document.createElement("img");aG.id=this.id;aG.src=this.obj.content;aG.style.position="absolute";var K,aH;if(aI.oversized&&Q.options.handleOversize=="resize"){K=aI.innerHeight;aH=aI.innerWidth}else{K=this.height;aH=this.width}aG.setAttribute("height",K);aG.setAttribute("width",aH);S.appendChild(aG)},remove:function(){var K=ad(this.id);if(K){C(K)}B();if(D){D.onload=null;D=null}},onLoad:function(){var K=Q.dimensions;if(K.oversized&&Q.options.handleOversize=="drag"){O()}},onWindowResize:function(){var aH=Q.dimensions;switch(Q.options.handleOversize){case"resize":var K=ad(this.id);K.height=aH.innerHeight;K.width=aH.innerWidth;break;case"drag":if(ag){var aG=parseInt(Q.getStyle(ag,"top")),S=parseInt(Q.getStyle(ag,"left"));if(aG+this.height<aH.innerHeight){ag.style.top=aH.innerHeight-this.height+"px"}if(S+this.width<aH.innerWidth){ag.style.left=aH.innerWidth-this.width+"px"}aA()}break}}};Q.iframe=function(S,aG){this.obj=S;this.id=aG;var K=ad("sb-overlay");this.height=S.height?parseInt(S.height,10):K.offsetHeight;this.width=S.width?parseInt(S.width,10):K.offsetWidth};Q.iframe.prototype={append:function(K,aG){var S='<iframe id="'+this.id+'" name="'+this.id+'" height="100%" width="100%" frameborder="0" marginwidth="0" marginheight="0" style="visibility:hidden" onload="this.style.visibility=\'visible\'" scrolling="auto"';if(Q.isIE){S+=' allowtransparency="true"';if(Q.isIE6){S+=" src=\"javascript:false;document.write('');\""}}S+="></iframe>";K.innerHTML=S},remove:function(){var K=ad(this.id);if(K){C(K);if(Q.isGecko){delete au.frames[this.id]}}},onLoad:function(){var K=Q.isIE?ad(this.id).contentWindow:au.frames[this.id];K.location.href=this.obj.content}};Q.html=function(K,S){this.obj=K;this.id=S;this.height=K.height?parseInt(K.height,10):300;this.width=K.width?parseInt(K.width,10):500};Q.html.prototype={append:function(K,S){var aG=document.createElement("div");aG.id=this.id;aG.className="html";aG.innerHTML=this.obj.content;K.appendChild(aG)},remove:function(){var K=ad(this.id);if(K){C(K)}}};var ao=false,Y=[],q=["sb-nav-close","sb-nav-next","sb-nav-play","sb-nav-pause","sb-nav-previous"],aa,ae,Z,m=true;function N(aG,aQ,aN,aL,aR){var K=(aQ=="opacity"),aM=K?Q.setOpacity:function(aS,aT){aS.style[aQ]=""+aT+"px"};if(aL==0||(!K&&!Q.options.animate)||(K&&!Q.options.animateFade)){aM(aG,aN);if(aR){aR()}return}var aO=parseFloat(Q.getStyle(aG,aQ))||0;var aP=aN-aO;if(aP==0){if(aR){aR()}return}aL*=1000;var aH=aw(),aK=Q.ease,aJ=aH+aL,aI;var S=setInterval(function(){aI=aw();if(aI>=aJ){clearInterval(S);S=null;aM(aG,aN);if(aR){aR()}}else{aM(aG,aO+aK((aI-aH)/aL)*aP)}},10)}function aB(){aa.style.height=Q.getWindowSize("Height")+"px";aa.style.width=Q.getWindowSize("Width")+"px"}function aE(){aa.style.top=document.documentElement.scrollTop+"px";aa.style.left=document.documentElement.scrollLeft+"px"}function ay(K){if(K){aF(Y,function(S,aG){aG[0].style.visibility=aG[1]||""})}else{Y=[];aF(Q.options.troubleElements,function(aG,S){aF(document.getElementsByTagName(S),function(aH,aI){Y.push([aI,aI.style.visibility]);aI.style.visibility="hidden"})})}}function r(aG,K){var S=ad("sb-nav-"+aG);if(S){S.style.display=K?"":"none"}}function ah(K,aJ){var aI=ad("sb-loading"),aG=Q.getCurrent().player,aH=(aG=="img"||aG=="html");if(K){Q.setOpacity(aI,0);aI.style.display="block";var S=function(){Q.clearOpacity(aI);if(aJ){aJ()}};if(aH){N(aI,"opacity",1,Q.options.fadeDuration,S)}else{S()}}else{var S=function(){aI.style.display="none";Q.clearOpacity(aI);if(aJ){aJ()}};if(aH){N(aI,"opacity",0,Q.options.fadeDuration,S)}else{S()}}}function t(aO){var aJ=Q.getCurrent();ad("sb-title-inner").innerHTML=aJ.title||"";var aP,aL,S,aQ,aM;if(Q.options.displayNav){aP=true;var aN=Q.gallery.length;if(aN>1){if(Q.options.continuous){aL=aM=true}else{aL=(aN-1)>Q.current;aM=Q.current>0}}if(Q.options.slideshowDelay>0&&Q.hasNext()){aQ=!Q.isPaused();S=!aQ}}else{aP=aL=S=aQ=aM=false}r("close",aP);r("next",aL);r("play",S);r("pause",aQ);r("previous",aM);var K="";if(Q.options.displayCounter&&Q.gallery.length>1){var aN=Q.gallery.length;if(Q.options.counterType=="skip"){var aI=0,aH=aN,aG=parseInt(Q.options.counterLimit)||0;if(aG<aN&&aG>2){var aK=Math.floor(aG/2);aI=Q.current-aK;if(aI<0){aI+=aN}aH=Q.current+(aG-aK);if(aH>aN){aH-=aN}}while(aI!=aH){if(aI==aN){aI=0}K+='<a onclick="Shadowbox.change('+aI+');"';if(aI==Q.current){K+=' class="sb-counter-current"'}K+=">"+(++aI)+"</a>"}}else{K=[Q.current+1,Q.lang.of,aN].join(" ")}}ad("sb-counter").innerHTML=K;aO()}function U(aH){var K=ad("sb-title-inner"),aG=ad("sb-info-inner"),S=0.35;K.style.visibility=aG.style.visibility="";if(K.innerHTML!=""){N(K,"marginTop",0,S)}N(aG,"marginTop",0,S,aH)}function av(aG,aM){var aK=ad("sb-title"),K=ad("sb-info"),aH=aK.offsetHeight,aI=K.offsetHeight,aJ=ad("sb-title-inner"),aL=ad("sb-info-inner"),S=(aG?0.35:0);N(aJ,"marginTop",aH,S);N(aL,"marginTop",aI*-1,S,function(){aJ.style.visibility=aL.style.visibility="hidden";aM()})}function ac(K,aH,S,aJ){var aI=ad("sb-wrapper-inner"),aG=(S?Q.options.resizeDuration:0);N(Z,"top",aH,aG);N(aI,"height",K,aG,aJ)}function ar(K,aH,S,aI){var aG=(S?Q.options.resizeDuration:0);N(Z,"left",aH,aG);N(Z,"width",K,aG,aI)}function ak(aM,aG){var aI=ad("sb-body-inner"),aM=parseInt(aM),aG=parseInt(aG),S=Z.offsetHeight-aI.offsetHeight,K=Z.offsetWidth-aI.offsetWidth,aK=ae.offsetHeight,aL=ae.offsetWidth,aJ=parseInt(Q.options.viewportPadding)||20,aH=(Q.player&&Q.options.handleOversize!="drag");return Q.setDimensions(aM,aG,aK,aL,S,K,aJ,aH)}var T={};T.markup='<div id="sb-container"><div id="sb-overlay"></div><div id="sb-wrapper"><div id="sb-title"><div id="sb-title-inner"></div></div><div id="sb-wrapper-inner"><div id="sb-body"><div id="sb-body-inner"></div><div id="sb-loading"><div id="sb-loading-inner"><span>{loading}</span></div></div></div></div><div id="sb-info"><div id="sb-info-inner"><div id="sb-counter"></div><div id="sb-nav"><a id="sb-nav-close" title="{close}" onclick="Shadowbox.close()"></a><a id="sb-nav-next" title="{next}" onclick="Shadowbox.next()"></a><a id="sb-nav-play" title="{play}" onclick="Shadowbox.play()"></a><a id="sb-nav-pause" title="{pause}" onclick="Shadowbox.pause()"></a><a id="sb-nav-previous" title="{previous}" onclick="Shadowbox.previous()"></a></div></div></div></div></div>';T.options={animSequence:"sync",counterLimit:10,counterType:"default",displayCounter:true,displayNav:true,fadeDuration:0.35,initialHeight:160,initialWidth:320,modal:false,overlayColor:"#000",overlayOpacity:0.5,resizeDuration:0.35,showOverlay:true,troubleElements:["select","object","embed","canvas"]};T.init=function(){Q.appendHTML(document.body,s(T.markup,Q.lang));T.body=ad("sb-body-inner");aa=ad("sb-container");ae=ad("sb-overlay");Z=ad("sb-wrapper");if(!x){aa.style.position="absolute"}if(!h){var aG,K,S=/url\("(.*\.png)"\)/;aF(q,function(aI,aJ){aG=ad(aJ);if(aG){K=Q.getStyle(aG,"backgroundImage").match(S);if(K){aG.style.backgroundImage="none";aG.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true,src="+K[1]+",sizingMethod=scale);"}}})}var aH;F(au,"resize",function(){if(aH){clearTimeout(aH);aH=null}if(A){aH=setTimeout(T.onWindowResize,10)}})};T.onOpen=function(K,aG){m=false;aa.style.display="block";aB();var S=ak(Q.options.initialHeight,Q.options.initialWidth);ac(S.innerHeight,S.top);ar(S.width,S.left);if(Q.options.showOverlay){ae.style.backgroundColor=Q.options.overlayColor;Q.setOpacity(ae,0);if(!Q.options.modal){F(ae,"click",Q.close)}ao=true}if(!x){aE();F(au,"scroll",aE)}ay();aa.style.visibility="visible";if(ao){N(ae,"opacity",Q.options.overlayOpacity,Q.options.fadeDuration,aG)}else{aG()}};T.onLoad=function(S,K){ah(true);while(T.body.firstChild){C(T.body.firstChild)}av(S,function(){if(!A){return}if(!S){Z.style.visibility="visible"}t(K)})};T.onReady=function(aH){if(!A){return}var S=Q.player,aG=ak(S.height,S.width);var K=function(){U(aH)};switch(Q.options.animSequence){case"hw":ac(aG.innerHeight,aG.top,true,function(){ar(aG.width,aG.left,true,K)});break;case"wh":ar(aG.width,aG.left,true,function(){ac(aG.innerHeight,aG.top,true,K)});break;default:ar(aG.width,aG.left,true);ac(aG.innerHeight,aG.top,true,K)}};T.onShow=function(K){ah(false,K);m=true};T.onClose=function(){if(!x){M(au,"scroll",aE)}M(ae,"click",Q.close);Z.style.visibility="hidden";var K=function(){aa.style.visibility="hidden";aa.style.display="none";ay(true)};if(ao){N(ae,"opacity",0,Q.options.fadeDuration,K)}else{K()}};T.onPlay=function(){r("play",false);r("pause",true)};T.onPause=function(){r("pause",false);r("play",true)};T.onWindowResize=function(){if(!m){return}aB();var K=Q.player,S=ak(K.height,K.width);ar(S.width,S.left);ac(S.innerHeight,S.top);if(K.onWindowResize){K.onWindowResize()}};Q.skin=T;au.Shadowbox=Q})(window);
/* SWFObject v2.1 <http://code.google.com/p/swfobject/>
 Copyright (c) 2007-2008 Geoff Stearns, Michael Williams, and Bobby van der Sluis
 This software is released under the MIT License <http://www.opensource.org/licenses/mit-license.php>
 */
try {
    var p = swfobject.p;
} catch (e) {
    /*! SWFObject v2.1 <http://code.google.com/p/swfobject/>
     Copyright (c) 2007-2008 Geoff Stearns, Michael Williams, and Bobby van der Sluis
     This software is released under the MIT License <http://www.opensource.org/licenses/mit-license.php>
     */
    var swfobject = function() {

        var UNDEF = "undefined",
                OBJECT = "object",
                SHOCKWAVE_FLASH = "Shockwave Flash",
                SHOCKWAVE_FLASH_AX = "ShockwaveFlash.ShockwaveFlash",
                FLASH_MIME_TYPE = "application/x-shockwave-flash",
                EXPRESS_INSTALL_ID = "SWFObjectExprInst",

                win = window,
                doc = document,
                nav = navigator,

                domLoadFnArr = [],
                regObjArr = [],
                objIdArr = [],
                listenersArr = [],
                script,
                timer = null,
                storedAltContent = null,
                storedAltContentId = null,
                isDomLoaded = false,
                isExpressInstallActive = false;

        /* Centralized function for browser feature detection
         - Proprietary feature detection (conditional compiling) is used to detect Internet Explorer's features
         - User agent string detection is only used when no alternative is possible
         - Is executed directly for optimal performance
         */
        var ua = function() {
            var w3cdom = typeof doc.getElementById != UNDEF && typeof doc.getElementsByTagName != UNDEF && typeof doc.createElement != UNDEF,
                    playerVersion = [0,0,0],
                    d = null;
            if (typeof nav.plugins != UNDEF && typeof nav.plugins[SHOCKWAVE_FLASH] == OBJECT) {
                d = nav.plugins[SHOCKWAVE_FLASH].description;
                if (d && !(typeof nav.mimeTypes != UNDEF && nav.mimeTypes[FLASH_MIME_TYPE] && !nav.mimeTypes[FLASH_MIME_TYPE].enabledPlugin)) { // navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin indicates whether plug-ins are enabled or disabled in Safari 3+
                    d = d.replace(/^.*\s+(\S+\s+\S+$)/, "$1");
                    playerVersion[0] = parseInt(d.replace(/^(.*)\..*$/, "$1"), 10);
                    playerVersion[1] = parseInt(d.replace(/^.*\.(.*)\s.*$/, "$1"), 10);
                    playerVersion[2] = /r/.test(d) ? parseInt(d.replace(/^.*r(.*)$/, "$1"), 10) : 0;
                }
            }
            else if (typeof win.ActiveXObject != UNDEF) {
                var a = null, fp6Crash = false;
                try {
                    a = new ActiveXObject(SHOCKWAVE_FLASH_AX + ".7");
                }
                catch(e) {
                    try {
                        a = new ActiveXObject(SHOCKWAVE_FLASH_AX + ".6");
                        playerVersion = [6,0,21];
                        a.allowScriptAccess = "sameDomain";  // Introduced in fp6.0.47
                    }
                    catch(e) {
                        if (playerVersion[0] == 6) {
                            fp6Crash = true;
                        }
                    }
                    if (!fp6Crash) {
                        try {
                            a = new ActiveXObject(SHOCKWAVE_FLASH_AX);
                        }
                        catch(e) {}
                        }
                }
                if (!fp6Crash && a) { // a will return null when ActiveX is disabled
                    try {
                        d = a.GetVariable("$version");  // Will crash fp6.0.21/23/29
                        if (d) {
                            d = d.split(" ")[1].split(",");
                            playerVersion = [parseInt(d[0], 10), parseInt(d[1], 10), parseInt(d[2], 10)];
                        }
                    }
                    catch(e) {}
                }
            }
            var u = nav.userAgent.toLowerCase(),
                    p = nav.platform.toLowerCase(),
                    webkit = /webkit/.test(u) ? parseFloat(u.replace(/^.*webkit\/(\d+(\.\d+)?).*$/, "$1")) : false, // returns either the webkit version or false if not webkit
                    ie = false,
                    windows = p ? /win/.test(p) : /win/.test(u),
                    mac = p ? /mac/.test(p) : /mac/.test(u);
            /*@cc_on
             ie = true;
             @if (@_win32)
             windows = true;
             @elif (@_mac)
             mac = true;
             @end
             @*/
            return { w3cdom:w3cdom, pv:playerVersion, webkit:webkit, ie:ie, win:windows, mac:mac };
        }();

        /* Cross-browser onDomLoad
         - Based on Dean Edwards' solution: http://dean.edwards.name/weblog/2006/06/again/
         - Will fire an event as soon as the DOM of a page is loaded (supported by Gecko based browsers - like Firefox -, IE, Opera9+, Safari)
         */
        var onDomLoad = function() {
            if (!ua.w3cdom) {
                return;
            }
            addDomLoadEvent(main);
            //CQ: START
            /*if (ua.ie && ua.win) {
                try {    // Avoid a possible Operation Aborted error
                    doc.write("<scr" + "ipt id=__ie_ondomload defer=true src=//:></scr" + "ipt>"); // String is split into pieces to avoid Norton AV to add code that can cause errors
                    script = getElementById("__ie_ondomload");
                    if (script) {
                        addListener(script, "onreadystatechange", checkReadyState);
                    }
                }
                catch(e) {}
            }*/
            //CQ: END
            if (ua.webkit && typeof doc.readyState != UNDEF) {
                        timer = setInterval(function() { if (/loaded|complete/.test(doc.readyState)) { callDomLoadFunctions(); }}, 10);
                    }
            if (typeof doc.addEventListener != UNDEF) {
                doc.addEventListener("DOMContentLoaded", callDomLoadFunctions, null);
            }
            addLoadEvent(callDomLoadFunctions);
        }();

        function checkReadyState() {
            if (script.readyState == "complete") {
                script.parentNode.removeChild(script);
                callDomLoadFunctions();
            }
        }

        function callDomLoadFunctions() {
            if (isDomLoaded) {
                return;
            }
            //CQ: START
            /*if (ua.ie && ua.win) { // Test if we can really add elements to the DOM; we don't want to fire it too early
                var s = createElement("span");
                try { // Avoid a possible Operation Aborted error
                    var t = doc.getElementsByTagName("body")[0].appendChild(s);
                    t.parentNode.removeChild(t);
                }
                catch (e) {
                    return;
                }
            }*/
            //CQ: END
            isDomLoaded = true;
            if (timer) {
                clearInterval(timer);
                timer = null;
            }
            var dl = domLoadFnArr.length;
            for (var i = 0; i < dl; i++) {
                domLoadFnArr[i]();
            }
        }

        function addDomLoadEvent(fn) {
            if (isDomLoaded) {
                fn();
            }
            else {
                domLoadFnArr[domLoadFnArr.length] = fn; // Array.push() is only available in IE5.5+
            }
        }

        /* Cross-browser onload
         - Based on James Edwards' solution: http://brothercake.com/site/resources/scripts/onload/
         - Will fire an event as soon as a web page including all of its assets are loaded
         */
        function addLoadEvent(fn) {
            if (typeof win.addEventListener != UNDEF) {
                win.addEventListener("load", fn, false);
            }
            else if (typeof doc.addEventListener != UNDEF) {
                doc.addEventListener("load", fn, false);
            }
            else if (typeof win.attachEvent != UNDEF) {
                addListener(win, "onload", fn);
            }
            else if (typeof win.onload == "function") {
                var fnOld = win.onload;
                win.onload = function() {
                    fnOld();
                    fn();
                };
            }
            else {
                win.onload = fn;
            }
        }

        /* Main function
         - Will preferably execute onDomLoad, otherwise onload (as a fallback)
         */
        function main() { // Static publishing only
            var rl = regObjArr.length;
            for (var i = 0; i < rl; i++) { // For each registered object element
                var id = regObjArr[i].id;
                if (ua.pv[0] > 0) {
                    var obj = getElementById(id);
                    if (obj) {
                        regObjArr[i].width = obj.getAttribute("width") ? obj.getAttribute("width") : "0";
                        regObjArr[i].height = obj.getAttribute("height") ? obj.getAttribute("height") : "0";
                        if (hasPlayerVersion(regObjArr[i].swfVersion)) { // Flash plug-in version >= Flash content version: Houston, we have a match!
                            if (ua.webkit && ua.webkit < 312) { // Older webkit engines ignore the object element's nested param elements
                                fixParams(obj);
                            }
                            setVisibility(id, true);
                        }
                        else if (regObjArr[i].expressInstall && !isExpressInstallActive && hasPlayerVersion("6.0.65") && (ua.win || ua.mac)) { // Show the Adobe Express Install dialog if set by the web page author and if supported (fp6.0.65+ on Win/Mac OS only)
                            showExpressInstall(regObjArr[i]);
                        }
                        else { // Flash plug-in and Flash content version mismatch: display alternative content instead of Flash content
                            displayAltContent(obj);
                        }
                    }
                }
                else {  // If no fp is installed, we let the object element do its job (show alternative content)
                    setVisibility(id, true);
                }
            }
        }

        /* Fix nested param elements, which are ignored by older webkit engines
         - This includes Safari up to and including version 1.2.2 on Mac OS 10.3
         - Fall back to the proprietary embed element
         */
        function fixParams(obj) {
            var nestedObj = obj.getElementsByTagName(OBJECT)[0];
            if (nestedObj) {
                var e = createElement("embed"), a = nestedObj.attributes;
                if (a) {
                    var al = a.length;
                    for (var i = 0; i < al; i++) {
                        if (a[i].nodeName == "DATA") {
                            e.setAttribute("src", a[i].nodeValue);
                        }
                        else {
                            e.setAttribute(a[i].nodeName, a[i].nodeValue);
                        }
                    }
                }
                var c = nestedObj.childNodes;
                if (c) {
                    var cl = c.length;
                    for (var j = 0; j < cl; j++) {
                        if (c[j].nodeType == 1 && c[j].nodeName == "PARAM") {
                            e.setAttribute(c[j].getAttribute("name"), c[j].getAttribute("value"));
                        }
                    }
                }
                obj.parentNode.replaceChild(e, obj);
            }
        }

        /* Show the Adobe Express Install dialog
         - Reference: http://www.adobe.com/cfusion/knowledgebase/index.cfm?id=6a253b75
         */
        function showExpressInstall(regObj) {
            isExpressInstallActive = true;
            var obj = getElementById(regObj.id);
            if (obj) {
                if (regObj.altContentId) {
                    var ac = getElementById(regObj.altContentId);
                    if (ac) {
                        storedAltContent = ac;
                        storedAltContentId = regObj.altContentId;
                    }
                }
                else {
                    storedAltContent = abstractAltContent(obj);
                }
                if (!(/%$/.test(regObj.width)) && parseInt(regObj.width, 10) < 310) {
                    regObj.width = "310";
                }
                if (!(/%$/.test(regObj.height)) && parseInt(regObj.height, 10) < 137) {
                    regObj.height = "137";
                }
                doc.title = doc.title.slice(0, 47) + " - Flash Player Installation";
                var pt = ua.ie && ua.win ? "ActiveX" : "PlugIn",
                        dt = doc.title,
                        fv = "MMredirectURL=" + win.location + "&MMplayerType=" + pt + "&MMdoctitle=" + dt,
                        replaceId = regObj.id;
                // For IE when a SWF is loading (AND: not available in cache) wait for the onload event to fire to remove the original object element
                // In IE you cannot properly cancel a loading SWF file without breaking browser load references, also obj.onreadystatechange doesn't work
                if (ua.ie && ua.win && obj.readyState != 4) {
                    var newObj = createElement("div");
                    replaceId += "SWFObjectNew";
                    newObj.setAttribute("id", replaceId);
                    obj.parentNode.insertBefore(newObj, obj); // Insert placeholder div that will be replaced by the object element that loads expressinstall.swf
                    obj.style.display = "none";
                    var fn = function() {
                        obj.parentNode.removeChild(obj);
                    };
                    addListener(win, "onload", fn);
                }
                createSWF({ data:regObj.expressInstall, id:EXPRESS_INSTALL_ID, width:regObj.width, height:regObj.height }, { flashvars:fv }, replaceId);
            }
        }

        /* Functions to abstract and display alternative content
         */
        function displayAltContent(obj) {
            if (ua.ie && ua.win && obj.readyState != 4) {
                // For IE when a SWF is loading (AND: not available in cache) wait for the onload event to fire to remove the original object element
                // In IE you cannot properly cancel a loading SWF file without breaking browser load references, also obj.onreadystatechange doesn't work
                var el = createElement("div");
                obj.parentNode.insertBefore(el, obj); // Insert placeholder div that will be replaced by the alternative content
                el.parentNode.replaceChild(abstractAltContent(obj), el);
                obj.style.display = "none";
                var fn = function() {
                    obj.parentNode.removeChild(obj);
                };
                addListener(win, "onload", fn);
            }
            else {
                obj.parentNode.replaceChild(abstractAltContent(obj), obj);
            }
        }


        function abstractAltContent(obj) {
            var ac = createElement("div");
            if (ua.win && ua.ie) {
                ac.innerHTML = obj.innerHTML;
            }
            else {
                var nestedObj = obj.getElementsByTagName(OBJECT)[0];
                if (nestedObj) {
                    var c = nestedObj.childNodes;
                    if (c) {
                        var cl = c.length;
                        for (var i = 0; i < cl; i++) {
                            if (!(c[i].nodeType == 1 && c[i].nodeName == "PARAM") && !(c[i].nodeType == 8)) {
                                ac.appendChild(c[i].cloneNode(true));
                            }
                        }
                    }
                }
            }
            return ac;
        }

        /* Cross-browser dynamic SWF creation
         */
        function createSWF(attObj, parObj, id) {
            var r, el = getElementById(id);
            if (el) {
                if (typeof attObj.id == UNDEF) { // if no 'id' is defined for the object element, it will inherit the 'id' from the alternative content
                    attObj.id = id;
                }
                if (ua.ie && ua.win) { // IE, the object element and W3C DOM methods do not combine: fall back to outerHTML
                    var att = "";
                    for (var i in attObj) {
                        if (attObj[i] != Object.prototype[i]) { // Filter out prototype additions from other potential libraries, like Object.prototype.toJSONString = function() {}
                            if (i.toLowerCase() == "data") {
                                parObj.movie = attObj[i];
                            }
                            else if (i.toLowerCase() == "styleclass") { // 'class' is an ECMA4 reserved keyword
                                att += ' class="' + attObj[i] + '"';
                            }
                            else if (i.toLowerCase() != "classid") {
                                att += ' ' + i + '="' + attObj[i] + '"';
                            }
                        }
                    }
                    var par = "";
                    for (var j in parObj) {
                        if (parObj[j] != Object.prototype[j]) { // Filter out prototype additions from other potential libraries
                            par += '<param name="' + j + '" value="' + parObj[j] + '" />';
                        }
                    }
                    el.outerHTML = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"' + att + '>' + par + '</object>';
                    objIdArr[objIdArr.length] = attObj.id; // Stored to fix object 'leaks' on unload (dynamic publishing only)
                    r = getElementById(attObj.id);
                }
                else if (ua.webkit && ua.webkit < 312) { // Older webkit engines ignore the object element's nested param elements: fall back to the proprietary embed element
                    var e = createElement("embed");
                    e.setAttribute("type", FLASH_MIME_TYPE);
                    for (var k in attObj) {
                        if (attObj[k] != Object.prototype[k]) { // Filter out prototype additions from other potential libraries
                            if (k.toLowerCase() == "data") {
                                e.setAttribute("src", attObj[k]);
                            }
                            else if (k.toLowerCase() == "styleclass") { // 'class' is an ECMA4 reserved keyword
                                e.setAttribute("class", attObj[k]);
                            }
                            else if (k.toLowerCase() != "classid") { // Filter out IE specific attribute
                                e.setAttribute(k, attObj[k]);
                            }
                        }
                    }
                    for (var l in parObj) {
                        if (parObj[l] != Object.prototype[l]) { // Filter out prototype additions from other potential libraries
                            if (l.toLowerCase() != "movie") { // Filter out IE specific param element
                                e.setAttribute(l, parObj[l]);
                            }
                        }
                    }
                    el.parentNode.replaceChild(e, el);
                    r = e;
                }
                else { // Well-behaving browsers
                    var o = createElement(OBJECT);
                    o.setAttribute("type", FLASH_MIME_TYPE);
                    for (var m in attObj) {
                        if (attObj[m] != Object.prototype[m]) { // Filter out prototype additions from other potential libraries
                            if (m.toLowerCase() == "styleclass") { // 'class' is an ECMA4 reserved keyword
                                o.setAttribute("class", attObj[m]);
                            }
                            else if (m.toLowerCase() != "classid") { // Filter out IE specific attribute
                                o.setAttribute(m, attObj[m]);
                            }
                        }
                    }
                    for (var n in parObj) {
                        if (parObj[n] != Object.prototype[n] && n.toLowerCase() != "movie") { // Filter out prototype additions from other potential libraries and IE specific param element
                            createObjParam(o, n, parObj[n]);
                        }
                    }
                    el.parentNode.replaceChild(o, el);
                    r = o;
                }
            }
            r.style.outline="none";
            return r;
        }

        function createObjParam(el, pName, pValue) {
            var p = createElement("param");
            p.setAttribute("name", pName);
            p.setAttribute("value", pValue);
            el.appendChild(p);
        }

        /* Cross-browser SWF removal
         - Especially needed to safely and completely remove a SWF in Internet Explorer
         */
        function removeSWF(id) {
            var obj = getElementById(id);
            if (obj && (obj.nodeName == "OBJECT" || obj.nodeName == "EMBED")) {
                if (ua.ie && ua.win) {
                    if (obj.readyState == 4) {
                        removeObjectInIE(id);
                    }
                    else {
                        win.attachEvent("onload", function() {
                            removeObjectInIE(id);
                        });
                    }
                }
                else {
                    obj.parentNode.removeChild(obj);
                }
            }
        }

        function removeObjectInIE(id) {
            var obj = getElementById(id);
            if (obj) {
                for (var i in obj) {
                    if (typeof obj[i] == "function") {
                        obj[i] = null;
                    }
                }
                obj.parentNode.removeChild(obj);
            }
        }

        /* Functions to optimize JavaScript compression
         */
        function getElementById(id) {
            var el = null;
            try {
                el = doc.getElementById(id);
            }
            catch (e) {
            }
            return el;
        }

        function createElement(el) {
            return doc.createElement(el);
        }

        /* Updated attachEvent function for Internet Explorer
         - Stores attachEvent information in an Array, so on unload the detachEvent functions can be called to avoid memory leaks
         */
        function addListener(target, eventType, fn) {
            target.attachEvent(eventType, fn);
            listenersArr[listenersArr.length] = [target, eventType, fn];
        }

        /* Flash Player and SWF content version matching
         */
        function hasPlayerVersion(rv) {
            var pv = ua.pv, v = rv.split(".");
            v[0] = parseInt(v[0], 10);
            v[1] = parseInt(v[1], 10) || 0; // supports short notation, e.g. "9" instead of "9.0.0"
            v[2] = parseInt(v[2], 10) || 0;
            return (pv[0] > v[0] || (pv[0] == v[0] && pv[1] > v[1]) || (pv[0] == v[0] && pv[1] == v[1] && pv[2] >= v[2])) ? true : false;
        }

        /* Cross-browser dynamic CSS creation
         - Based on Bobby van der Sluis' solution: http://www.bobbyvandersluis.com/articles/dynamicCSS.php
         */
        function createCSS(sel, decl) {
            if (ua.ie && ua.mac) {
                return;
            }
            var h = doc.getElementsByTagName("head")[0], s = createElement("style");
            s.setAttribute("type", "text/css");
            s.setAttribute("media", "screen");
            if (!(ua.ie && ua.win) && typeof doc.createTextNode != UNDEF) {
                s.appendChild(doc.createTextNode(sel + " {" + decl + "}"));
            }
            h.appendChild(s);
            if (ua.ie && ua.win && typeof doc.styleSheets != UNDEF && doc.styleSheets.length > 0) {
                var ls = doc.styleSheets[doc.styleSheets.length - 1];
                if (typeof ls.addRule == OBJECT) {
                    ls.addRule(sel, decl);
                }
            }
        }

        function setVisibility(id, isVisible) {
            var v = isVisible ? "visible" : "hidden";
            if (isDomLoaded && getElementById(id)) {
                getElementById(id).style.visibility = v;
            }
            else {
                createCSS("#" + id, "visibility:" + v);
            }
        }


        /* Filter to avoid XSS attacks
         */
        function urlEncodeIfNecessary(s) {
            var regex = /[\\\"<>\.;]/;
            var hasBadChars = regex.exec(s) != null;
            return hasBadChars ? encodeURIComponent(s) : s;
        }

        /* Release memory to avoid memory leaks caused by closures, fix hanging audio/video threads and force open sockets/NetConnections to disconnect (Internet Explorer only)
         */
        var cleanup = function() {
            if (ua.ie && ua.win) {
                var fn = function() {
                    // remove listeners to avoid memory leaks
                    var ll = listenersArr.length;
                    for (var i = 0; i < ll; i++) {
                        listenersArr[i][0].detachEvent(listenersArr[i][1], listenersArr[i][2]);
                    }
                    // cleanup dynamically embedded objects to fix audio/video threads and force open sockets and NetConnections to disconnect
                    var il = objIdArr.length;
                    for (var j = 0; j < il; j++) {
                        removeSWF(objIdArr[j]);
                    }
                    // cleanup library's main closures to avoid memory leaks
                    for (var k in ua) {
                        ua[k] = null;
                    }
                    ua = null;
                    for (var l in swfobject) {
                        swfobject[l] = null;
                    }
                    swfobject = null;
                    // detach ourself
                    window.detachEvent("onunload", fn);
                };
                window.attachEvent("onunload", fn);
            }
        }();


        return {
            /* Public API
             - Reference: http://code.google.com/p/swfobject/wiki/SWFObject_2_0_documentation
             */
            registerObject: function(objectIdStr, swfVersionStr, xiSwfUrlStr) {
                if (!ua.w3cdom || !objectIdStr || !swfVersionStr) {
                    return;
                }
                var regObj = {};
                regObj.id = objectIdStr;
                regObj.swfVersion = swfVersionStr;
                regObj.expressInstall = xiSwfUrlStr ? xiSwfUrlStr : false;
                regObjArr[regObjArr.length] = regObj;
                setVisibility(objectIdStr, false);
            },

            getObjectById: function(objectIdStr) {
                var r = null;
                if (ua.w3cdom) {
                    var o = getElementById(objectIdStr);
                    if (o) {
                        var n = o.getElementsByTagName(OBJECT)[0];
                        if (!n || (n && typeof o.SetVariable != UNDEF)) {
                            r = o;
                        }
                        else if (typeof n.SetVariable != UNDEF) {
                            r = n;
                        }
                    }
                }
                return r;
            },

            embedSWF: function(swfUrlStr, replaceElemIdStr, widthStr, heightStr, swfVersionStr, xiSwfUrlStr, flashvarsObj, parObj, attObj) {
                //CQ: START
                if (document.readyState === undefined || document.readyState == "complete") {
                    callDomLoadFunctions();
                }
                //CQ: END
                if (!ua.w3cdom || !swfUrlStr || !replaceElemIdStr || !widthStr || !heightStr || !swfVersionStr) {
                    return;
                }
                widthStr += ""; // Auto-convert to string
                heightStr += "";
                if (hasPlayerVersion(swfVersionStr)) {
                    setVisibility(replaceElemIdStr, false);
                    var att = {};
                    if (attObj && typeof attObj === OBJECT) {
                        for (var i in attObj) {
                            if (attObj[i] != Object.prototype[i]) { // Filter out prototype additions from other potential libraries
                                att[i] = attObj[i];
                            }
                        }
                    }
                    att.data = swfUrlStr;
                    att.width = widthStr;
                    att.height = heightStr;
                    var par = {};
                    if (parObj && typeof parObj === OBJECT) {
                        for (var j in parObj) {
                            if (parObj[j] != Object.prototype[j]) { // Filter out prototype additions from other potential libraries
                                par[j] = parObj[j];
                            }
                        }
                    }
                    if (flashvarsObj && typeof flashvarsObj === OBJECT) {
                        for (var k in flashvarsObj) {
                            if (flashvarsObj[k] != Object.prototype[k]) { // Filter out prototype additions from other potential libraries
                                if (typeof par.flashvars != UNDEF) {
                                    par.flashvars += "&" + k + "=" + flashvarsObj[k];
                                }
                                else {
                                    par.flashvars = k + "=" + flashvarsObj[k];
                                }
                            }
                        }
                    }
                    addDomLoadEvent(function() {
                        createSWF(att, par, replaceElemIdStr);
                        if (att.id == replaceElemIdStr) {
                            setVisibility(replaceElemIdStr, true);
                        }
                    });
                }
                else if (xiSwfUrlStr && !isExpressInstallActive && hasPlayerVersion("6.0.65") && (ua.win || ua.mac)) {
                    isExpressInstallActive = true; // deferred execution
                    setVisibility(replaceElemIdStr, false);
                    addDomLoadEvent(function() {
                        var regObj = {};
                        regObj.id = regObj.altContentId = replaceElemIdStr;
                        regObj.width = widthStr;
                        regObj.height = heightStr;
                        regObj.expressInstall = xiSwfUrlStr;
                        showExpressInstall(regObj);
                    });
                }
            },

            getFlashPlayerVersion: function() {
                return { major:ua.pv[0], minor:ua.pv[1], release:ua.pv[2] };
            },

            hasFlashPlayerVersion: hasPlayerVersion,

            createSWF: function(attObj, parObj, replaceElemIdStr) {
                if (ua.w3cdom) {
                    return createSWF(attObj, parObj, replaceElemIdStr);
                }
                else {
                    return undefined;
                }
            },

            removeSWF: function(objElemIdStr) {
                if (ua.w3cdom) {
                    removeSWF(objElemIdStr);
                }
            },

            createCSS: function(sel, decl) {
                if (ua.w3cdom) {
                    createCSS(sel, decl);
                }
            },

            addDomLoadEvent: addDomLoadEvent,

            addLoadEvent: addLoadEvent,

            getQueryParamValue: function(param) {
                var q = doc.location.search || doc.location.hash;
                if (param == null) {
                    return urlEncodeIfNecessary(q);
                }
                if (q) {
                    var pairs = q.substring(1).split("&");
                    for (var i = 0; i < pairs.length; i++) {
                        if (pairs[i].substring(0, pairs[i].indexOf("=")) == param) {
                            return urlEncodeIfNecessary(pairs[i].substring((pairs[i].indexOf("=") + 1)));
                        }
                    }
                }
                return "";
            },

            // For internal usage only
            expressInstallCallback: function() {
                if (isExpressInstallActive && storedAltContent) {
                    var obj = getElementById(EXPRESS_INSTALL_ID);
                    if (obj) {
                        obj.parentNode.replaceChild(storedAltContent, obj);
                        if (storedAltContentId) {
                            setVisibility(storedAltContentId, true);
                            if (ua.ie && ua.win) {
                                storedAltContent.style.display = "block";
                            }
                        }
                        storedAltContent = null;
                        storedAltContentId = null;
                        isExpressInstallActive = false;
                    }
                }
            }
        };
    }();
}
$(document).ready(function() { 
        var hrefs = $("a");
        var curHost=  "http://"+ window.location.host;
        var govDomainList = new Array('http://www.ask.census.gov','https://www.census.gov', 'http://census.gov', 'https://www.census.gov', 'https://census.gov','http://www.commerce.gov', 'http://commerce.gov',curHost);
            
         
          $('#confirm1').hide();
       // This Loop used to initialize external link handlers for every <a> on the page.
        // this refers to the current <a> element
          var filename = window.location.pathname.split('/').pop();
          if(filename.indexOf("siteheader")== -1) {
        $('a').each(function(){
        var lnk=$(this).attr('href');
        if(typeof lnk === "undefined" || !lnk.match("^http")||lnk.indexOf(curHost)!=-1){
        }
        else{
        var index = externalink($(this).attr('href'), govDomainList);       
        if(!index) {
        	$(this).attr('target', '_blank'); 
            $(this).on('click', function(event){
                interstit($(this).attr('href'), event);
           });
          }
         }
        });
          }
   });
   
// Popup Disclaimer for external links
function interstit( page, event ) {
    var ret;
    $msg=$.trim($('#popuptext').text()) ;
    ret=confirm($msg);
    if ( ret ) {
        // window.open(page)
     } else {
        event.preventDefault();
     }
}

function externalink(domain, domList) {

  var domainRexp = new RegExp('.*\\.gov');
  var found = false;
  
  for(var i = 0; i < domList.length; i++)   {
         if(domainRexp.test(domain) ) {
          found = true;
          break;
        }
  }
    return found;
}
$(document).ready(function () {   
     
    //$(".lefttnav > li").children("ul").css("display", "none"); //hide all dropdowns
    //var top = $(".lefttnav > li").position().top + $(".lefttnav > li").height(); //find the top position for the dropdown menu by adding the top position of the navigation links to their height (giving the bottom position for the navigation links)       
    var counter = 0;
    //when the user hovers of the navigation link..
    var navItems=null    
navItems=$(".grid_navInnerLandingLinks >ul").children();
    if(navItems.length==0){
        navItems=$(".leftnav >ul").children();
    }
   
    if(navItems.length>0){
    //$(navItems).children("span").css("margin-left","5px");
    $(navItems).each(function (index) {
        if($(this).children("ul").length>0){          
        if(!$(this).children("ul").hasClass("expanded")){
        $(this).hover(delayNavDisplay,hideNavSub);
        }else{                              
        var children= $(this).children("ul").children();
        if(children){              
            $(children).each(function (index) {            
                           if($(this).children("ul").hasClass("expanded")){                           
                           $(this).children("ul").children("li").css('margin-left', '0px');                            
                           }
                           else{
                $(this).hover(delayNavDisplay2,hideNavSub);
                }                
            });
        }
        }
    }
    });
    }
});

var delayNavDisplay = function () {
    //clearInterval(delay);
     var left = $(this).position().left; //get the postion of the main link relative to the <body>
    var right = $(this).position().right; // get the right margin of the parent tab
    var offset = $(this).offset().left; //get the position of the main link relative to the document
    var width = $(this).width(); //get the width of the dropdown
    
    var listItem = this;
    $(this).css('background-color', '#0d6a99');    
    $(this).children("span").children("a").css('color', '#FFFFFF');
    $(this).children("span").children("strong").css('color', '#FFFFFF');
    $(this).children("span").children("a").css('text-decoration', 'underline');
    
    $(this).children("ul").css("display", "block");
    $(this).children("ul").css("position", "absolute");   
    $(this).children("ul").css("box-shadow", "0px 0px 0px #818493, 3px 3px 3px #818493, 0px 0px 0px #818493");    
    $(this).children("ul").children("li").children("a").css('color', '#041C5D');
    
    $(this).children("ul").css({
        left: width,
        top: $(this).position().top
     });
};

var delayNavDisplay2 = function () {
    
    var left = $(this).position().left; //get the postion of the main link relative to the <body>
    var right = $(this).position().right; // get the right margin of the parent tab
    var offset = $(this).offset().left; //get the position of the main link relative to the document
    var width = $(this).width(); //get the width of the dropdown
    
    var listItem = this;
    $(this).css('background-color', '#66cccc');
    $(this).children("span").children("a").css('text-decoration', 'underline')
    $(this).css('color', 'black');
    $(this).children("ul").css("display", "block");
    $(this).children("ul").css("position", "absolute");    
    $(this).children("ul").css("box-shadow", "0px 0px 0px #818493, 3px 3px 3px #818493, 0px 0px 0px #818493");
    
    $(this).children("ul").css({
        left: width-2,
        top: $(this).position().top
     });
};

var hideNavSub = (function () { //if the user moves the cursor outside of the dropdown menu...
    //clearInterval(delay);
    $(this).css('background-color', '');
    $(this).children("span").children("a").css('color', 'rgb(51,51,51)');
    $(this).children("span").children("strong").css('color', 'rgb(51,51,51)');
    $(this).css('color', 'rgb(51,51,51)');
    $($($(this).children().get(0)).children().get(0)).css("text-decoration","none");
    $(this).children("ul").hide(); //hide the menu
    
});

function expandAll_EL() {
    $( "#EL_accordion>div" ).accordion( "option", "active", 0);
    $('#collapseAll').show();
    $('#expandAll').hide();
    $('#toTop').show();
}

function collapseAll_EL() {  
    $( "#EL_accordion>div" ).accordion( "option", "active", false);
    $('#collapseAll').hide();
    $('#expandAll').show();
    $('#toTop').hide();
}
function customFilter(array, terms) {
    arrayOfTerms = terms.split(" ");
    var term = $.map(arrayOfTerms, function(tm) {
        return $.ui.autocomplete.escapeRegex(tm);
    }).join('|');
    var matcher = new RegExp("\\b" + term, "i");
    return $.grep(array, function(value) {
        return matcher.test(value.value);
    });
};

$(document)
        .ready(
                function() {
                    if ($("#geoField").length>0) {
                        $("#geoField")
                                .autocomplete(
                                        {
                                            source : data,
                                            multiple : true,
                                            minLength : 0,
                                            appendTo : "#statelist",
                                            mustMatch : false,
                                            source : function(request, response) {
                                                // delegate back to
                                                // autocomplete, but extract the
                                                // last term
                                                response(customFilter(data,
                                                        request.term));
                                            },
                                            select : function(event, ui) {
                                                //location.href = ui.item.url;
                                                //window.open(ui.item.url, '_blank');
                                                return false;
                                            },
                                            focus : function(event, ui) {
                                                return false;
                                            },
                                            create : function() {
                                                $(this).data('ui-autocomplete')._renderItem = function(
                                                        ul, item) {
                                                    return $('<li>')
                                                            .append(
                                                                    '<a href="'+item.url+'" class="stateitem" filetrack="' + item.fileTrackKey +'" onclick="buttonClick(this);" target="_blank">'
                                                                        + item.label
                                                                        + '</a>')
                                                            .appendTo(ul);
                                                };
                                            }
                                        });
                    }
                });
    $(document).ready(function() {
        var hrefs = $("a");
       // This Loop used to initialize link handlers for every <a> on the page.

        $('a').each(function(){
            var lnk = $(this).attr('href');
            var isdownload = $(this).attr('filetrack');
            
            if( typeof isdownload !== typeof undefined && isdownload !== false ){                
                $(this).click(trackDownload);
            }
        });
    });

    function trackDownload(){
        span('isdownload clicked...');
        var isdownload = $(this).attr('filetrack');
        var downloadArray = isdownload.split('|');
        digitalData.event.eventInfo.download.fileUrl = downloadArray[0];
        digitalData.event.eventInfo.download.fileName = downloadArray[1];
        digitalData.event.eventName = 'Download';
    }

    function trackSConversion(){
        //digitalData.event.eventInfo.search.term = digitalData.event.eventInfo.search.term; //??
    }

