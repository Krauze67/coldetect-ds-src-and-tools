
Lifestyle
Japan’s top 100 most common family names

    Oct 11, 2009
    Article history
    PRINT
    SHARE

America may have around a million surnames and Finland the most in the world in proportion to its population, but with more than 100,000 family names now in use in Japan, this country puts China’s few thousand and Korea’s mere 200 firmly in the name-game shade.

Here, in descending order, are the top 100 most common Japanese family names these days — followed by a list of the five most commonly occurring ones in each of the country’s 47 prefectures.

    Sato
    Suzuki
    Takahashi
    Tanaka
    Watanabe
    Ito
    Yamamoto
    Nakamura
    Kobayashi
    Kato
    Yoshida
    Yamada
    Sasaki
    Yamaguchi
    Saito
    Matsumoto
    Inoue
    Kimura
    Hayashi
    Shimizu
    Yamazaki
    Mori
    Abe
    Ikeda
    Hashimoto
    Yamashita
    Ishikawa
    Nakajima
    Maeda
    Fujita
    Ogawa
    Goto
    Okada
    Hasegawa
    Murakami
    Kondo
    Ishii
    Saito (different kanji)
    Sakamoto
    Endo
    Aoki
    Fujii
    Nishimura
    Fukuda
    Ota
    Miura
    Fujiwara
    Okamoto
    Matsuda
    Nakagawa
    Nakano
    Harada
    Ono
    Tamura
    Takeuchi
    Kaneko
    Wada
    Nakayama
    Ishida
    Ueda
    Morita
    Hara
    Shibata
    Sakai
    Kudo
    Yokoyama
    Miyazaki
    Miyamoto
    Uchida
    Takagi
    Ando
    Taniguchi
    Ohno
    Maruyama
    Imai
    Takada
    Fujimoto
    Takeda
    Murata
    Ueno
    Sugiyama
    Masuda
    Sugawara
    Hirano
    Kojima
    Otsuka
    Chiba
    Kubo
    Matsui
    Iwasaki
    Sakurai
    Kinoshita
    Noguchi
    Matsuo
    Nomura
    Kikuchi
    Sano
    Onishi
    Sugimoto
    Arai


