package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"math/rand"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/Krauze67/coldetect/tools"
	"gitlab.com/Krauze67/flib/stringtools"

	fp "github.com/Amonsat/fullname_parser"
	"github.com/fxsjy/gonn/gonn"
	log "github.com/sirupsen/logrus"
)

const (
	StatDir = "stat-test_2017-08-25_05_NN-binSum-RND-SameLength"
	DSRoot  = "/home/df/dev/go/dfwork/coldetector_/coldetect-ds"
	//DSRoot  = "/home/df/dev/go/dfwork/coldetector_/coldetect-ds-fix-CLEAN"
	COUNTRY_FILTER         = "US|CA|GB|AU|FR"
	CountriesAssumeMatched = "US|CA|GB|AU"
	FileOutMatchedFilt     = "matched_filt.csv"
	FileOutNotmatchedFilt  = "not_matched_filt.csv"
	FileOutMatched         = "matched.csv"
	FileOutNotmatched      = "not_matched.csv"
	FileOutStat            = "stat.txt"
	FileInOrders           = "orders.csv"

	CsvHeaderNotMatched = "fullname,original country,Fname,Lname"
	cNNInputFields      = 28
)

func main() {
	log.SetLevel(log.InfoLevel)
	log.SetFormatter(&log.TextFormatter{ForceColors: true})

	log.Infoln("Init...")

	// log.Info("Load NN firstnames:")
	// nnF := gonn.LoadNN("./nann-fnames-30-20-1-5.nn")
	// log.Info("Load NN lastnames:")
	// nnL := gonn.LoadNN("./nann-lnames-30-20-1-5.nn")
	log.Info("Load NN firstnames:")
	nnF := gonn.LoadNN("./nann-fnames-28-17-1-1.nn")
	log.Info("Load NN lastnames:")
	nnL := gonn.LoadNN("./nann-lnames-28-17-1-1.nn")

	log.Infoln("Initialized")

	//========================================================
	// fullname := "Adonay Obando"
	// detectedC, detectErr := detector.GetCountriesByFullname(fullname)
	// //check(err)

	// if nil != detectErr {
	// 	fmt.Println("Detect error: %v", detectErr)
	// } else {
	// 	for idx, item := range detectedC {
	// 		nextLine := fmt.Sprintf("%v - %v: %v (%v)", fullname, idx, item.Country, item.Score)
	// 		fmt.Println(nextLine)
	// 	}
	// }

	// return
	//========================================================

	statDir, _ := filepath.Abs(StatDir)
	os.MkdirAll(statDir, os.ModeDir|os.ModePerm)

	fileNotMatchesFiltered, err := os.Create(filepath.Join(StatDir, FileOutNotmatchedFilt))
	check(err)
	defer fileNotMatchesFiltered.Close()
	fileMatchesFiltered, err := os.Create(filepath.Join(StatDir, FileOutMatchedFilt))
	check(err)
	defer fileMatchesFiltered.Close()

	fileNotMatchesRest, err := os.Create(filepath.Join(StatDir, FileOutNotmatched))
	check(err)
	defer fileNotMatchesRest.Close()
	fileMatchesRest, err := os.Create(filepath.Join(StatDir, FileOutMatched))
	check(err)
	defer fileMatchesRest.Close()

	fileStat, err := os.Create(filepath.Join(StatDir, FileOutStat))
	check(err)
	defer fileStat.Close()

	fileNotMatchesFiltered.WriteString(CsvHeaderNotMatched + "\n")
	fileMatchesFiltered.WriteString(CsvHeaderNotMatched + "\n")
	fileNotMatchesRest.WriteString(CsvHeaderNotMatched + "\n")
	fileMatchesRest.WriteString(CsvHeaderNotMatched + "\n")

	var (
		matches    int
		notMatches int
		records    int
	)
	orders := parseOrders(FileInOrders)
	for _, v := range orders {
		records++
		log.Infof("processing line #%d\n", records)
		origCountry := v.country

		log.Debugf("Test[%d]: '%v' -> %v\n", records, v.fullname, origCountry)
		parsedname := fp.ParseFullname(v.fullname)

		firstname := strings.TrimSpace(parsedname.First)
		//first := strings.ToUpper(firstname)
		first := RandStr(len(firstname))
		first = tools.Str2vec(first)
		nameAfloat := tools.Vec2afloat(first)
		//nameAfloat := StrToNFloats(first, cNNInputFields)
		if cNNInputFields != len(nameAfloat) {
			log.Warningf("   invalid input fields(firstname): %d", len(nameAfloat))
			continue
		}
		ansF := nnF.Forward(nameAfloat)

		lastname := strings.TrimSpace(parsedname.Last)
		//last := strings.ToUpper(lastname)
		last := RandStr(len(lastname))
		last = tools.Str2vec(last)
		nameAfloat = tools.Vec2afloat(last)
		//nameAfloat = StrToNFloats(last, cNNInputFields)
		if cNNInputFields != len(nameAfloat) {
			log.Warningf("   invalid input fields(lastname): %d", len(nameAfloat))
			continue
		}
		ansL := nnL.Forward(nameAfloat)

		log.Debugf("        '%v' -> %v\n", firstname, ansF[0])
		log.Debugf("        '%v' -> %v\n", lastname, ansL[0])

		bIsEngCountry := strings.Contains(CountriesAssumeMatched, origCountry)

		//text := fmt.Sprintf("%v,%v,%v,%v\n", v.fullname, v.country, ansF[0], ansL[0])
		text := fmt.Sprintf("%v,%v,%v,%v\n", first, last, ansF[0], ansL[0])
		bIsMatched := false
		if bIsEngCountry {
			bIsMatched = (ansF[0] > 0.5 && ansL[0] > 0.5)
		} else {
			fileNotMatchesRest.WriteString(fmt.Sprintf("\"%s\",\"\",\"%s\"\n", v.fullname, origCountry))
			bIsMatched = (ansF[0] < 0.5 && ansL[0] < 0.5)
		}

		if bIsMatched {
			fileMatchesFiltered.WriteString(text)
			matches++
		} else {
			fileNotMatchesFiltered.WriteString(text)
			notMatches++
		}
	}

	fileStat.WriteString(fmt.Sprintf("Total records: %v\n", records))
	fileStat.WriteString(fmt.Sprintf("Number of matches: %v (%v %%)\n", matches, float64(matches)/float64(records)*100))
	fileStat.WriteString(fmt.Sprintf("Number of not matches: %v (%v %%)\n----------\n", notMatches, float64(notMatches)/float64(records)*100))

	// fileStat.WriteString(fmt.Sprintf("Filtered records: %v\n", recordsFiltered))
	// fileStat.WriteString(fmt.Sprintf("    filtered matches: %v(%v %%))\n", matchesFiltered, float64(matchesFiltered)/float64(recordsFiltered)*100))
	// fileStat.WriteString(fmt.Sprintf("    filtered not matches: %v(%v %%)\n", notMatchesFiltered, float64(notMatchesFiltered)/float64(recordsFiltered)*100))

	fmt.Println("All work done!")
}

func RandStrRndLen(min, max int) string {
	ncount := rand.Intn(int(float64(min) + float64(max) - float64(min) + float64(0.5)))
	return RandStr(ncount)
}

func RandStr(count int) string {
	runes := make([]rune, count)
	enUp := []rune(stringtools.EnUpper)
	for i := range runes {
		runes[i] = enUp[rand.Intn(len(enUp)-1)]
	}
	return string(runes)
}

func StrToNFloats(str string, count int) []float64 {
	var out []float64
	runes := []rune(str)
	add_max := len(runes)
	if count < add_max {
		add_max = count
	}
	for i := 0; i < add_max; i++ {
		// if float64(runes[i]-'A'+1) < 0 {
		// 	fmt.Println("")
		// }
		out = append(out, float64(runes[i]-'A'+1))
	}
	for i := len(runes); i < count; i++ {
		out = append(out, float64(0))
	}
	return out
}

func check(e error) {
	if e != nil {
		err := fmt.Sprintln(e)
		fmt.Println(err)
	}
}

type Order struct {
	fullname string
	country  string
}

func parseOrders(filename string) (orders []Order) {
	f, err := os.Open(filename)
	if err != nil {
		fmt.Println("Open error: ", err)
	}

	r := csv.NewReader(bufio.NewReader(f))
	r.Comma = ','

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		fullname := record[0] + " " + record[1]
		fullname = strings.TrimSpace(fullname)
		fullname = strings.Trim(fullname, ". \"\t")

		country := record[2]
		country = strings.TrimSpace(country)

		orders = append(orders, Order{fullname: fullname, country: country})
	}
	return
}
