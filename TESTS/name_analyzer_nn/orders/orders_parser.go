package orders

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strings"
)

type Order struct {
	Fullname string
	Country  string
}

func ParseFile(filename string) (orders []Order) {
	f, err := os.Open(filename)
	if err != nil {
		fmt.Println("Open error: ", err)
	}

	r := csv.NewReader(bufio.NewReader(f))
	r.Comma = ','

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		fullname := record[0] + " " + record[1]
		fullname = strings.TrimSpace(fullname)
		fullname = strings.Trim(fullname, ".")

		country := record[2]
		country = strings.TrimSpace(country)

		orders = append(orders, Order{Fullname: fullname, Country: country})
	}
	return
}
