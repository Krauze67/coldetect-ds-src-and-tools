package tools

import (
	"fmt"
	"math"
	"strconv"
	"strings"
	"unicode"
)

// Name2afloat convert name to float64 array of bits representation
func Name2afloat(name string) (afloat []float64) {
	var sum float64
	for _, v := range name {
		sum += rune2float(v)
	}
	vec := fmt.Sprintf("%028b", int64(sum))
	for _, b := range vec {
		f, _ := strconv.ParseFloat(string(b), 64)
		afloat = append(afloat, f)
	}
	return
}

func Vec2afloat(vec string) (afloat []float64) {
	for _, b := range vec {
		f, _ := strconv.ParseFloat(string(b), 64)
		afloat = append(afloat, f)
	}
	return
}

func Str2vec(str string) (vec string) {
	var sum float64
	for _, v := range str {
		sum += rune2float(v)
	}
	vec = fmt.Sprintf("%028b", int64(sum))
	return
}

func Afloat2int(afloat []float64) (num int64) {
	var strs []string
	for _, v := range afloat {
		// s := strconv.FormatFloat(v, 'f', 0, 64)
		f := int64(math.Abs(v) + .5)
		s := strconv.FormatInt(f, 10)
		strs = append(strs, s)
	}
	str := strings.Join(strs, "")
	num, _ = strconv.ParseInt(str, 2, 64)
	return
}

func rune2float(r rune) (res float64) {
	res = 0
	if unicode.IsLetter(r) {
		res = math.Pow(2, float64(r)-65)
	}
	return
}

func Int2afloat(num int64) (afloat []float64) {
	vec := fmt.Sprintf("%05b", num)
	for _, b := range vec {
		f, _ := strconv.ParseFloat(string(b), 64)
		afloat = append(afloat, f)
	}
	return
}

// IsLetters check if all character in string is letter
func IsLetters(str string) bool {
	for _, v := range str {
		if !unicode.IsLetter(v) {
			return false
		}
	}
	return true
}
