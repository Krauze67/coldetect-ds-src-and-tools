package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"math"
	"os"
	"strconv"
	"strings"

	pb "gopkg.in/cheggaaa/pb.v1"

	"./orders"
	"./tools"
	fp "github.com/Amonsat/fullname_parser"
	"github.com/fxsjy/gonn/gonn"
	log "github.com/sirupsen/logrus"
)

const (
	COUNTRY_FILTER = "US|CA|GB|AU"
)

type countryCode struct {
	country string
	code    int64
}

var countries []countryCode

var tests = [][]string{
	{"Aaron Cohen", "US"},
	{"Aaron Harden", "US"},
	{"Aasia Williams", "US"},
	{"Abby Rumpel", "CA"},
	{"Abdussamad Seedat", "AE"},
	{"Aasia Cohen", "US"},
	{"Aaron Williams", "US"},
	{"abdel soudquy", "CA"},
	{"Abdul Shally", "CA"},
	{"William Hewitt", "CA"},
	{"Robert Benson", "CA"},
	{"ABEL MALDONADO", "MX"},
	{"Abelia Patrigeon", "FR"},
	{"Valerie CHIZZOLINI", "FR"},
}

const (
	// TRAIN_ITERATION = 1
	// NN_ENTERS       = 28
	// NN_HIDDEN       = 17
	// NN_OUTPUT       = 1
	TRAIN_ITERATION = 5
	NN_ENTERS       = 30
	NN_HIDDEN       = 20
	NN_OUTPUT       = 1
)

func main() {
	// log.SetLevel(log.DebugLevel)
	log.SetLevel(log.InfoLevel)
	log.SetFormatter(&log.TextFormatter{ForceColors: true})

	trainNNs()
	log.Info("All done!")
	return

	checkTestsNN()
	log.Info("All done!")
	return

	// var nn *gonn.NeuralNetwork
	// dumpName := fmt.Sprintf("nann-%v-%v-%v-%v.nn", NN_ENTERS, NN_HIDDEN, NN_OUTPUT, TRAIN_ITERATION)

	// if _, err := os.Stat(dumpName); os.IsNotExist(err) {
	// 	inputs, targets := loadTrainData()
	// 	nn = gonn.DefaultNetwork(NN_ENTERS, NN_HIDDEN, NN_OUTPUT, true)
	// 	nn.Train(inputs, targets, TRAIN_ITERATION)
	// 	gonn.DumpNN(dumpName, nn)
	// } else {
	// 	nn = gonn.LoadNN(dumpName)
	// 	//loadFromFile(countryCodeName)
	// }

	// for _, v := range tests {
	// 	parsedname := fp.ParseFullname(v[0])

	// 	first := strings.TrimSpace(parsedname.First)
	// 	first = strings.ToUpper(first)
	// 	first = tools.Str2vec(first)

	// 	second := strings.TrimSpace(parsedname.Last)
	// 	second = strings.ToUpper(second)
	// 	second = tools.Str2vec(second)

	// 	nameVec := first + second
	// 	nameAfloat := tools.Vec2afloat(nameVec)
	// 	// nameAfloat := tools.Name2afloat(strings.ToUpper(v[0]))
	// 	ans := nn.Forward(nameAfloat)

	// 	for i, n := range ans {
	// 		ans[i] = math.Abs(n)
	// 	}
	// 	code := tools.Afloat2int(ans)
	// 	country, err := getCountryByCode(code)
	// 	if err != nil {
	// 		log.Warnf("%v => %.1f", err, ans)
	// 		continue
	// 	}
	// 	fmt.Printf("%-20v | %v => %.0f => %.1f => %v \n", v[0], v[1], nameAfloat, ans, country)
	// }

	// for _, v := range tests {
	// 	parsedname := fp.ParseFullname(v[0])
	// 	first := strings.TrimSpace(parsedname.First)
	// 	first = strings.ToUpper(first)
	// 	first = tools.Str2vec(first)
	// 	second := strings.TrimSpace(parsedname.Last)
	// 	second = strings.ToUpper(second)
	// 	second = tools.Str2vec(second)

	// 	nameVec := first + second
	// 	nameAfloat := tools.Vec2afloat(nameVec)
	// 	// nameAfloat := tools.Name2afloat(strings.ToUpper(v[0]))
	// 	ans := nn.Forward(nameAfloat)

	// 	for i, n := range ans {
	// 		ans[i] = math.Abs(n)
	// 	}
	// 	code := tools.Afloat2int(ans)
	// 	country, err := getCountryByCode(code)
	// 	if err != nil {
	// 		log.Warnf("%v => %.1f", err, ans)
	// 		continue
	// 	}
	// 	fmt.Printf("%-20v | %v => %.0f => %.1f => %v \n", v[0], v[1], nameAfloat, ans, country)
	// }
}

func trainNNs() {
	log.Infof("Loading train data %s...", "firstnames")
	nnFirstnames := gonn.DefaultNetwork(NN_ENTERS, NN_HIDDEN, NN_OUTPUT, true)
	dumpNameFirstnames := fmt.Sprintf("nann-fnames-%v-%v-%v-%v.nn", NN_ENTERS, NN_HIDDEN, NN_OUTPUT, TRAIN_ITERATION)

	inputs, targets := loadTrainData("./traindata/firstname_train.csv")
	//inputs, targets := loadTrainData("./traindata/test.csv")
	log.Infof("Training %s...", "firstnames")
	nnFirstnames.Train(inputs, targets, TRAIN_ITERATION)
	log.Infof("Saving NN %s...", "firstnames")
	gonn.DumpNN(dumpNameFirstnames, nnFirstnames)

	log.Infof("Loading train data %s...", "lastnames")
	nnLastnames := gonn.DefaultNetwork(NN_ENTERS, NN_HIDDEN, NN_OUTPUT, true)
	dumpNameLastnames := fmt.Sprintf("nann-lnames-%v-%v-%v-%v.nn", NN_ENTERS, NN_HIDDEN, NN_OUTPUT, TRAIN_ITERATION)

	inputs, targets = nil, nil
	inputs, targets = loadTrainData("./traindata/lastname_train.csv")
	//inputs, targets := loadTrainData("./traindata/test.csv")
	log.Infof("Training %s...", "lastnames")
	nnLastnames.Train(inputs, targets, TRAIN_ITERATION)
	log.Infof("Saving NN %s...", "lastnames")
	gonn.DumpNN(dumpNameLastnames, nnLastnames)

	// nnLastnames := gonn.DefaultNetwork(NN_ENTERS, NN_HIDDEN, NN_OUTPUT, true)
	// dumpNameLastnames := fmt.Sprintf("nann-lnames-%v-%v-%v-%v.nn", NN_ENTERS, NN_HIDDEN, NN_OUTPUT, TRAIN_ITERATION)

	// log.Infof("Loading train data %s...", "lastnames")
	// inputs, targets = nil, nil
	// inputs, targets = loadTrainData("./traindata/lastname_train.csv")
	// //inputs, targets := loadTrainData("./traindata/test.csv")
	// log.Infof("Training %s...", "lastnames")
	// nnLastnames.Train(inputs, targets, TRAIN_ITERATION)
	// log.Infof("Saving NN %s...", "lastnames")
	// gonn.DumpNN(dumpNameLastnames, nnLastnames)
}

func checkTestsNN() {
	log.Info("Load NN firstnames:")
	nnF := gonn.LoadNN("./nann-fnames-28-17-1-1.nn")
	log.Info("Load NN lastnames:")
	nnL := gonn.LoadNN("./nann-lnames-28-17-1-1.nn")

	log.Info("Start tests")
	for testidx, v := range tests {
		log.Infof("Test[%d]: '%v' -> %v\n", testidx, v[0], v[1])
		parsedname := fp.ParseFullname(v[0])

		firstname := strings.TrimSpace(parsedname.First)
		first := strings.ToUpper(firstname)
		first = tools.Str2vec(first)
		nameAfloat := tools.Vec2afloat(first)
		ansF := nnF.Forward(nameAfloat)

		lastname := strings.TrimSpace(parsedname.Last)
		last := strings.ToUpper(lastname)
		last = tools.Str2vec(last)
		nameAfloat = tools.Vec2afloat(last)
		ansL := nnL.Forward(nameAfloat)

		//for i, n := range ansF {
		log.Infof("        '%v' -> %v\n", firstname, ansF)
		//}
		//for i, n := range ansL {
		log.Infof("        '%v' -> %v\n", lastname, ansL)
		//}
	}
}

func loadTrainData(filename string) (inputs, targets [][]float64) {
	log.Infof("Load train data from %s...", filename)
	var fInput *os.File
	var err error
	if fInput, err = os.Open(filename); nil != err {
		log.Error(err.Error())
	}
	r := csv.NewReader(bufio.NewReader(fInput))
	r.Comma = ','

	nLine := 0
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		nLine++
		if nil != err {
			switch err.(type) {
			case *csv.ParseError:
				log.Error(err.Error())
				continue
			default:
				break
			}
		}
		if NN_ENTERS+NN_OUTPUT != len(record) {
			log.Warningf("%v", record)
			log.Warningf("Train data - %d fields on line %d when expected %d - skipped", len(record), nLine, NN_ENTERS+NN_OUTPUT)
			continue
		}

		var newInp []float64
		var newTarg []float64
		for idx, valstr := range record {
			f, _ := strconv.ParseFloat(valstr, 64)
			if idx < len(record)-1 {
				newInp = append(newInp, f)
			} else {
				newTarg = append(newTarg, f)
			}
		}

		inputs = append(inputs, newInp)
		targets = append(targets, newTarg)
	}
	return
}

func prepareTrainData() (inputs, targets [][]float64) {
	ordersData := orders.ParseFile("./orders/orders.csv")
	log.Infoln("Prepare train data...")
	bar := pb.StartNew(len(ordersData))
	for _, v := range ordersData {
		bar.Increment()
		if !strings.Contains(COUNTRY_FILTER, v.Country) || strings.TrimSpace(v.Country) == "" {
			continue
		}

		parsedname := fp.ParseFullname(v.Fullname)
		if !tools.IsLetters(parsedname.First) && !tools.IsLetters(parsedname.Last) {
			continue
		}

		// fullname := parsedname.First + " " + parsedname.Last
		// fullname = strings.TrimSpace(fullname)
		// nameAfloat := tools.Name2afloat(strings.ToUpper(fullname))

		first := strings.TrimSpace(parsedname.First)
		first = strings.ToUpper(first)
		first = tools.Str2vec(first)
		second := strings.TrimSpace(parsedname.Last)
		second = strings.ToUpper(second)
		second = tools.Str2vec(second)

		nameVec := first + second
		nameAfloat := tools.Vec2afloat(nameVec)

		// log.Debugf("%v - %.1f", , nameAfloat)

		var code int64
		if hasCountry(v.Country) {
			var err error
			code, err = getCodeByCountry(v.Country)
			if err != nil {
				log.Warnln(err)
				continue
			}
			log.Debugf("%v country exist - %v", v.Country, code)
		} else {
			code = int64(math.Pow(2, float64(len(countries))))
			countries = append(countries, countryCode{v.Country, code})
			log.Debugf("%v country doesnt exist, new code - %v", v.Country, code)
		}

		codeAfloat := tools.Int2afloat(code)
		log.Debugf("%v - %v - %v", v.Country, code, codeAfloat)
		// data := [][]float64{nameAfloat, codeAfloat}
		// trainData = append(trainData, data...)
		inputs = append(inputs, nameAfloat)
		targets = append(targets, codeAfloat)
	}

	code := int64(math.Pow(2, float64(len(countries))))
	countries = append(countries, countryCode{"XX", code})
	codeAfloat := tools.Int2afloat(code)
	targets = append(targets, codeAfloat)

	bar.FinishPrint("The End!")
	return
}

func saveToFile(filepath string) {
	f, err := os.Create(filepath)
	if err != nil {
		log.Error(err)
		return
	}
	defer f.Close()

	for _, v := range countries {
		s := fmt.Sprintf("%v,%v\n", v.country, v.code)
		f.WriteString(s)
	}
}

// func loadFromFile(filepath string) {
// 	f, err := os.Open(filepath)
// 	if err != nil {
// 		log.Error("Open error: ", err)
// 		return
// 	}

// 	r := csv.NewReader(bufio.NewReader(f))
// 	r.Comma = ','

// 	for {
// 		record, err := r.Read()
// 		if err == io.EOF {
// 			break
// 		}
// 		country := record[0]
// 		code, err := strconv.ParseInt(record[1], 10, 64)
// 		if err != nil {
// 			log.Error("parse: ", err)
// 			continue
// 		}
// 		countries = append(countries, countryCode{country, code})
// 	}
// 	return
// }

func hasCountry(country string) bool {
	for _, v := range countries {
		if v.country == country {
			return true
		}
	}
	return false
}

func getCountryByCode(code int64) (string, error) {
	for _, v := range countries {
		if v.code == code {
			return v.country, nil
		}
	}
	return "", fmt.Errorf("Countries has no `%v` code", code)
}

func getCodeByCountry(country string) (int64, error) {
	for _, v := range countries {
		if v.country == country {
			return v.code, nil
		}
	}
	return 0, fmt.Errorf("Countries has no `%v` country", country)
}
