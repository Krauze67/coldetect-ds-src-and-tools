package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/Krauze67/coldetect"
)

const (
	StatDir = "stat-test_2017-08-21_2_LastRepoData"
	DSRoot  = "/home/df/dev/go/dfwork/coldetector_/coldetect-ds"
	//DSRoot  = "/home/df/dev/go/dfwork/coldetector_/coldetect-ds-fix-CLEAN"
	//COUNTRY_FILTER        = "US|CA|GB|AU|FR"
	COUNTRY_FILTER         = "US|CA|GB|AU|FR"
	CountriesAssumeMatched = "US|CA|GB|AU"
	FileOutMatchedFilt     = "matched_filt.csv"
	FileOutNotmatchedFilt  = "not_matched_filt.csv"
	FileOutMatched         = "matched.csv"
	FileOutNotmatched      = "not_matched.csv"
	FileOutStat            = "stat.txt"
	FileInOrders           = "orders.csv"

	CsvHeaderNotMatched = "fullname,detected score,detected country,original country"
)

func main() {
	fmt.Println("Detector init...")

	var detector coldetect.TColDetect
	//err := detector.Init(DSRoot)
	err := detector.Init(DSRoot)
	check(err)
	fmt.Println("Detector initialized")

	//========================================================
	// fullname := "Adonay Obando"
	// detectedC, detectErr := detector.GetCountriesByFullname(fullname)
	// //check(err)

	// if nil != detectErr {
	// 	fmt.Println("Detect error: %v", detectErr)
	// } else {
	// 	for idx, item := range detectedC {
	// 		nextLine := fmt.Sprintf("%v - %v: %v (%v)", fullname, idx, item.Country, item.Score)
	// 		fmt.Println(nextLine)
	// 	}
	// }

	// return
	//========================================================

	statDir, _ := filepath.Abs(StatDir)
	os.MkdirAll(statDir, os.ModeDir|os.ModePerm)

	fileNotMatchesFiltered, err := os.Create(filepath.Join(StatDir, FileOutNotmatchedFilt))
	check(err)
	defer fileNotMatchesFiltered.Close()
	fileMatchesFiltered, err := os.Create(filepath.Join(StatDir, FileOutMatchedFilt))
	check(err)
	defer fileMatchesFiltered.Close()

	fileNotMatchesRest, err := os.Create(filepath.Join(StatDir, FileOutNotmatched))
	check(err)
	defer fileNotMatchesRest.Close()
	fileMatchesRest, err := os.Create(filepath.Join(StatDir, FileOutMatched))
	check(err)
	defer fileMatchesRest.Close()

	fileStat, err := os.Create(filepath.Join(StatDir, FileOutStat))
	check(err)
	defer fileStat.Close()

	fileNotMatchesFiltered.WriteString(CsvHeaderNotMatched + "\n")
	fileMatchesFiltered.WriteString(CsvHeaderNotMatched + "\n")
	fileNotMatchesRest.WriteString(CsvHeaderNotMatched + "\n")
	fileMatchesRest.WriteString(CsvHeaderNotMatched + "\n")

	var (
		matches    int
		notMatches int
		records    int

		recordsFiltered    int
		matchesFiltered    int
		notMatchesFiltered int
	)
	orders := parseOrders(FileInOrders)
	file_lines := 0
	for _, v := range orders {
		file_lines++
		fmt.Printf("processing line #%d\n", file_lines)

		origCountry := v.country
		// if !strings.Contains(COUNTRY_FILTER, origCountry) {
		// 	continue
		// }

		records++

		if strings.Contains(COUNTRY_FILTER, origCountry) {
			recordsFiltered++
		}
		detectedCountry, detectedScore, err := detector.GetMostProbableCountryByFullname(v.fullname)
		check(err)

		//if !strings.Contains(COUNTRY_FILTER, detectedCountry) {
		text := fmt.Sprintf("%v,%v,%v,%v\n", v.fullname, detectedScore, detectedCountry, v.country)
		bIsMatched := (origCountry == detectedCountry)

		if !bIsMatched && 0 < len(CountriesAssumeMatched) {
			bIsMatched = strings.Contains(CountriesAssumeMatched, origCountry) &&
				strings.Contains(CountriesAssumeMatched, detectedCountry)
		}

		if bIsMatched {
			if strings.Contains(COUNTRY_FILTER, origCountry) {
				fileMatchesFiltered.WriteString(text)
				matchesFiltered++
			} else {
				fileMatchesRest.WriteString(text)
			}
			matches++
		} else {
			if strings.Contains(COUNTRY_FILTER, origCountry) {
				fileNotMatchesFiltered.WriteString(text)
				notMatchesFiltered++
			} else {
				fileNotMatchesRest.WriteString(text)
			}
			notMatches++
		}
	}

	fileStat.WriteString(fmt.Sprintf("Total records: %v\n", records))
	fileStat.WriteString(fmt.Sprintf("Number of matches: %v (%v %%)\n", matches, float64(matches)/float64(records)*100))
	fileStat.WriteString(fmt.Sprintf("Number of not matches: %v (%v %%)\n----------\n", notMatches, float64(notMatches)/float64(records)*100))

	fileStat.WriteString(fmt.Sprintf("Filtered records: %v\n", recordsFiltered))
	fileStat.WriteString(fmt.Sprintf("    filtered matches: %v(%v %%))\n", matchesFiltered, float64(matchesFiltered)/float64(recordsFiltered)*100))
	fileStat.WriteString(fmt.Sprintf("    filtered not matches: %v(%v %%)\n", notMatchesFiltered, float64(notMatchesFiltered)/float64(recordsFiltered)*100))

	fmt.Println("All work done!")
}

func check(e error) {
	if e != nil {
		err := fmt.Sprintln(e)
		fmt.Println(err)
	}
}

type Order struct {
	fullname string
	country  string
}

func parseOrders(filename string) (orders []Order) {
	f, err := os.Open(filename)
	if err != nil {
		fmt.Println("Open error: ", err)
	}

	r := csv.NewReader(bufio.NewReader(f))
	r.Comma = ','

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		fullname := record[0] + " " + record[1]
		fullname = strings.TrimSpace(fullname)
		fullname = strings.Trim(fullname, ".")

		country := record[2]
		country = strings.TrimSpace(country)

		orders = append(orders, Order{fullname: fullname, country: country})
	}
	return
}
